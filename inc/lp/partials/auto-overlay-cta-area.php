<?php 

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * @var $webinar_data
 */
?>

<?php require WEBINARIGNITION_PATH . 'inc/lp/partials/main-overlay-cta.php'; ?>
<?php require WEBINARIGNITION_PATH . 'inc/lp/partials/additional-overlay-cta.php';
