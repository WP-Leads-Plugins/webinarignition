<?php
/**
 * @var $webinar_data
 * @var $assets
 * @var $statusCheck
 */

if(webinarignition_fs()->is__premium_only()){
    $statusCheck_name = $statusCheck->name;

    if (!empty($webinar_data->video_integration) && $webinar_data->video_integration === 'video_sdk') {
        if (empty($webinar_data->video_sdk_api_key)) {
            $webinar_data->webinar_live_video = '';
        } else {
            ob_start();
            require WEBINARIGNITION_PATH . 'inc/lp/partials/webinar_page/partials/webinar-live-video-content-videosdk.php';
            $webinar_data->webinar_live_video = ob_get_clean();
        }
    }
    webinarignition_display(
        do_shortcode( $webinar_data->webinar_live_video ),
        '<img class="img-fluid" style="width: 85%;max-width: 100%;height: auto;" src="' . $assets . '/images/videoplaceholder.png" />'
    );
} else {
    webinarignition_display(
        do_shortcode( $webinar_data->webinar_live_video ),
        '<img class="img-fluid" style="width: 85%;max-width: 100%;height: auto;" src="' . $assets . '/images/videoplaceholder.png" />'
    );
}
?>
