<?php
/**
 * @var $webinar_data
 * @var $leadinfo
 */

$current_user = wp_get_current_user();
$videosdk_api_key = !empty($webinar_data->video_sdk_api_key) ? $webinar_data->video_sdk_api_key : '';
$webinar_hosts = !empty($webinar_data->video_sdk_hosts) ? $webinar_data->video_sdk_hosts : '';
$webinar_id = $webinar_data->hash_id;
$admin = !empty($_GET['admin']) ? 'yes' : '';
$webinar_hosts_ids = explode('|', $webinar_hosts);

$name = 'Guest';
$admin = false;
$video_sdk_type = !empty($webinar_data->video_sdk_type) && in_array($webinar_data->video_sdk_type, ['video_sdk_hls', 'video_sdk_rtc']) ? $webinar_data->video_sdk_type : 'video_sdk_hls';
if (!empty($current_user->ID)) {
    $admin = in_array($current_user->ID, $webinar_hosts_ids);

    $name = !empty($current_user->display_name) ? $current_user->display_name : $current_user->user_login;
} else if (!empty($lead->name)) {
    $name = $lead->name;
}

$admin = !empty($admin) ? 'yes' : '';

$videosdk_token = !empty($webinar_data->video_sdk_token) ? $webinar_data->video_sdk_token : '';
?>
<?php require_once WEBINARIGNITION_PATH . 'inc/lp/partials/webinar_page/partials/videosdk/' . $video_sdk_type . '.php'; ?>

