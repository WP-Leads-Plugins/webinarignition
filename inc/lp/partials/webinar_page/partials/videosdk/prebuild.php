<?php
/**
 * @var $admin
 * @var $assets
 * @var $name
 * @var $webinar_id
 * @var $videosdk_api_key
 */
?>

<script>
    document.body.classList.add("videosdk_webinar_page");
    var admin = '<?php echo $admin; ?>';
    console.log({admin});
    var script = document.createElement("script");
    script.type = "text/javascript";

    script.addEventListener("load", function (event) {
        const config = {
            name: '<?php echo $name; ?>', // Participant name
            meetingId: '<?php echo $webinar_id; ?>', // Root ID
            // apiKey: '<?php echo $videosdk_api_key; ?>', // API Key
            token: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhcGlrZXkiOiIzMjdhMjIzMy0xNTE0LTQ2NTktYTZjMC05ZjJkOGI1ODMyMjEiLCJwZXJtaXNzaW9ucyI6WyJhbGxvd19qb2luIl0sImlhdCI6MTc0MDM5MzA5MywiZXhwIjoxNzQyOTg1MDkzfQ.cRVLPAPahw5fjGz_prfSPX98_X7Rxln6nVKlhAXic9A",

            containerId: 'vidBox', // Container where the video conference will be loaded

            micEnabled: admin === 'yes', // By default mic will be disabled
            webcamEnabled: admin === 'yes', // By default video will be disabled
            participantCanToggleSelfWebcam: admin === 'yes', // Allow users to toggle webcam devices
            participantCanToggleSelfMic: admin === 'yes', // Allow users to toggle mic devices

            chatEnabled: true, // Diable chat
            pollEnabled: true, // Disable polls
            raiseHandEnabled: true, // Disable polls
            moreOptionsEnabled: admin === 'yes',
            whiteboardEnabled: admin === 'yes',
            hlsEnabled: true,
            autoStartHls: true,

            screenShareEnabled: admin === 'yes',

            participantCanLeave: true, // Allow participant to leave the meeting
            participantCanToggleHls: true,

            waitingScreen: {
                imageUrl: "",
                text: "Loading...",
            },

            joinScreen: {
                visible: false, // Hide the join screen
                title: '<?php echo $webinar_id; ?>', // Meeting title
                meetingUrl: window.location.href, // Meeting joining url is current URL
            },

            hls: {
                enabled: true,
                autoStart: false,
                theme: "DARK", // DARK || LIGHT || DEFAULT
            },

            mode: "CONFERENCE", // VIEWER || CONFERENCE

            pin: {
                allowed: admin === 'yes', // participant can pin any participant in meeting
                layout: "SPOTLIGHT", // meeting layout - GRID | SPOTLIGHT | SIDEBAR
            },

            permissions: {
                pin: false, // Allow users to pin a speaker or participant
                endMeeting: admin === 'yes', // Disable users from ending the meeting
                changeLayout: admin === 'yes', // Allow users to change the layout
                toggleLivestream: admin === 'yes', // Disable live stream
                canCreatePoll: admin === 'yes', // Disable user from creating polls
                removeParticipant: admin === 'yes',
                toggleParticipantMode: admin === 'yes',
                toggleHls: admin === 'yes',
            },

            layout: {
                type: "SPOTLIGHT", // "SPOTLIGHT" | "SIDEBAR" | "GRID"
                priority: "PIN", // "SPEAKER" | "PIN",
                gridSize: 1,
            },


            notificationSoundEnabled: false, // Disable notification sounds as it might be annoying
            maxResolution: "hd", // Allow only SD videos to save bandwidth if needed
            joinWithoutUserInteraction: true, // Directly join the meeting without "Join the meeting" step
        };

        const meeting = new VideoSDKMeeting(); // Create the meeting object
        meeting.init(config); // Send all the configuration options we defined above
    });

    script.src = "<?php echo $assets ?>/js/rtc-js-prebuilt.js?ver=0.0.1"; // Load the prebuilt JS file
    document.getElementsByTagName("head")[0].appendChild(script); // Load the meeting
</script>
