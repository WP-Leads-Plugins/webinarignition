<?php
/**
 * @var $admin
 * @var $assets
 * @var $name
 * @var $webinar_id
 * @var $videosdk_api_key
 */
?>
<script>
    document.body.classList.add("videosdk_webinar_page");
    var admin = '<?php echo $admin; ?>';
    var script = document.createElement("script");
    script.type = "text/javascript";

    script.addEventListener("load", function (event) {
        const config = {
            name: '<?php echo $name; ?>',
            meetingId: '<?php echo $webinar_id; ?>',
            containerId: 'vidBox',
            // maxResolution: "hd",
            debug: false,

            joinWithoutUserInteraction: true,

            micEnabled: admin === 'yes',
            webcamEnabled: admin === 'yes',
            participantCanToggleSelfWebcam: true,
            participantCanToggleSelfMic: true,

            chatEnabled: true,
            pollEnabled: true,
            raiseHandEnabled: true,
            moreOptionsEnabled: true,
            whiteboardEnabled: admin === 'yes',
            screenShareEnabled: true,

            videoConfig: {
                resolution: "h1080p_w1920p", //h360p_w640p, h540p_w960p, h1080p_w1920p
                optimizationMode: "motion", // text , detail
                multiStream: true,
            },

            screenShareConfig: {
                resolution: "h1080p_15fps",
                optimizationMode: "text",
            },

            permissions: {
                toggleRecording: admin === 'yes',
                pin: admin === 'yes',
                endMeeting: admin === 'yes',
                canCreatePoll: admin === 'yes',
                removeParticipant: admin === 'yes',
                changeLayout: admin === 'yes',
                toggleParticipantWebcam: admin === 'yes',
                toggleParticipantMic: admin === 'yes',
                drawOnWhiteboard: admin === 'yes',
                toggleWhiteboard: admin === 'yes',
                toggleVirtualBackground: admin === 'yes',
                toggleParticipantMode: admin === 'yes',
                toggleHls: admin === 'yes',
            },

            layout: {
                type: "GRID", // "SPOTLIGHT" | "SIDEBAR" | "GRID"
                priority: "PIN", // "SPEAKER" | "PIN",
                gridSize: 6,
            },

            recording: {
                enabled: admin === 'yes',
                autoStart: false,
            },

            hls: {
                enabled: true,
                autoStart: false,
            },

            mode: admin === 'yes' ? "CONFERENCE" : "VIEWER",
        };

        <?php
        if (!empty($videosdk_token)) {
            ?>
            config.token = '<?php echo $videosdk_token; ?>';
            <?php
        } else {
            ?>
            config.apiKey = '<?php echo $videosdk_api_key; ?>';
            <?php
        }
        ?>

        const meeting = new VideoSDKMeeting();
        meeting.init(config);
    });

    script.src = "<?php echo $assets ?>/js/rtc-js-prebuilt.js?ver=0.0.1";
    document.getElementsByTagName("head")[0].appendChild(script);
</script>
