<?php
/*
Template Name: Full-width page layout
Template Post Type: ai_content_page
*/
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
wp_head();
the_content();
wp_footer();
