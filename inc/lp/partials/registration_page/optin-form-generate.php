<?php 

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * @var $webinar_data
 * @var $assets
 * @var $paid_check
 * @var $loginUrl
 * @var $user - Who is user
 */
?>

<?php

/**
 * The Below code is fully copied from auto-register.php
 * no changes made in functionality only improve the UI of the box that took verification code from the user
 *
 * If the login attribute contains false or something else
 * I will prefer the functionality of previous developers
 */
$email = '';
$user_full_name = '';
$user_first_name = '';
$user_last_name = '';
$webinar_user_email = '';

// Only get the required values from INPUT_GET
if ( isset( $_GET['login'] )&& isset( $_GET['e'] ) && wp_validate_boolean( $_GET['login'] ) ) { // phpcs:ignore
    $email = trim( sanitize_text_field( $_GET['e'] ) );
}

$webinar_user_email = ( isset( $_GET['email'] ) && ! empty( $_GET['email'] ) ) ? trim( sanitize_text_field( $_GET['email'] ) ) : '';
$user_full_name = ( isset( $_GET['first'] ) && ! empty( $_GET['first'] ) ) ? trim( sanitize_text_field( $_GET['first'] ) ) : '';
$user_full_name = is_email( $webinar_user_email ) ? $user_full_name : base64_decode( $user_full_name );
$webinar_user_email = is_email( $webinar_user_email ) ? $webinar_user_email : base64_decode( $webinar_user_email );
$user_full_name = $user_full_name != '' && isset($user_full_name) ? $user_full_name : (isset($_GET['n']) ? $_GET['n'] : '');


$order_id = WebinarignitionManager::webinarignition_is_paid_webinar( $webinar_data ) && 
            WebinarignitionManager::webinarignition_get_paid_webinar_type( $webinar_data ) === 'woocommerce' && 
            WebinarignitionManager::webinarignition_url_has_valid_wc_order_id();
$disable_reg_fields = false;
if ( $order_id ) {
    $disable_reg_fields = true;
	$paid_check = true;
    $user = WebinarignitionManager::webinarignition_get_user_from_wc_order_id();
} elseif ( is_user_logged_in() ) {
    $user = wp_get_current_user();
}
?>
<div class="optinFormArea optin-form-area" 
<?php
if ( 'no' === $paid_check ) {
	echo "style='display:none;'";}
?>
>
<?php
if ( ! empty( $user ) ) {
    $user_full_name  = $user->display_name;
    $user_first_name = $user->first_name;
    $user_last_name  = $user->last_name;
    if ( empty( $webinar_user_email ) ) {
        $webinar_user_email = $user->user_email;
    }
}
$user_first_name = $user_first_name != '' ? $user_first_name : $user_full_name;


//applicable for logged in users
if( is_user_logged_in() && current_user_can('activate_plugins') ) {
			

	// Manually format date and time
	$todays_date = gmdate('mdY'); // Format: mmddyyyy
	$todays_time = current_time('Hi');  // Format: 24-hour time without colon (e.g., 0000)

	// Get and sanitize the website URL
	$website_url = str_replace('http://', '', get_site_url()); // Remove http
	$website_url = str_replace('https://', '', $website_url);  // Remove https

	// Create a unique webinar user email using the sanitized time, date, and URL
	$webinar_user_email = "{$todays_time}-{$todays_date}@{$website_url}";
	$user_first_name = "{$todays_time}-{$todays_date}";
	$user_full_name = "{$todays_time}-{$todays_date}";
} else {
	$wi_new_regisration_tab = 'webhook_data_registration_url';
}

$WPreadOnlyMethod = 'wp_readonly';
if ( ! function_exists( $WPreadOnlyMethod ) ) {
    $WPreadOnlyMethod = 'readonly';
}
if ( isset( $_GET['n'] ) && ! empty( $_GET['n'] ) ) {
    $user_full_name = trim( sanitize_text_field( $_GET['n'] ) );
} elseif ( isset( $_GET['first'] ) && ! empty( $_GET['first'] ) ) {
    $user_full_name = trim( sanitize_text_field( $_GET['first'] ) );
}
$webinar_user_email = ( isset( $_GET['e'] ) && ! empty( $_GET['e'] ) ) ? trim( sanitize_text_field( $_GET['e'] ) ) : $webinar_user_email;
if ( ! empty( $webinar_data->ar_fields_order ) && is_array( $webinar_data->ar_fields_order ) ) {
    $alreadyAddedFields = array();
    $wi_showingGDPRHeading = false;

    foreach ( $webinar_data->ar_fields_order as $_field ) {
        if ( in_array( $_field, $alreadyAddedFields ) ) {
            continue;
        }
        $alreadyAddedFields[] = $_field;

        switch ( $_field ) {
            case 'ar_name':
                $required = ( isset( $webinar_data->ar_required_fields ) && is_array( $webinar_data->ar_required_fields ) && in_array( 'ar_name', $webinar_data->ar_required_fields ) ) ? true : false;

                if ( ! in_array( 'ar_lname', $webinar_data->ar_fields_order ) ) {
                    ?>
                    <div class="wiFormGroup wiFormGroup-lg">
                        <input type="text" 
                            class="radius fieldRadius wiRegForm wiFormControl <?php echo $required ? ' required' : ''; ?>" 
                            id="optName"
                            placeholder="<?php
                            webinarignition_display( $webinar_data->lp_optin_name, __( 'Enter Your Full Name...', 'webinar-ignition' ) );
                            echo $required ? '*' : '';
                            ?>"
                            value="<?php echo esc_html( $user_full_name ); ?>" 
                            autocomplete="off"
                        >
                    </div>
                    <?php
                } else {
                    ?>
                    <div class="wiFormGroup wiFormGroup-lg">
                        <input type="text" 
                            class="radius testclass fieldRadius wiRegForm optNamer wiFormControl <?php echo $required ? 'required' : ''; ?>" 
                            id="optFName" 
                            placeholder="<?php
                            webinarignition_display( $webinar_data->lp_optin_name, __( 'Enter Your First Name...', 'webinar-ignition' ) );
                            echo $required ? '*' : '';
                            ?>"
                            value="<?php echo esc_html( $user_first_name ); ?>" 
                            autocomplete="off"
                        >
                    </div>
                    <?php
                }
                break;
            case 'ar_lname':
                $required = ( isset( $webinar_data->ar_required_fields ) && is_array( $webinar_data->ar_required_fields ) && in_array( 'ar_lname', $webinar_data->ar_required_fields, true ) ) ? true : false;
                ?>
                <div class="wiFormGroup wiFormGroup-lg">
                    <input type="text" <?php $WPreadOnlyMethod( $disable_reg_fields, true, true ); ?> class="radius fieldRadius wiRegForm optNamer wiFormControl <?php echo $required ? 'required' : ''; ?>" id="optLName"
                            placeholder="<?php
                            webinarignition_display( $webinar_data->lp_optin_lname, __( 'Enter Your Last Name...', 'webinar-ignition' ) );
                            echo $required ? '*' : '';
                            ?>"
                            value="<?php echo esc_html( $user_last_name ); ?>" autocomplete="off" >
                </div>
                <input type="hidden" id="optName" value="#firstlast#">
                <?php
                break;
            case 'ar_email':
                // Checking if the current shortcode is autofill registration block.
                // Checking if the email is readonly.
                global $webinarignition_shortcode_params;

                /**
                 * The email readonly should check from url instead of from webinar data.
                 */
                $readonly_email = ( isset( $_GET['readonly'] ) && ! empty( $_GET['readonly'] ) ) ? 
				filter_var( trim( sanitize_text_field( $_GET['readonly'] ) ), 
				FILTER_VALIDATE_BOOLEAN ) : 
				false;

                // URL parameter ko check karen
				$readonly_email = false; // Default value
				if ( isset( $_GET['readonly'] ) && ! empty( $_GET['readonly'] ) ) {
					$readonly_email = filter_var( trim( sanitize_text_field( $_GET['readonly'] ) ), FILTER_VALIDATE_BOOLEAN );
					
				} elseif ( ! empty( $webinarignition_shortcode_params[ $webinar_data->id ] ) && 
						! empty( $webinarignition_shortcode_params[ $webinar_data->id ]['block'] ) && 
						isset( $webinarignition_shortcode_params[ $webinar_data->id ]['readonly'] ) ) {
					// Agar URL me value nahi hai, to shortcode ki value use karen
					$readonly_email = wp_validate_boolean( $webinarignition_shortcode_params[ $webinar_data->id ]['readonly'] );
				}
				
                ?>
                <div class="wiFormGroup wiFormGroup-lg">
                    <input type="email" 
                        class="radius fieldRadius wiRegForm wiFormControl" 
                        id="optEmail" 
                        placeholder="<?php
                        webinarignition_display(
                            $webinar_data->lp_optin_email,
                            __( 'Enter Your Best Email...', 'webinar-ignition' )
                        );
                        ?>*"
                        value="<?php echo esc_html( $webinar_user_email ); ?>" 
                        autocomplete="off"
                        required
						<?php echo $readonly_email || $disable_reg_fields ? 'readonly' : ''; ?>
                    >
                </div>
                <?php
                break;
            case 'ar_phone':
                ?>
                <div class="wiFormGroup wiFormGroup-lg">
                    <input type="tel" class="radius fieldRadius wiRegForm wi_phone_number wiFormControl <?php echo ( isset( $webinar_data->ar_required_fields ) && is_array( $webinar_data->ar_required_fields ) && in_array( 'ar_phone', $webinar_data->ar_required_fields, true ) ) ? ' required' : ''; ?>" id="optPhone"
                            placeholder="<?php
                            webinarignition_display( $webinar_data->lp_optin_phone, __( 'Enter Your Phone Number...', 'webinar-ignition' ) );
                            echo ( isset( $webinar_data->ar_required_fields ) && is_array( $webinar_data->ar_required_fields ) && in_array( 'ar_phone', $webinar_data->ar_required_fields, true ) ) ? '*' : '';
                            ?>"
                    >
                </div>
                <?php
                break;

				case 'ar_custom_1':
					?>
					<div class="wiFormGroup wiFormGroup-lg">
						<input type="text" class="radius fieldRadius wiRegForm wi_optCustom_1 wiFormControl <?php echo ( isset( $webinar_data->ar_required_fields ) && is_array( $webinar_data->ar_required_fields ) && in_array( 'ar_custom_1', $webinar_data->ar_required_fields, true ) ) ? ' required' : ''; ?>" id="optCustom_1"
								placeholder="<?php
								webinarignition_display( $webinar_data->lp_optin_custom_1, '' );
								echo ( isset( $webinar_data->ar_required_fields ) && is_array( $webinar_data->ar_required_fields ) && in_array( 'ar_custom_1', $webinar_data->ar_required_fields, true ) ) ? '*' : '';
								?>
								" >
					</div>
					<?php
					break;

				case 'ar_custom_2':
					?>
					<div class="wiFormGroup wiFormGroup-lg">
						<input type="text" class="radius fieldRadius wiRegForm wi_optCustom_2 wiFormControl <?php echo ( isset( $webinar_data->ar_required_fields ) && is_array( $webinar_data->ar_required_fields ) && in_array( 'ar_custom_2', $webinar_data->ar_required_fields, true ) ) ? 'required' : ''; ?>" id="optCustom_2"
								placeholder="<?php
								webinarignition_display( $webinar_data->lp_optin_custom_2, '' );
								echo ( isset( $webinar_data->ar_required_fields ) && is_array( $webinar_data->ar_required_fields ) && in_array( 'ar_custom_2', $webinar_data->ar_required_fields, true ) ) ? '*' : '';
								?>
								" >
					</div>
					<?php
					break;

				case 'ar_custom_3':
					?>
					<div class="wiFormGroup wiFormGroup-lg">
						<input type="text" class="radius fieldRadius wiRegForm wi_optCustom_3 wiFormControl <?php echo ( isset( $webinar_data->ar_required_fields ) && is_array( $webinar_data->ar_required_fields ) && in_array( 'ar_custom_3', $webinar_data->ar_required_fields, true ) ) ? 'required' : ''; ?>" id="optCustom_3"
								placeholder="<?php
								webinarignition_display( $webinar_data->lp_optin_custom_3, '' );
								echo ( isset( $webinar_data->ar_required_fields ) && is_array( $webinar_data->ar_required_fields ) && in_array( 'ar_custom_3', $webinar_data->ar_required_fields, true ) ) ? '*' : '';
								?>
								" >
					</div>
					<?php
					break;

				case 'ar_custom_4':
					?>
					<div class="wiFormGroup wiFormGroup-lg">
						<input type="text" class="radius fieldRadius wiRegForm wi_optCustom_4 wiFormControl <?php echo ( isset( $webinar_data->ar_required_fields ) && is_array( $webinar_data->ar_required_fields ) && in_array( 'ar_custom_4', $webinar_data->ar_required_fields ) ) ? 'required' : ''; ?>" id="optCustom_4"
								placeholder="<?php
								webinarignition_display( $webinar_data->lp_optin_custom_4, '' );
								echo ( isset( $webinar_data->ar_required_fields ) && is_array( $webinar_data->ar_required_fields ) && in_array( 'ar_custom_4', $webinar_data->ar_required_fields ) ) ? '*' : '';
								?>
								" >
					</div>
					<?php
					break;

				case 'ar_custom_5':
				case 'ar_custom_6':
					$index               = str_replace( 'ar_custom_', '', $_field );
					$options_setting_str = 'lp_optin_custom_' . $index;
					$is_required         = isset( $webinar_data->ar_required_fields ) && is_array( $webinar_data->ar_required_fields ) && in_array( $_field, $webinar_data->ar_required_fields );
					?>
					<div class="customFieldDiv wiFormCheckbox wiFormCheckbox-lg">
						<label for="optCustom_<?php echo absint( $index ); ?>"><?php echo esc_html( $webinar_data->{$options_setting_str} ); ?><?php echo $is_required ? ' *' : ''; ?></label>
						<input type="checkbox" id="optCustom_<?php echo absint( $index ); ?>" class="wiRegForm wi_optCustom_<?php echo absint( $index ); ?><?php echo $is_required ? ' required' : ''; ?>" <?php echo $is_required ? ' required' : ''; ?>>
					</div>
					<?php
					break;

				case 'ar_custom_7':
					?>
					<div class="customFieldDiv wiFormGroup wiFormGroup-lg">
						<label for="optCustom_7"><?php echo wp_kses_post( $webinar_data->lp_optin_custom_7 ); ?></label>
						<textarea id="optCustom_7" class="wiRegForm wi_optCustom_7 wiFormControl 
						<?php echo ( isset( $webinar_data->ar_required_fields ) && is_array( $webinar_data->ar_required_fields ) && in_array( 'ar_custom_7', $webinar_data->ar_required_fields, true ) ) ? 'required' : ''; ?>" 
						placeholder="<?php echo wp_kses_post( $webinar_data->lp_optin_custom_7 ); ?><?php echo ( isset( $webinar_data->ar_required_fields ) && is_array( $webinar_data->ar_required_fields ) && in_array( 'ar_custom_7', $webinar_data->ar_required_fields, true ) ) ? ' (Required)' : ''; ?>" 
						rows="4" cols="50"></textarea>
					</div>
					<?php
					break;

				case 'ar_custom_15':
				case 'ar_custom_16':
				case 'ar_custom_17':
				case 'ar_custom_18':
					$index               = str_replace( 'ar_custom_', '', $_field );
					$label_setting       = 'lp_optin_custom_' . $index;
					$options_setting_str = 'lp_optin_custom_select_' . $index;
					$options_setting     = $webinar_data->{$options_setting_str};
					$options_setting2    = $webinar_data->{$options_setting_str};
					$is_required         = isset( $webinar_data->ar_required_fields ) && is_array( $webinar_data->ar_required_fields ) && in_array( $_field, $webinar_data->ar_required_fields, true );

					if ( ! empty( trim( $options_setting ) ) ) {
						$options_array = explode( "\n", $options_setting );
						?>
						<div class="customFieldDiv customFieldSelectDiv wiFormGroup wiFormGroup-lg">
							<label for="optCustom_<?php echo absint( $index ); ?>">
								<span><?php echo wp_kses_post( $webinar_data->{$label_setting} ); ?></span>
								<?php echo $is_required ? ' <strong>(' . esc_html__( 'Required', 'webinar-ignition' ) . ')</strong>' : ''; ?>
							</label>

							<select
								id="optCustom_<?php echo absint( $index ); ?>"
								class="wiRegForm wi_optCustom_<?php echo absint( $index ); ?> wiFormControl<?php echo $is_required ? ' required' : ''; ?>"
							>
								<?php
								foreach ( $options_array as $option ) {
									$option_array = explode( '::', $option );

									if ( 1 === count( $option_array ) ) {
										$value = trim( $option_array[0] );
										$label = trim( $option_array[0] );
									} else {
										$value = trim( $option_array[0] );
										$label = trim( $option_array[1] );
									}
									?>
									<option value="<?php echo esc_attr( $value ); ?>"><?php echo wp_kses_post( $label ); ?></option>
									<?php
								}
								?>
							</select>
						</div>
						<?php
					}
					break;

				case 'ar_privacy_policy':
					$is_required = isset( $webinar_data->ar_required_fields ) && is_array( $webinar_data->ar_required_fields ) && in_array( $_field, $webinar_data->ar_required_fields );
					webinarignition_showGDPRHeading( $webinar_data );
					?>
					<div class="gdprConsentField gdpr-pp wiFormCheckbox wiFormCheckbox-sm">
						<label for="gdpr-pp"><?php echo ! empty( $webinar_data->lp_optin_privacy_policy ) ? wp_kses_post( $webinar_data->lp_optin_privacy_policy ) : esc_html__( 'Have read and understood our Privacy Policy', 'webinar-ignition' ); ?><?php echo $is_required ? ' *' : ''; ?></label>
						<input type="checkbox" name="optGDPR_PP" id="gdpr-pp" <?php echo $is_required ? ' required' : ''; ?> class="<?php echo $is_required ? 'required' : ''; ?>" style="margin:4px 0 -5px;">
					</div>
					<?php
					break;
				case 'ar_terms_and_conditions':
					$is_required = isset( $webinar_data->ar_required_fields ) && is_array( $webinar_data->ar_required_fields ) && in_array( $_field, $webinar_data->ar_required_fields, true );
					webinarignition_showGDPRHeading( $webinar_data );
					?>
					<div class="gdprConsentField gdpr-tc wiFormCheckbox wiFormCheckbox-sm">
						<label for="gdpr-tc"><?php echo ! empty( $webinar_data->lp_optin_terms_and_conditions ) ? wp_kses_post( $webinar_data->lp_optin_terms_and_conditions ) : esc_html__( 'Accept our Terms &amp; Conditions', 'webinar-ignition' ); ?><?php echo $is_required ? ' *' : ''; ?></label>
						<input type="checkbox" name="optGDPR_TC" id="gdpr-tc" <?php echo $is_required ? ' required' : ''; ?> class="<?php echo $is_required ? 'required' : ''; ?>" style="margin:4px 0 -5px;">
					</div>
					<?php
					break;
				case 'ar_mailing_list':
					$is_required = isset( $webinar_data->ar_required_fields ) && is_array( $webinar_data->ar_required_fields ) && in_array( $_field, $webinar_data->ar_required_fields, true );
					webinarignition_showGDPRHeading( $webinar_data );
					?>
					<div class="gdprConsentField gdpr-ml wiFormCheckbox wiFormCheckbox-sm">
						<label for="gdpr-ml"><?php echo ! empty( $webinar_data->lp_optin_mailing_list ) ? wp_kses_post( $webinar_data->lp_optin_mailing_list ) : esc_html__( 'Want to be added to our mailing list', 'webinar-ignition' ); ?><?php echo $is_required ? ' *' : ''; ?></label>
						<input type="checkbox" name="optGDPR_ML" id="gdpr-ml" <?php echo $is_required ? ' required' : ''; ?> class="<?php echo $is_required ? 'required' : ''; ?>" style="margin:4px 0 -5px;">
					</div>
					<?php
					break;

            default:
                break;
        }
    }

    webinarignition_closeGDPRSection();
}

if ( empty( $webinar_data->lp_optin_button ) || 'color' === trim( $webinar_data->lp_optin_button ) ) {
	switch_to_locale( $webinar_data->webinar_lang );
	unload_textdomain( 'webinar-ignition' );
	load_textdomain( 'webinar-ignition', WEBINARIGNITION_PATH . 'languages/webinar-ignition-' . $webinar_data->webinar_lang . '.mo' );
	$btn_color = isset($webinar_data->lp_optin_btn_color) && $webinar_data->lp_optin_btn_color !== '' ? $webinar_data->lp_optin_btn_color : '#74BB00';
	$color_array = webinarignition_btn_color($btn_color);
	$hover_color = $color_array['hover_color'];
	$text_color = $color_array['text_color'];
    ?>
	<button href="#" id="optinBTN" class="large button wiButton wiButton-block wiButton-lg addedArrow"
    style="background-color: <?php echo esc_attr($btn_color); ?> !important; color: <?php echo esc_attr($text_color); ?>;"
    onmouseover="this.style.backgroundColor='<?php echo esc_attr($hover_color); ?>'"
    onmouseout="this.style.backgroundColor='<?php echo esc_attr($btn_color); ?>' !important;">
    <span id="optinBTNText">
        <?php echo isset($webinar_data->lp_optin_btn) ? wp_kses_post(webinarignition_display( $webinar_data->lp_optin_btn, esc_html( __( 'Register For The Webinar', 'webinar-ignition' ) ) )) : esc_html( __( 'Register For The Webinar', 'webinar-ignition' ) ); ?>
    </span>
    <span id="optinBTNLoading" style="display: none;">
        <img src="<?php echo esc_url( WEBINARIGNITION_URL . 'inc/lp/images/loading_dots_cropped_small.gif' ); ?>" style="width: auto; height: 20px;"/>
    </span>
</button>

    <?php
} else {
    ?>
    <a href="#" id="optinBTN" class="optinBTN optinBTNimg">
        <img src="<?php echo esc_url( $webinar_data->lp_optin_btn_image ); ?>" width="327" border="0"/>
    </a>
    <?php
}
?>
<div class="spam wiSpamMessage">
    <?php echo isset($webinar_data->lp_optin_spam) ? wp_kses_post( webinarignition_display( $webinar_data->lp_optin_spam, esc_html( __( '* Your data is safe with us *', 'webinar-ignition' ) ) ) ) : esc_html( __( '* Your data is safe with us *', 'webinar-ignition' ) ); ?>
</div>
<?php if ( get_option( 'webinarignition_show_footer_branding' ) ) { ?>
    <div class="powered_by_text_wrap" style="margin-top: 15px;"><a href="<?php echo esc_url( get_option( 'webinarignition_affiliate_link' ) ); ?>"  target="_blank"><b><?php echo esc_html( get_option( 'webinarignition_branding_copy' ) ); ?></b></a> </div>
<?php } 
		restore_previous_locale();

?>
</div>
