<?php
/**
 * @var $webinar_data
 * @var $uid
 */
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
?>

<div class="eventDate fixed-type <?php echo esc_attr( $uid ); ?>">
	<?php

	if (preg_match('/UTC([+-]\d+(\.\d+)?)/', $webinar_data->auto_timezone_fixed, $matches)) {
        // Convert offset to valid "+HH:MM" format
        $offset = floatval($matches[1]);
        $hours = floor($offset);
        $minutes = ($offset - $hours) * 60;
        $timezone_offset = sprintf('%+03d:%02d', $hours, abs($minutes));
    } else {

        // Assume the input is already a valid timezone identifier
        $timezone_offset = $webinar_data->auto_timezone_fixed;
    }

    // Create a DateTimeZone object with the validated or transformed timezone
    $dateTimeZone = new DateTimeZone($timezone_offset);
    $dateTime = new DateTime();
    $dateTime->setTimeZone($dateTimeZone);
	$tz_abbr     = $dateTime->format( 'T' );
	$tz_name     = $dateTime->getTimezone()->getName();
	$tz_abbr     = $tz_name.' ('.$tz_abbr.') ';

	$split_date = !empty($webinar_data->auto_date_fixed) ? explode('-', $webinar_data->auto_date_fixed) : [];
	$date_format = ! empty( $webinar_data->date_format ) ? $webinar_data->date_format : 'l, F j, Y';
	$auto_date   = webinarignition_get_translated_date( $webinar_data->auto_date_fixed, 'Y-m-d', $date_format );
	?>
	<img src="<?php echo esc_url( WEBINARIGNITION_URL . 'images/date_crystal.png' ); ?>"/>
	<span style="font-size: 18px"><?php echo esc_html( $auto_date ); ?></span>
	<img src="<?php echo esc_url( WEBINARIGNITION_URL . 'images/clock.png' ); ?>"/>
	<span style="font-size: 18px"><?php echo esc_html( $webinar_data->auto_time_fixed . ' ' . $tz_abbr ); ?></span>
	<input type="hidden" id="webinar_start_date"
			value="<?php echo isset($split_date[2]) ? esc_html($split_date[2]) : ''; ?>-<?php echo isset($split_date[0]) ? esc_html($split_date[0]) : ''; ?>-<?php echo isset($split_date[1]) ? esc_html($split_date[1]) : ''; ?>"/>
	<input type="hidden" id="webinar_start_time" value="<?php echo esc_html( $webinar_data->auto_time_fixed ); ?>"/>
	<input type="hidden" id="timezone_user" value="<?php echo esc_html( $webinar_data->auto_timezone_fixed ); ?>">
	<input type="hidden" id="today_date" value="<?php echo esc_html( gmdate( 'Y-m-d' ) ); ?>">
	<br clear="left"/>
</div>
