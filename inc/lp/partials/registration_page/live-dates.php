<?php
/**
 * @var $webinar_data
 * @var $uid
 * @var $liveEventMonth
 * @var $liveEventDateDigit
 * @var $autoDate_format
 * @var $autoTime
 * @var $is_compact
 */

 if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

?>
<div class="eventDate <?php echo esc_attr( $uid ); ?>">
	<div class="dateIcon">
		<div class="dateMonth">
			<?php echo esc_html( $localized_month ); ?>
		</div>
		<div class="dateDay">
			<?php
			echo esc_html( $webinarDateObject->format( 'd' ) );
			echo ( 'en' === substr( get_locale(), 0, 2 ) ) ? '' : '.';
			?>
		</div>

			<div class="dateDayWeek">
				<?php echo esc_html( $localized_week_day ); ?>
			</div>
	</div>

	<?php
	if ( ! $is_compact ) {
		if (preg_match('/UTC([+-]\d+(\.\d+)?)/', $webinar_data->webinar_timezone, $matches)) {
			// Convert offset to valid "+HH:MM" format
			$offset = floatval($matches[1]);
			$hours = floor($offset);
			$minutes = ($offset - $hours) * 60;
			$timezone_offset = sprintf('%+03d:%02d', $hours, abs($minutes));
		} else {
	
			// Assume the input is already a valid timezone identifier
			$timezone_offset = $webinar_data->webinar_timezone;
		}
		if (!in_array($timezone_offset, timezone_identifiers_list())) {
			error_log("Invalid timezone: " . $timezone_offset);
			$timezone_offset = 'UTC'; // Fallback to UTC
		}
		$dateTimeZone = new DateTimeZone($timezone_offset);		$dateTime = new DateTime();
		$dateTime->setTimeZone($dateTimeZone);
		$tz_abbr     = $dateTime->format( 'T' );
		$tz_name     = $dateTime->getTimezone()->getName();
		if ( ! empty( $webinar_data->display_tz ) && ( $webinar_data->display_tz == 'yes' ) ) {
			$tz_abbr     = $tz_name.' ('.$tz_abbr.') ';
		}else{
			$tz_abbr     = '';
		}
		
		?>
		<div class="dateInfo">
			<div class="dateHeadline"><?php echo esc_html( $localized_date ); ?></div>
			<div class="dateSubHeadline">
				<?php
				if ( $webinar_data->lp_webinar_subheadline ) {
					echo esc_html( $webinar_data->lp_webinar_subheadline );
				} else {
					echo esc_html__( 'At', 'webinar-ignition' ) . ' ' . esc_html( gmdate( $time_format, strtotime( $webinar_data->webinar_start_time ) ) ) . '  '.esc_attr( $tz_abbr );
				}
				?>
			</div>
		</div>
		<?php
	}
	?>
	<br clear="left"/>
</div>
