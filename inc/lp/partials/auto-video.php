<?php
/**
 * @var $webinar_data
 * @var $is_preview
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
$main = false;

$timeover = false;

if ( isset( $_GET['lid'] ) ) {
	$lead_id = sanitize_text_field( $_GET['lid'] );
	if(webinarignition_fs()->is__premium_only()){

		wp_enqueue_script( 'limit-custom-video' );
		wp_localize_script(
			'limit-custom-video',
			'lcv_php_var',
			array(
				'ajax_url' => admin_url( 'admin-ajax.php' ),
				'nonce'    => wp_create_nonce( 'limit-custom-video' ),
				'lead_id'  => $lead_id,
			)
		);
    }
	$watch_time = get_option( 'wi_lead_watch_time_' . $lead_id, true );

	if(webinarignition_fs()->is__premium_only()){
		$watch_limit = 60 * 45;
		$statusCheck = WebinarignitionLicense::webinarignition_get_license_level(); // Added premium check
		$site_url = get_site_url(); 
		$statusCheck = new stdClass();

		$statusCheck->switch = 'free';
		$statusCheck->slug = 'free';
		$statusCheck->licensor = '';
		$statusCheck->is_free = 1;
		$statusCheck->is_dev = '';
		$statusCheck->is_registered = '';
		$statusCheck->title = 'Free';
		$statusCheck->member_area = '';
		$statusCheck->is_pending_activation = 1;
		$statusCheck->upgrade_url = $site_url. '/wp-admin/admin.php?billing_cycle=annual&page=webinarignition-dashboard-pricing';
		$statusCheck->trial_url = $site_url.'/wp-admin/admin.php?billing_cycle=annual&trial=true&page=webinarignition-dashboard-pricing';
		$statusCheck->reconnect_url = $site_url.'/wp-admin/admin.php?nonce=fc5eb326b0&fs_action=webinar-ignition_reconnect&page=webinarignition-dashboard';
		$statusCheck->account_url = $site_url.'/wp-admin/admin.php?page=webinarignition-dashboard-account';
		$statusCheck->name = '';
		$license_level = WebinarignitionLicense::webinarignition_get_license_level(); // Added premium check
		$statusCheck = WebinarignitionLicense::webinarignition_get_license_level(); // Added premium check

	}
	else{
		$site_url = get_site_url(); 
		$statusCheck = new stdClass();

		$statusCheck->switch = 'free';
		$statusCheck->slug = 'free';
		$statusCheck->licensor = '';
		$statusCheck->is_free = 1;
		$statusCheck->is_dev = '';
		$statusCheck->is_registered = '';
		$statusCheck->title = 'Free';
		$statusCheck->member_area = '';
		$statusCheck->is_pending_activation = 1;
		$statusCheck->upgrade_url = $site_url. '/wp-admin/admin.php?billing_cycle=annual&page=webinarignition-dashboard-pricing';
		$statusCheck->trial_url = $site_url.'/wp-admin/admin.php?billing_cycle=annual&trial=true&page=webinarignition-dashboard-pricing';
		$statusCheck->reconnect_url = $site_url.'/wp-admin/admin.php?nonce=fc5eb326b0&fs_action=webinar-ignition_reconnect&page=webinarignition-dashboard';
		$statusCheck->account_url = $site_url.'/wp-admin/admin.php?page=webinarignition-dashboard-account';
		$statusCheck->name = '';
	}
	if(webinarignition_fs()->is__premium_only()){

		if ( 'ultimate_powerup_tier1a' === $statusCheck->name ) {
			$watch_limit = 60 * 45;
		} 


		if ( is_plugin_active( 'webinar-ignition-helper/webinar-ignition-helper.php' ) ) {
			$watch_limit = 300;
		}

		if ( absint( $watch_time ) >= $watch_limit && $license_level->name === 'ultimate_powerup_tier1a' ) {
			$timeover = true;
		}
	}
}//end if
?>

<?php if ( ! $timeover ) : 
$show_controls = ! empty( $webinar_data->webinar_show_videojs_controls ) || ! empty( $is_preview );
?>

<video disablePictureInPicture 
		<?php 
			
				echo $show_controls ? 'controls' : ''; 
			
		?> 
		id="autoReplay" class="video-js vjs-default-skin">
	<source src="<?php echo esc_url( $webinar_data->auto_video_url ); ?>" type='video/mp4'/>
	<source src="<?php echo esc_url( $webinar_data->auto_video_url2 ); ?>" type="video/webm"/>
</video>

<input type="hidden" id="autoVideoTime">
<?php else : ?>
	<h3>
	<?php
		if(webinarignition_fs()->is__premium_only()){
			$watch_time_limit_string = __( '45 Minutes', 'webinar-ignition' );
			$statusCheck = WebinarignitionLicense::webinarignition_get_license_level(); // Added premium check
			$site_url = get_site_url(); 
			$statusCheck = new stdClass();

			$statusCheck->switch = 'free';
			$statusCheck->slug = 'free';
			$statusCheck->licensor = '';
			$statusCheck->is_free = 1;
			$statusCheck->is_dev = '';
			$statusCheck->is_registered = '';
			$statusCheck->title = 'Free';
			$statusCheck->member_area = '';
			$statusCheck->is_pending_activation = 1;
			$statusCheck->upgrade_url = $site_url. '/wp-admin/admin.php?billing_cycle=annual&page=webinarignition-dashboard-pricing';
			$statusCheck->trial_url = $site_url.'/wp-admin/admin.php?billing_cycle=annual&trial=true&page=webinarignition-dashboard-pricing';
			$statusCheck->reconnect_url = $site_url.'/wp-admin/admin.php?nonce=fc5eb326b0&fs_action=webinar-ignition_reconnect&page=webinarignition-dashboard';
			$statusCheck->account_url = $site_url.'/wp-admin/admin.php?page=webinarignition-dashboard-account';
			$statusCheck->name = '';
		}
		else{
			$site_url = get_site_url(); 
			$statusCheck = new stdClass();

			$statusCheck->switch = 'free';
			$statusCheck->slug = 'free';
			$statusCheck->licensor = '';
			$statusCheck->is_free = 1;
			$statusCheck->is_dev = '';
			$statusCheck->is_registered = '';
			$statusCheck->title = 'Free';
			$statusCheck->member_area = '';
			$statusCheck->is_pending_activation = 1;
			$statusCheck->upgrade_url = $site_url. '/wp-admin/admin.php?billing_cycle=annual&page=webinarignition-dashboard-pricing';
			$statusCheck->trial_url = $site_url.'/wp-admin/admin.php?billing_cycle=annual&trial=true&page=webinarignition-dashboard-pricing';
			$statusCheck->reconnect_url = $site_url.'/wp-admin/admin.php?nonce=fc5eb326b0&fs_action=webinar-ignition_reconnect&page=webinarignition-dashboard';
			$statusCheck->account_url = $site_url.'/wp-admin/admin.php?page=webinarignition-dashboard-account';
			$statusCheck->name = '';
		}
		if(webinarignition_fs()->is__premium_only()){

			if ( 'ultimate_powerup_tier1a' === $statusCheck->name ) {
				$watch_time_limit_string = __( '2 Hours', 'webinar-ignition' );
			}

			printf(
				/* translators: %s: Watch time limit (e.g. '45 Minutes' or '2 Hours') */
				esc_html__( 'You have availed %s view time. Webinar is closed for you.', 'webinar-ignition' ),
				esc_html( $watch_time_limit_string )
			);
		?>
			</h3>
<?php 	} endif; ?>
