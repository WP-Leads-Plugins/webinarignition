<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
$cta_index = isset( $index ) ? absint( $index ) : 0;
$cta_iframe = '';
$cta_iframe_sc = '';
$cta_content = '';

if ( 0 === $cta_index ) { // Main CTA

	if ( isset( $webinar_data->cta_iframe ) ) {
		$cta_iframe = $webinar_data->cta_iframe;
	}

	if ( isset( $webinar_data->cta_iframe_sc ) ) {
		$cta_iframe_sc = $webinar_data->cta_iframe_sc;
	}

	if ( isset( $webinar_data->auto_action_copy ) ) {
		$cta_content = $webinar_data->auto_action_copy;
	}
} else { // Additional CTA

	if ( isset( $additional_autoaction['cta_iframe'] ) ) {
		$cta_iframe = $additional_autoaction['cta_iframe'];
	}

	if ( isset( $additional_autoaction['cta_iframe_sc'] ) ) {
		$cta_iframe_sc = $additional_autoaction['cta_iframe_sc'];
	}

	if ( isset( $additional_autoaction['auto_action_copy'] ) ) {
		$cta_content = $additional_autoaction['auto_action_copy'];
	}
}//end if

$cta_iframe = strtolower( $cta_iframe );
$cta_iframe_sc = wp_kses_post( $cta_iframe_sc );
$cta_content = stripcslashes( wpautop( $cta_content ) );

if(webinarignition_fs()->is__premium_only()){
	$statusCheck = WebinarignitionLicense::webinarignition_get_license_level(); // Added premium check
	$site_url = get_site_url(); 
	$statusCheck = new stdClass();

	$statusCheck->switch = 'free';
	$statusCheck->slug = 'free';
	$statusCheck->licensor = '';
	$statusCheck->is_free = 1;
	$statusCheck->is_dev = '';
	$statusCheck->is_registered = '';
	$statusCheck->title = 'Free';
	$statusCheck->member_area = '';
	$statusCheck->is_pending_activation = 1;
	$statusCheck->upgrade_url = $site_url. '/wp-admin/admin.php?billing_cycle=annual&page=webinarignition-dashboard-pricing';
	$statusCheck->trial_url = $site_url.'/wp-admin/admin.php?billing_cycle=annual&trial=true&page=webinarignition-dashboard-pricing';
	$statusCheck->reconnect_url = $site_url.'/wp-admin/admin.php?nonce=fc5eb326b0&fs_action=webinar-ignition_reconnect&page=webinarignition-dashboard';
	$statusCheck->account_url = $site_url.'/wp-admin/admin.php?page=webinarignition-dashboard-account';
	$statusCheck->name = '';
}
else{
	$site_url = get_site_url(); 
	$statusCheck = new stdClass();

	$statusCheck->switch = 'free';
	$statusCheck->slug = 'free';
	$statusCheck->licensor = '';
	$statusCheck->is_free = 1;
	$statusCheck->is_dev = '';
	$statusCheck->is_registered = '';
	$statusCheck->title = 'Free';
	$statusCheck->member_area = '';
	$statusCheck->is_pending_activation = 1;
	$statusCheck->upgrade_url = $site_url. '/wp-admin/admin.php?billing_cycle=annual&page=webinarignition-dashboard-pricing';
	$statusCheck->trial_url = $site_url.'/wp-admin/admin.php?billing_cycle=annual&trial=true&page=webinarignition-dashboard-pricing';
	$statusCheck->reconnect_url = $site_url.'/wp-admin/admin.php?nonce=fc5eb326b0&fs_action=webinar-ignition_reconnect&page=webinarignition-dashboard';
	$statusCheck->account_url = $site_url.'/wp-admin/admin.php?page=webinarignition-dashboard-account';
	$statusCheck->name = '';
}
$webinar_id = absint( $webinar_data->id );
if ( webinarignition_fs()->is__premium_only()  ){ 
	$statusCheck = WebinarignitionLicense::webinarignition_get_license_level();
	$cta_content  = apply_filters( 'webinarignition_additional_cta_content', $cta_content );
	if ( class_exists( 'advancediFrame' ) && 'yes' === $cta_iframe ) {

		$cta_content .= webinarignition_get_cta_aiframe_sc( $webinar_id, $cta_index, $cta_iframe_sc );
		$cta_content = apply_filters( 'ai_handle_temp_pages', $cta_content );
	}
	if ( $statusCheck && 'ultimate_powerup_tier3a' === $statusCheck->name && webinarignition_fs()->can_use_premium_code() ) { 
	}else{
		// Remove <iframe> tags for security purpose
		$cta_content = preg_replace('/<iframe[^>]*>.*?<\/iframe>/is', '', $cta_content);

		// Remove [advanced_iframe] shortcode for security purpose
		$cta_content = preg_replace('/\[advanced_iframe[^\]]*\]/i', '', $cta_content);
	}
}else{
	$cta_content  = apply_filters( 'webinarignition_additional_cta_content', $cta_content );
	if ( class_exists( 'advancediFrame' ) && 'yes' === $cta_iframe ) {

		$cta_content .= webinarignition_get_cta_aiframe_sc( $webinar_id, $cta_index, $cta_iframe_sc );
		$cta_content = apply_filters( 'ai_handle_temp_pages', $cta_content );
	}
	// Remove <iframe> tags for security purpose
	$cta_content = preg_replace('/<iframe[^>]*>.*?<\/iframe>/is', '', $cta_content);

	// Remove [advanced_iframe] shortcode for security purpose
	$cta_content = preg_replace('/\[advanced_iframe[^\]]*\]/i', '', $cta_content);
}
echo do_shortcode( $cta_content );
