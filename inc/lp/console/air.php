<!-- ON AIR AREA -->
<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>
<div id="onairTab" style="display:none;" class="consoleTabs">
	<div class="statsDashbord">
		<div class="statsTitle statsTitle-Air">
			<div class="statsTitleIcon">
				<i class="icon-microphone icon-2x"></i>
			</div>

			<div class="statsTitleCopy">
				<h2><?php esc_html_e( 'On Air', 'webinar-ignition' ); ?></h2>
				<p><?php esc_html_e( 'Manage the live broadcasting message to live viewers...', 'webinar-ignition' ); ?></p>
			</div>

			<br clear="left" />
		</div>
	</div>

	<div class="innerOuterContainer">
		<div class="innerContainer">
			<div class="airSwitch">
				<div class="airSwitchLeft">
					<span class="airSwitchTitle"><?php esc_html_e( 'On Air Broadcast Switch', 'webinar-ignition' ); ?></span>
					<span class="airSwitchInfo"><?php esc_html_e( 'If set to ON, the message/html below will appear under the webinar (instantly) for people on the webinar...', 'webinar-ignition' ); ?></span>
				</div>

				<div class="airSwitchRight">
					<p class="field switch">
						<input type="hidden" id="airToggle" value="
						<?php
						if ( ! isset( $webinar_data->air_toggle ) || empty( $webinar_data->air_toggle ) || 'on' === $webinar_data->air_toggle ) {
							echo 'on';
						} else {
							echo esc_html( $webinar_data->air_toggle );
						}
						?>
						">
						<label for="radio1" class="cb-enable 
						<?php
						if ( ! isset( $webinar_data->air_toggle ) || empty( $webinar_data->air_toggle ) || $webinar_data->air_toggle == 'on' ) {
							echo 'selected';
						}
						?>
						"><span><?php esc_html_e( 'On', 'webinar-ignition' ); ?></span></label>
						<label for="radio2" class="cb-disable 
						<?php
						if ( isset( $webinar_data->air_toggle ) && $webinar_data->air_toggle == 'off' ) {
							echo 'selected';
						}
						?>
						"><span><?php esc_html_e( 'Off', 'webinar-ignition' ); ?></span></label>
					</p>
				</div>

				<br clear="all" />
			</div>

		  

			<!-- Ameilia Switch -->
			<div class="airSwitch">
				<div class="airSwitchLeft">
					<span class="airSwitchTitle"><?php esc_html_e( 'Display CTA in iFrame', 'webinar-ignition' ); ?></span>
					<?php
					// check if the iframe plugin is activated or not to display message to user.
					if ( is_plugin_active( 'advanced-iframe/advanced-iframe.php' ) || is_plugin_active( 'advanced-iframe-pro/advanced-iframe-pro.php' ) ) {
						?>
				<span class="airSwitchInfo"><?php esc_html_e( 'Display your CTA contents in Iframe using Advanced Iframe plugin.', 'webinar-ignition' ); ?></span>
						<?php
					} else {
						$advanced_iframe_url = sprintf( '
								<a href="%s" target="_blank">%s</a>',
								esc_url( self_admin_url( 'plugin-install.php?tab=plugin-information&plugin=advanced-iframe' ) ), 
								esc_html__( 'Advanced iFrame', 'webinar-ignition' ) 
							);
						?>
				<span class="airSwitchInfo">
					<?php
					printf(
						/* translators: %s: URL to Advanced iFrame plugin */
						esc_html__( 'Does your CTA content contain scripts or does it not load and look good? Then you can display your CTA content in an Iframe, to enable this feature you need to install and activate the free "%s" plugin.', 'webinar-ignition' ),
						wp_kses( $advanced_iframe_url, array( 'a' => array( 'href' => array(), 'target' => array() ) ) )
					);
					?>
				</span>
						<?php
						// Advanced iFrame plugin is not activated
					}
					?>
					
				</div>
			<?php
			if ( is_plugin_active( 'advanced-iframe/advanced-iframe.php' ) || is_plugin_active( 'advanced-iframe-pro/advanced-iframe-pro.php' ) ) {
				?>
				<div class="airSwitchRight">
					<p class="field switch ameliaSwitch">
						<input type="hidden" id="isAdvaceIframeActive" value="1" />
						<input type="hidden" id="airAmeliaToggle" value="<?php if ( ! isset( $webinar_data->air_amelia_toggle ) || empty( $webinar_data->air_amelia_toggle ) || 'on' === $webinar_data->air_toggle ) {echo 'on'; } else { echo esc_html( $webinar_data->air_amelia_toggle );}?>">
						<label for="radio1" class="cb-enable 
						<?php
						if ( ! isset( $webinar_data->air_amelia_toggle ) || empty( $webinar_data->air_amelia_toggle ) || 'on' === $webinar_data->air_amelia_toggle ) {
							echo 'selected';
						}
						?>
						"><span><?php esc_html_e( 'On', 'webinar-ignition' ); ?></span></label>
						<label for="radio2" class="cb-disable 
						<?php
						if ( isset( $webinar_data->air_amelia_toggle ) && $webinar_data->air_amelia_toggle == 'off' ) {
							echo 'selected';
						}
						?>
						"><span><?php esc_html_e( 'Off', 'webinar-ignition' ); ?></span></label>
					</p>
				</div>
				<?php
			}else{
				?>
				<input type="hidden" id="isAdvaceIframeActive" value="0" />
				<?php
			}
			?>

				<br clear="all" />
			</div>

			<div id="wi-notification-overlay" class="wi-notification-overlay wi-hidden"></div>
			<div id="wi-notification-box" class="wi-hidden">
				<div class="wi-notification-content">
	<span id="wi-notification-message"></span>
	<button id="wi-close-notification">Close</button>
	</div>
</div>

			<div class="airExtraOptions">
				<span class="airSwitchTitle"><?php esc_html_e( 'CTA Position', 'webinar-ignition' ); ?></span>
				<span class="airSwitchInfo"><?php esc_html_e( 'If you select overlay, CTA section will cover your webinar video.', 'webinar-ignition' ); ?></span>
				<div class="PositionRadiosForLiveWebinarCTAs">
					<input class="live_webinar_ctas_position_radios" <?php echo isset( $webinar_data->live_webinar_ctas_position_radios ) ? checked( $webinar_data->live_webinar_ctas_position_radios, 'overlay', false ) : ''; ?> type="radio" id="OverlayPos" name="cta_position" value="overlay">
					<label class="live_webinar_ctas_position_label" for="OverlayPos">Overlay</label>
					<input class="live_webinar_ctas_position_radios" <?php echo isset( $webinar_data->live_webinar_ctas_position_radios ) ? checked( $webinar_data->live_webinar_ctas_position_radios, 'outer', false ) : ''; ?> type="radio" id="TabPos" name="cta_position" value="outer">
					<label class="live_webinar_ctas_position_label" for="TabPos">Sidebar</label>
				</div>
			</div>
			<div class="airExtraOptions console-if-outer-container">
				<span class="airSwitchTitle"><?php esc_html_e( 'Tab name', 'webinar-ignition' ); ?></span>
				<span class="airSwitchInfo"><?php esc_html_e( 'This is the tab name that is displayed on the sidebar...', 'webinar-ignition' ); ?></span>
				<input type="text" style="margin-top: 10px;" placeholder="<?php esc_html_e( 'Ex: Default Title', 'webinar-ignition' ); ?>" id="air_tab_copy" value="<?php echo isset( $webinar_data->air_tab_copy ) ? esc_attr( $webinar_data->air_tab_copy ) : ''; ?>">
				<input type="hidden" id="isTabNameAvailable" value="1" />
			</div>
			<div class="airExtraOptions console-if-overlay-container">
				<span class="airSwitchTitle"><?php esc_html_e( 'No Tab Name is applicable for sidebar CTA', 'webinar-ignition' ); ?></span>
				<input type="hidden" id="isTabNameAvailable" value="0" />
			</div>

			<div class="airEditorArea" style="margin-top: 20px;">
				<?php
				$editor_content = isset( $webinar_data->air_html ) ? stripcslashes( $webinar_data->air_html ) : '';
				// added default WordPress editor instead of summernote editor
				$content   = $editor_content; // Initial content
				$editor_id = 'airCopy_editor'; // Unique ID for the editor
				$settings = array(
					'textarea_name' => 'airCopy_textarea',
					'textarea_rows' => 10,
					'tinymce'       => array(
						'toolbar1' => 'styleselect | bold italic underline | fontselect | forecolor | backcolor | bullist numlist | table | link | fullscreen | code | help',
						'toolbar2' => 'alignleft aligncenter alignright alignjustify | outdent indent | hr | insertdatetime | preview | wp_page | wp_more',
						'plugins'  => 'lists, link, image, media, paste, textcolor, fullscreen', // Added plugins
					),
					'quicktags'     => true,
				);
				// Generate the editor
				?>
				<div class="airExtraOptions">
					<span class="airSwitchTitle">
						<?php echo esc_html__( 'CTA Content', 'webinar-ignition' ) ; ?>
					</span>
					<span class="airSwitchInfo">
						<?php 
						if ( webinarignition_fs()->is__premium_only()  ){ 
							$statusCheck = WebinarignitionLicense::webinarignition_get_license_level();
							if ( $statusCheck && 'ultimate_powerup_tier3a' === $statusCheck->name && webinarignition_fs()->can_use_premium_code() ) { 
								$all_html_tags['iframe'] = true;
							}else{
								printf(
									esc_html__( 'WP by default does not allow the use of iframes in the editor. We managed a workaround in the Ultimate Plus license. %s', 'webinar-ignition' ), 
									'<a href="'.esc_url( admin_url( "admin.php?page=webinarignition-dashboard-pricing" ) ).'" target="_blank" rel="noopener noreferrer">' . esc_html__( 'Get license here.', 'webinar-ignition' ) . '</a>'
								);
							}
						}else{
							printf(
								esc_html__( 'WP by default does not allow the use of iframes in the editor. We managed a workaround in the Ultimate Plus license. %s', 'webinar-ignition' ), 
								'<a href="'.esc_url( admin_url( "admin.php?page=webinarignition-dashboard-pricing" ) ).'" target="_blank" rel="noopener noreferrer">' . esc_html__( 'Get license here.', 'webinar-ignition' ) . '</a>'
							);
						}
						 ?>
					</span>
					<?php wp_editor( $content, $editor_id, $settings ); ?>
				</div>
				<div class="airExtraOptions">
					<span class="airSwitchTitle">
						<?php echo esc_html__( 'CTA Button text', 'webinar-ignition' ) ; ?>
					</span>
					<span class="airSwitchInfo"><?php esc_html_e( 'This is the text that is displayed on the button...', 'webinar-ignition' ); ?></span>
					<input type="text" style="margin-top: 10px;" placeholder="<?php esc_html_e( 'Ex: Click Here To Download Your Copy', 'webinar-ignition' ); ?>" id="air_btn_copy" value="<?php echo isset( $webinar_data->air_btn_copy ) ? esc_attr( $webinar_data->air_btn_copy ) : ''; ?>">
				</div>

				<div class="airExtraOptions">
					<span class="airSwitchTitle"><?php esc_html_e( 'CTA Button URL', 'webinar-ignition' ); ?></span>
					<span class="airSwitchInfo"><?php esc_html_e( 'This is the url the button goes to (leave blank if you don\'t want the button to appear)...', 'webinar-ignition' ); ?></span>
					<input type="text" style="margin-top: 10px;" placeholder="<?php esc_html_e( 'Ex: http://yoursite.com/order-now', 'webinar-ignition' ); ?>" id="air_btn_url" value="<?php echo isset( $webinar_data->air_btn_url ) ? esc_url( $webinar_data->air_btn_url ) : ''; ?>">
				</div>
				<div class="airExtraOptions">
					<?php
					$air_btn_color = ! empty( $webinar_data->air_btn_color ) ? $webinar_data->air_btn_color : '#6BBA40';
					webinarignition_display_color(
						$ID,
						$air_btn_color,
						__( 'CTA Button Color', 'webinar-ignition' ),
						'air_btn_color',
						__( 'This is the color of the CTA button...', 'webinar-ignition' ),
						'#6BBA40'
					);
					?>
				</div>
				<!-- Broadcast Message Background Transparency -->
				<div class="airExtraOptions console-if-overlay-container">
					<span class="airSwitchTitle"><?php esc_html_e( 'Broadcast Message Background Transparency', 'webinar-ignition' ); ?></span>
					<span class="airSwitchInfo"><?php esc_html_e( 'Set BG transparancy from 0 to 100, where 100 - totally transparent', 'webinar-ignition' ); ?></span>
					<input type="text" style="margin-top: 10px;" placeholder="<?php esc_attr_e( 'Ex: 10', 'webinar-ignition' ); ?>" id="air_broadcast_message_bg_transparency" value="<?php echo isset( $webinar_data->air_broadcast_message_bg_transparency ) ? esc_attr( stripcslashes( $webinar_data->air_broadcast_message_bg_transparency ) ) : '0'; ?>">
				</div>
				<!-- Broadcast Message Width -->
				<div class="airExtraOptions console-if-overlay-container">
					<span class="airSwitchTitle"><?php esc_html_e( 'Broadcast Message Width', 'webinar-ignition' ); ?></span>
					<span class="airSwitchInfo"><?php esc_html_e( 'Set maximum width for default CTA section. Left blank or set 0 if you want to set CTA 60% width', 'webinar-ignition' ); ?></span>
					<input type="text" style="margin-top: 10px;" placeholder="<?php esc_attr_e( 'Ex: 60%', 'webinar-ignition' ); ?>" id="air_broadcast_message_width" value="<?php echo isset( $webinar_data->air_broadcast_message_width ) ? esc_attr( stripcslashes( $webinar_data->air_broadcast_message_width ) ) : '60%'; ?>">
				</div>

				<div class="airExtraOptions console-if-overlay-container">
					<span class="airSwitchTitle"><?php esc_html_e( 'Broadcast Message Alignment', 'webinar-ignition' ); ?></span>
					<span class="airSwitchInfo"><?php esc_html_e( 'Set alignment for default CTA section. If not selected then center will be the default option.', 'webinar-ignition' ); ?></span>
					<?php 
					if ( webinarignition_fs()->is__premium_only()  ){ 
						if ( Webinar_Ignition_Notices_Manager::webinarignition_only_pro_users_can_use( 'cta-alignment' ) && webinarignition_fs()->can_use_premium_code() ) { ?>
							<div class="AlignmentRadiosForLiveWebinarCTAs">
								<input class="live_webinar_ctas_alignment_radios" <?php echo isset( $webinar_data->live_webinar_ctas_alignment_radios ) ? checked( $webinar_data->live_webinar_ctas_alignment_radios, 'flex-start', false ) : ''; ?> type="radio" id="leftAlign" name="alignment" value="flex-start">
								<label class="live_webinar_ctas_alignment_label" for="leftAlign">Left Align</label>

								<input class="live_webinar_ctas_alignment_radios" <?php echo isset( $webinar_data->live_webinar_ctas_alignment_radios ) ? checked( $webinar_data->live_webinar_ctas_alignment_radios, 'center', false ) : ''; ?> type="radio" id="centerAlign" name="alignment" value="center">
								<label class="live_webinar_ctas_alignment_label" for="centerAlign">Center Align</label>

								<input class="live_webinar_ctas_alignment_radios" <?php echo isset( $webinar_data->live_webinar_ctas_alignment_radios ) ? checked( $webinar_data->live_webinar_ctas_alignment_radios, 'flex-end', false ) : ''; ?> type="radio" id="rightAlign" name="alignment" value="flex-end">
								<label class="live_webinar_ctas_alignment_label" for="rightAlign">Right Align</label>
							</div>
						<?php 
						}
						else{
							include WEBINARIGNITION_PATH . 'templates/notices/pro-notice.php';
						}
					}else {
						include WEBINARIGNITION_PATH . 'templates/notices/pro-notice.php';
					}
						?> 
					<?php 
					?>
				</div>
				<!-- Broadcast Message Width -->
				<div class="airExtraOptions console-if-outer-container">
					<span class="airSwitchTitle"><?php esc_html_e( 'No width is applicable for sidebar CTA', 'webinar-ignition' ); ?></span>
					<input type="hidden" style="margin-top: 10px;" id="air_broadcast_message_width" value="100%">
				</div>
				<div class="airExtraOptions console-if-outer-container">
					<span class="airSwitchTitle"><?php esc_html_e( 'Broadcast Message Alignment Not Applicable on Sidebar CTA', 'webinar-ignition' ); ?></span>
					<input class="live_webinar_ctas_alignment_radios" 
						<?php checked( isset( $webinar_data->live_webinar_ctas_alignment_radios ) ? $webinar_data->live_webinar_ctas_alignment_radios : 'center', 'center' ); ?> 
						type="checkbox" id="centerAlign" name="alignment" value="center">
				</div>
				
			</div>

			
			<!-- Beware to remove html element who have js relation -->
			<div class="airSwitchSaveArea" rel="js-webinar-id" data-webinar-id="<?php echo esc_attr( $webinar_data->id ); ?>">
				<div class="airSwitchFooterRight" style="margin-bottom: 20px;">
					<button type="button" href="#" id="saveAir" class="small button radius success" style="margin-right:0px;"><i class="icon-save"></i> <span id="saveAirText"><?php esc_html_e( 'Save On Air Settings', 'webinar-ignition' ); ?></span></button>
				</div>

				<br clear="all" />

			</div>

		</div>
	</div>

</div>