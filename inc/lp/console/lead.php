<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>
<!-- ON AIR AREA -->
<div id="leadTab" style="display:none;" class="consoleTabs">
	<div class="statsDashbord">
		<div class="statsTitle statsTitle-Lead">
			<div class="statsTitleIcon">
				<i class="icon-group icon-2x"></i>
			</div>

			<div class="statsTitleCopy">
				<h2><?php esc_html_e( 'Manage Registrants For Webinar', 'webinar-ignition' ); ?></h2>

				<p><?php esc_html_e( 'All your Registrants / Leads for the event...', 'webinar-ignition' ); ?></p>
			</div>

			<br clear="left"/>
		</div>
	</div>

	
	<?php 
	if ( webinarignition_fs()->is__premium_only() ){
		if ( Webinar_Ignition_Notices_Manager::webinarignition_only_pro_users_can_use(  ) && webinarignition_fs()->can_use_premium_code() ) { ?>
			<div class="innerOuterContainer">
				<div class="innerContainer">
					<div class="questionMainTa2b" style="margin-top: 20px;">
						<div class="airSwitch" style="padding-top: 0px;">
							<div class="airSwitchLeft">
								<span class="airSwitchTitle"><?php esc_html_e( 'Your Registrants Command Center', 'webinar-ignition' ); ?></span>
								<span class="airSwitchInfo"><?php esc_html_e( 'All the stats you will need for your registrants...', 'webinar-ignition' ); ?></span>
							</div>

							<form action="<?php echo esc_url( admin_url( 'admin-post.php' ) ); ?>" method='post'
								id='webinarignition_export_leads_form' target="_blank">
								<div id="ezrestrict-verify-inputs">
									<input type='hidden' name='action' value='webinarignition_export_leads'>
									<input type='hidden' name='webinarignition_webinar_id' value='<?php echo absint( $webinar_data->id ); ?>'>
									<input type="hidden" name="webinarignition_leads_type" id="webinarignition_leads_type"
										value="<?php echo webinarignition_is_auto( $webinar_data ) ? 'evergreen' : 'live'; ?>">
							</form>
													<!-- wi-js-105 Code Copied to webinarignition-frontend.js File -->
													<div class="airSwitchRight">
														<?php if ( 'AUTO' === $webinar_data->webinar_date ) { ?>
															<?php if ( Webinar_Ignition_Notices_Manager::webinarignition_only_pro_users_can_use() ) { ?>
																<button type="button" href="#" id="live_delete"
																		class="small button secondary" style="margin-right: 0px;"><i
																			class="icon-file-text"></i>
																	<?php esc_html_e( 'Delete all', 'webinar-ignition' ); ?>
																</button>
																<button type="button" href="#" onclick="webinarignitionExportLeads('evergreen_normal')"
																		class="small button secondary" style="margin-right: 0px;"><i
																			class="icon-file-text"></i>
																	<?php esc_html_e( 'Export CSV', 'webinar-ignition' ); ?>
																</button>
																<button type="button" href="#" onclick="webinarignitionExportLeads('evergreen_hot')"
																		class="small button secondary" style="margin-right: 0px;"><i
																			class="icon-file-text"></i>
																	<?php esc_html_e( 'HOT LEADS', 'webinar-ignition' ); ?>
																</button>
															<?php } ?>
															<a href="#" id="showtrackingcode" class="small button secondary" style="margin-right: 0px;"><i
																		class="icon-dollar"></i> <?php esc_html_e( 'Get Order Code', 'webinar-ignition' ); ?></a>
														<?php } else { ?>
															<?php if ( Webinar_Ignition_Notices_Manager::webinarignition_only_pro_users_can_use() ) { ?>
																<button type="button" href="#" id="live_delete"
																		class="small button secondary" style="margin-right: 0px;"><i
																			class="icon-file-text"></i>
																	<?php esc_html_e( 'Delete all', 'webinar-ignition' ); ?>
																</button>
																<button type="button" href="#" onclick="webinarignitionExportLeads('live_normal')"
																		class="small button secondary" style="margin-right: 0px;"><i
																			class="icon-file-text"></i>
																	<?php esc_html_e( 'Export CSV', 'webinar-ignition' ); ?>
																</button>
																<button type="button" href="#" onclick="webinarignitionExportLeads('live_hot')"
																		class="small button secondary" style="margin-right: 0px;"><i
																			class="icon-file-text"></i>
																	<?php esc_html_e( 'HOT LEADS', 'webinar-ignition' ); ?>
																</button>
																<a href="#" id="importLeads" class="small button secondary thickbox" style="margin-right: 0px;"><i
																			class="icon-group"></i> <?php esc_html_e( 'Import Leads (CSV)', 'webinar-ignition' ); ?>
																</a>
															<?php } ?>
															<a href="#" id="showtrackingcode" class="small button secondary" style="margin-right: 0px;"><i
																		class="icon-dollar"></i> <?php esc_html_e( 'Get Order Code', 'webinar-ignition' ); ?></a>
														<?php } ?>
													</div>
							<br clear="all"/>

							<!-- Import CSV File Area -->
							<div class="importCSVArea" style="display: none;">
								<h2><?php esc_html_e( 'Import Leads Into This Campaign', 'webinar-ignition' ); ?></h2>
								<h4><?php esc_html_e( 'Paste in your CSV in the area below. <b>Must Follow This Format: NAME, EMAIL, PHONE', 'webinar-ignition' ); ?></b></h4>
								<textarea id="importCSV"
										placeholder="<?php esc_html_e( 'Add your CSV Code here...', 'webinar-ignition' ); ?>"></textarea>
								<a href="#" class="button secondary"
								id="addCSV"><?php esc_html_e( 'Import Leads Now', 'webinar-ignition' ); ?></a>
								<br>
								<hr>
								<div class="reh-upload-csv">
									<form id="csv-upload-form" method="POST" enctype="multipart/form-data">
										<label for="csv_file"> <?php esc_html_e( 'Upload CSV File here', 'webinar-ignition' ); ?> </label>
										<input type="file" name="csv_file"  accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel, .txt, .csv" />
										<!-- <input type="file" name="csv_file" accept=".csv"> -->
										<input type="submit" value="Upload">
									</form>
								</div>
							</div>

							<!-- New Stats Funnel Design Leads -->
							<div class="leadStatBlock">

								<div class="leadStat leadStat1">
									<div class="leadStatTop" id="leadTotal"><?php echo esc_html( $totalLeads ); ?></div>
									<div class="leadStatLabel"
										style="border-bottom-left-radius: 5px;"><?php esc_html_e( 'total leads', 'webinar-ignition' ); ?></div>
								</div>

								<div class="leadStat leadStat2">
									<div class="leadStatTop"><span id="eventTotal">0</span></div>
									<div class="leadStatLabel"><?php esc_html_e( 'attended live event', 'webinar-ignition' ); ?></div>
								</div>


								<div class="leadStat leadStat4">
									<div class="leadStatTop"><span id="orderTotal">0</span></div>
									<div class="leadStatLabel"
										style="border-bottom-right-radius: 5px;"><?php esc_html_e( 'purchased', 'webinar-ignition' ); ?></div>
								</div>

								<br clear="left"/>

							</div>
						</div>

						<table id="leads" class="table table-striped table-bordered">
							<thead>
							<tr>
								<th class="leadHead"><i class="icon-user"
														style="margin-right: 5px;"></i><?php esc_html_e( 'Registrants Information', 'webinar-ignition' ); ?>
									:
								</th>
								<th><i class="icon-eye-open"
									style="margin-right: 5px;"></i><?php esc_html_e( 'Attended Event', 'webinar-ignition' ); ?>:
								</th>
								<th><i class="icon-dollar"
									style="margin-right: 5px;"></i><?php esc_html_e( 'Ordered', 'webinar-ignition' ); ?>:
								</th>
								<th width="90"><i class="icon-trash"
												style="margin-right: 5px;"></i> <?php esc_html_e( 'Delete', 'webinar-ignition' ); ?>
								</th>
							</tr>
							</thead>
							<tbody>

							<?php foreach ( $leads as $lead ) : ?>

								<tr id="table_lead_<?php echo esc_attr( $lead['ID'] ); ?>" class="leadTableBlock">
									<td style="padding: 15px; width: 350px;">
										<span class="leadName"><span class="fbLead"
																	style="display: 
																	<?php
																	if ( 'FB' === $lead['trk1'] ) {
																		echo 'inline';
																	} else {
																		echo 'none';
																	}
																	?>
																	;"><i
														class="icon-facebook-sign"></i></span> <?php echo esc_html( $lead['name'] ); ?> </span>
										<span class="leadInfoSub">
											<i class="icon-calendar"
											style="margin-right: 5px;"></i> <?php echo esc_html( $lead['created'] ); ?>
											<b><i class="icon-envelope-alt"
												style="margin-right: 5px; margin-left: 5px;"></i> <?php echo esc_html( $lead['email'] ); ?>
											</b>
										</span>
										<span class="leadInfoSub" style="margin-top: 5px;">
											<i class="icon-mobile-phone" style="margin-right: 5px;"></i> 
											<?php
											if ( 'undefined' === $lead['phone'] || empty( $lead['phone'] ) ) {
												echo 'No Phone';
											} else {
												echo esc_html( $lead['phone'] );
											}
											?>
										</span>
										<?php if ( 'AUTO' === $webinar_data->webinar_date ) { ?>
											<span class="leadInfoSub">
												<i class="icon-time" style="margin-right: 5px; color: green"></i>
												<?php echo esc_html( $lead['date_picked_and_live'] ); ?>
												<b>
												<i class="icon-sun" style="margin-right: 5px; margin-left: 5px;color: orangered"></i>
												<?php echo esc_html( $lead['lead_timezone'] ); ?>
												</b>
											</span>
										<?php } ?>
										<?php
										$lead_meta = WebinarignitionLeadsManager::webinarignition_get_lead_meta( $lead['ID'], 'wiRegForm', 'AUTO' === $webinar_data->webinar_date ? 'evergreen' : 'live' );
										
										
										if ( ! empty( $lead_meta['meta_value'] ) ) {
											$lead_meta_data = maybe_unserialize( $lead_meta['meta_value'] );
											$lead_meta_data = WebinarignitionLeadsManager::webinarignition_fix_opt_name( $lead_meta_data );
											
										
											if ( is_array( $lead_meta_data ) ) {
												// Fields to exclude
												$exclude_fields = array(
													'firstName' => '',
													'webinar_type' => '',
													'phone' => '',
													'id' => '',
													'source' => '',
													'gdpr_data' => '',
													'ip' => '',
													'hash_ID' => ''
												);
										
												// Remove excluded fields from the lead meta data
												$filtered_lead_meta_data = array_diff_key($lead_meta_data, $exclude_fields);
										
												if ( isset( $lead_meta_data['optName'] ) || isset( $lead_meta_data['optEmail'] ) ) {
													foreach ( $filtered_lead_meta_data as $field_name => $field ) {
														$field_label = $field['label'];
														$field_value = $field['value'];
														?>
														<span class="leadInfoSub">
															<?php echo esc_html( $field_label ); ?>:
															<b><?php echo esc_html( $field_value ); ?></b>
														</span>

														<?php
													}
												} else { // compatibility with old lead data
													foreach ( $filtered_lead_meta_data as $field_label => $field_value ) {
														?>
														<span class="leadInfoSub">
															<?php echo esc_html( $field_label ); ?>:
															<b><?php echo esc_html( $field_value ); ?></b>

														</span>
														<?php
													}
												}//end if
											}//end if
										}//end if
										?>
									</td>
									<td><span class="radius checkEvent 
									<?php
									if ( 'No' === $lead['event'] || empty( $lead['event'] ) ) {
											echo 'secondary';
									} else {
										echo 'success';
									}
									?>
										label">
										<?php
										if ( 'No' == $lead['event'] || empty( $lead['event'] ) ) {
												echo 'No';
										} else {
											echo 'Yes';
										}
										?>
											</span></td>
									<td><span class="radius checkOrder 
									<?php
									if ( empty( $lead['trk2'] ) ) {
											echo 'secondary';
									} else {
										echo 'success';
									}
									?>
										label">
										<?php
										if ( empty( $lead['trk2'] ) ) {
												echo 'No';
										} else {
											echo esc_html( $lead['trk2'] );
										}
										?>
									</span></td>

									<td>
										<center><i class="icon-remove delete_lead" lead_id="<?php echo esc_attr( $lead['ID'] ); ?>"></i>
										</center>
									</td>
								</tr>
								<?php

							endforeach;
							?>

							</tbody>
						</table>

						<br clear="all"/>
					</div>
				</div>
			</div>
		<?php }
		else{
			?>
			<div class="container">
				<?php include WEBINARIGNITION_PATH . 'templates/notices/pro-notice.php'; ?>
			</div>
			<?php
		}
	} else{
		?>
		<div class="container">
			<?php include WEBINARIGNITION_PATH . 'templates/notices/pro-notice.php'; ?>
		</div>
		<?php
	}?>
</div>

