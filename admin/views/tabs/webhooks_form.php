<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
$action = sanitize_text_field( filter_input( INPUT_GET, 'action', FILTER_SANITIZE_SPECIAL_CHARS ) );
$webhook_id = absint( filter_input( INPUT_GET, 'id', FILTER_SANITIZE_SPECIAL_CHARS ) );
$triggers = WebinarIgnitionWebhooks::webinarignition_webhook_get_triggers();
$integrations = WebinarIgnitionWebhooks::webinarignition_webhook_get_integrations();
$webhook_data = WebinarIgnitionWebhooks::webinarignition_webhook_get_data( $webhook_id );
$webhook_data['conditions'] = maybe_unserialize( $webhook_data['conditions'] );
?>

<p><a href="<?php echo esc_attr( add_query_arg( array(
	'page' => 'webinarignition_settings',
	'tab' => 'webhooks',
), admin_url( 'admin.php' ) ) ); ?>" class="button button-primary button-large">
	<span class="dashicons dashicons-arrow-left-alt2" style="line-height: 32px;"></span>
	<?php esc_attr_e( 'Webhooks List', 'webinar-ignition' ); ?></a>
</p>

<form method="post">
	<input type="hidden" name="action" value="webinarignition_webhook_test_request">
	<input type="hidden" name="webhook_id" value="<?php echo esc_attr( $webhook_id ); ?>">
	<table class="form-table">
		<tbody>
		<tr>
			<th><label><?php esc_attr_e( 'Name', 'webinar-ignition' ); ?></label></th>
			<td><input type="text" class="regular-text" name="webhook_name" value="<?php echo esc_attr( $webhook_data['name'] ); ?>"></td>
		</tr>
		<tr>
			<th><label><?php esc_attr_e( 'Trigger', 'webinar-ignition' ); ?></label></th>
			<td>
				<select name="webhook_trigger">
					<?php foreach ( $triggers as $trigger => $trigger_name ) : ?>
						<option value="<?php echo esc_attr( $trigger ); ?>" <?php selected( $webhook_data['trigger'], $trigger ); ?>><?php echo esc_attr( $trigger_name ); ?></option>
					<?php endforeach; ?>
				</select>
			</td>
		</tr>
		<tr style="display:none;">
			<th><label><?php esc_attr_e( 'Data Template', 'webinar-ignition' ); ?></label></th>
			<td>
				<select name="webhook_integration">
					<?php foreach ( $integrations as $integration => $integration_name ) : ?>
						<option value="<?php echo esc_attr( $integration ); ?>" <?php selected( $webhook_data['integration'], $integration ); ?>><?php echo esc_attr( $integration_name ); ?></option>
					<?php endforeach; ?>
				</select>
			</td>
		</tr>
		<tr>
			<th><label><?php esc_attr_e( 'Delivery URL', 'webinar-ignition' ); ?></label></th>
			<td><input type="text" class="regular-text" name="webhook_url" value="<?php echo esc_attr( $webhook_data['url'] ); ?>"></td>
		</tr>
		<tr>
			<th><label><?php esc_attr_e( 'Request Method', 'webinar-ignition' ); ?></label></th>
			<td>
				<select name="webhook_request_method">
					<option value="1" <?php selected( absint( $webhook_data['request_method'] ), 1 ); ?>><?php esc_attr_e( 'POST', 'webinar-ignition' ); ?></option>
					<option value="0" <?php selected( absint( $webhook_data['request_method'] ), 0 ); ?>><?php esc_attr_e( 'GET', 'webinar-ignition' ); ?></option>
				</select>
			</td>
		</tr>
		<tr>
			<th><label><?php esc_attr_e( 'Request Format', 'webinar-ignition' ); ?></label></th>
			<td>
				<select name="webhook_request_format">
					<option value="0" <?php selected( absint( $webhook_data['request_format'] ), 0 ); ?>><?php esc_attr_e( 'JSON', 'webinar-ignition' ); ?></option>
					<option value="1" <?php selected( absint( $webhook_data['request_format'] ), 1 ); ?>><?php esc_attr_e( 'FORM', 'webinar-ignition' ); ?></option>
				</select>
			</td>
		</tr>
		<tr>
			<th><label><?php esc_attr_e( 'Secret', 'webinar-ignition' ); ?></label></th>
			<td><input type="text" class="regular-text" name="webhook_secret" value="<?php echo esc_attr( $webhook_data['secret'] ); ?>"></td>
		</tr>
		<tr>
			<th><label><?php esc_attr_e( 'Status', 'webinar-ignition' ); ?></label></th>
			<td>
				<?php
				if ( webinarignition_fs()->is__premium_only() && webinarignition_fs()->can_use_premium_code() ){ 
					if ( Webinar_Ignition_Notices_Manager::webinarignition_only_pro_users_can_use() ) { ?>
						<select name="webhook_status">
							<option value="1" <?php selected( absint( $webhook_data['is_active'] ), 1 ); ?>><?php esc_attr_e( 'Active', 'webinar-ignition' ); ?></option>
							<option value="0" <?php selected( absint( $webhook_data['is_active'] ), 0 ); ?>><?php esc_attr_e( 'Inactive', 'webinar-ignition' ); ?></option>
						</select>
					<?php }
				}
				include WEBINARIGNITION_PATH . 'templates/notices/pro-notice.php'; 
				?> 
			</td>
		</tr>
		</tbody>
	</table>
	<hr>
	<h3><?php esc_attr_e( 'Additional Fields:', 'webinar-ignition' ); ?></h3> <button class="button button-secondary button-small" id="wi_webhooks_condition_add"><?php esc_attr_e( 'Add', 'webinar-ignition' ); ?></button>
	<?php

	$_props = array(
		'ar_name',
		'ar_lname',
		'ar_phone',
		'ar_email',
		'ar_webinar_host',
		'ar_privacy_policy',
		'ar_terms_and_conditions',
		'ar_mailing_list',
		'ar_utm_source',
		'ar_custom_1',
		'ar_custom_2',
		'ar_custom_3',
		'ar_custom_4',
		'ar_custom_5',
		'ar_custom_6',
		'ar_custom_7',
		'ar_custom_8',
		'ar_custom_9',
		'ar_custom_10',
		'ar_custom_11',
		'ar_custom_12',
		'ar_custom_13',
		'ar_custom_14',
		'ar_custom_15',
		'ar_custom_16',
		'ar_custom_17',
		'ar_custom_18',
	);

	// Hidden/DB fields
	$fields = array(
		'id',
		'title',
		'url',
		'date',
		'time',
		'date_time',
		'timezone',
		'host',
		'registration_date',
		'registration_time',
		'registration_date_time',
		'lead_url',
	);

	$operators = array(
		'map' => __( 'Map', 'webinar-ignition' ),
		'equal' => __( 'Equal', 'webinar-ignition' ),
		'not_equal' => __( 'Not Equal', 'webinar-ignition' ),
		'greater_than' => __( 'Greater Than (number)', 'webinar-ignition' ),
		'less_than' => __( 'Less Than (number)', 'webinar-ignition' ),
	);
	foreach ( $_props as $prop ) {
		$field_name = Webinar_Ignition_Helper::webinarignition_ar_field_to_opt( $prop );
		$fields[] = $field_name;
	}

	?>
	<div id="wi_webhooks_conditions_wrapper">
		<table id="wi_webhooks_condition_table" class="form-table">
			<tbody>
			<tr id="wi_webhooks_condition_table_headers">
				<th><?php esc_attr_e( 'Field', 'webinar-ignition' ); ?></th>
				<th><?php esc_attr_e( 'Compare/Map', 'webinar-ignition' ); ?></th>
				<th><?php esc_attr_e( 'Compare Value', 'webinar-ignition' ); ?></th>
				<th><?php esc_attr_e( 'New field name', 'webinar-ignition' ); ?></th>
				<th><?php esc_attr_e( 'New field value', 'webinar-ignition' ); ?></th>
			</tr>
			<tr id="wi_webhooks_condition_row" style="display:none;">
				<td class="wi_webhooks_condition_field">
					<select name="wi_webhooks_condition[field][]">
						<?php foreach ( $fields as $field ) : ?>
							<option value="<?php echo esc_attr( $field ); ?>"><?php echo esc_attr( $field ); ?></option>
						<?php endforeach; ?>
					</select>
				</td>
				<td class="wi_webhooks_condition_operator">

					<select name="wi_webhooks_condition[operator][]">
						<?php foreach ( $operators as $operator => $operator_name ) : ?>
							<option value="<?php echo esc_attr( $operator ); ?>"><?php echo esc_attr( $operator_name ); ?></option>
						<?php endforeach; ?>
					</select>
				</td>
				<td class="wi_webhooks_condition_value"><input type="text" value="" name="wi_webhooks_condition[value][]" readonly></td>
				<td class="wi_webhooks_condition_new_field_name"><input type="text" value="" name="wi_webhooks_condition[field_name][]"></td>
				<td class="wi_webhooks_condition_new_field_value"><input type="text" value="" name="wi_webhooks_condition[field_value][]" readonly></td>
				<td><a href="#" class="wi_webhooks_condition_delete" style="color:red; font-weight:700;">x</a> </td>
			</tr>
			<?php
			if ( ! empty( $webhook_data['conditions'] ) ) {
				foreach ( $webhook_data['conditions']['field'] as $index => $field ) :

					?>
					<tr class="wi-webhook-condition-row">
						<td class="wi_webhooks_condition_field">
							<select name="wi_webhooks_condition[field][]">
								<?php foreach ( $fields as $field ) :
									// Rename "date_registered_on" and "time_registered_on" to "webinar_registration_date" and "webinar_registration_time" respectively
									if ( 'date_registered_on' === $webhook_data['conditions']['field'][ $index ] ) {
										$webhook_data['conditions']['field'][ $index ] = 'webinar_registration_date';
									} elseif ( 'time_registered_on' === $webhook_data['conditions']['field'][ $index ] ) {
										$webhook_data['conditions']['field'][ $index ] = 'webinar_registration_time';
									}

									if ( "webinar_{$field}" === $webhook_data['conditions']['field'][ $index ] ) {
										$webhook_data['conditions']['field'][ $index ] = $field;
									}
									?>
									<option value="<?php echo esc_attr( $field ); ?>" <?php selected( $webhook_data['conditions']['field'][ $index ], $field ); ?>><?php echo esc_attr( $field ); ?></option>
								<?php endforeach; ?>
							</select>
						</td>
						<td class="wi_webhooks_condition_operator">

							<select name="wi_webhooks_condition[operator][]">
								<?php foreach ( $operators as $operator => $operator_name ) : ?>
									<option value="<?php echo esc_attr( $operator ); ?>" <?php selected( $webhook_data['conditions']['operator'][ $index ], $operator ); ?>><?php echo esc_attr( $operator_name ); ?></option>
								<?php endforeach; ?>
							</select>
						</td>
						<td class="wi_webhooks_condition_value"><input type="text" value="<?php echo esc_attr( $webhook_data['conditions']['value'][ $index ] ); ?>" name="wi_webhooks_condition[value][]" <?php wp_readonly( $webhook_data['conditions']['operator'][ $index ], 'map' ); ?>></td>
						<td class="wi_webhooks_condition_new_field_name"><input type="text" value="<?php echo esc_attr( $webhook_data['conditions']['field_name'][ $index ] ); ?>" name="wi_webhooks_condition[field_name][]"></td>
						<td class="wi_webhooks_condition_new_field_value"><input type="text" value="<?php echo esc_attr( $webhook_data['conditions']['field_value'][ $index ] ); ?>" name="wi_webhooks_condition[field_value][]"  <?php wp_readonly( $webhook_data['conditions']['operator'][ $index ], 'map' ); ?>></td>
						<td><a href="#" class="wi_webhooks_condition_delete" style="color:red; font-weight:700;">x</a> </td>
					</tr>

					<?php
				endforeach;
			}//end if
			?>
			<tr class="wi-webhook-condition-no-rows-message" style="<?php echo ! empty( $webhook_data['conditions']['field'] ) ? 'display:none;' : ''; ?>">
				<td colspan="6"><?php esc_attr_e( 'Click the "Add" button above to define additional custom fields.', 'webinar-ignition' ); ?></td>
			</tr>
			</tbody>
		</table>
	</div>
	<p>&nbsp;</p>
	<p><button type="submit" name="save" class="button button-primary button-large" id="webinarignition-save-webhook"><?php esc_attr_e( 'Save webhook', 'webinar-ignition' ); ?></button></p>
	<!--<button name="test_webhook" id="webinar_ignition_test_webhook" class="button button-secondary button-large"><?php /*_e( 'Test webhook','webinarignition' ); */ ?></button>-->
	<?php wp_nonce_field( 'webinarignition-webhook-save' ); ?>
</form>