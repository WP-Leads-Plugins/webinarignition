<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

// WP_List_Table is not loaded automatically so we need to load it in our application
if( ! class_exists( 'WP_List_Table' ) ) {
	require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}

/**
 * Create a new table class that will extend the WP_List_Table
 */
class WebinarIgnition_Admin_Webhooks_List_Table extends WP_List_Table {

	/**
	 * Prepare the items for the table to process
	 *
	 * @return Void
	 */
	public function prepare_items() {
		$columns = $this->get_columns();
		$hidden = $this->get_hidden_columns();
		$sortable = $this->get_sortable_columns();

		$data = $this->table_data();
		usort( $data, array( &$this, 'sort_data' ) );

		$perPage = 10;
		$currentPage = $this->get_pagenum();
		$totalItems = count($data);

		$this->set_pagination_args( array(
			'total_items' => $totalItems,
			'per_page'    => $perPage
		) );

		$data = array_slice($data,(($currentPage-1)*$perPage),$perPage);

		$this->_column_headers = array($columns, $hidden, $sortable);
		$this->items = $data;
	}

	/**
	 * Override the parent columns method. Defines the columns to use in your listing table
	 *
	 * @return Array
	 */
	public function get_columns() {
		$columns = array(
			'name' => 'Name',
			'trigger' => 'Trigger',
			'request_method' => 'Method',
			'request_format' => 'Format',
			'is_active' => 'Status',
			'modified' => 'Updated At'
		);

		return $columns;
	}

	/**
	 * Define which columns are hidden
	 *
	 * @return Array
	 */
	public function get_hidden_columns() {
		return array();
	}

	/**
	 * Define the sortable columns
	 *
	 * @return Array
	 */
	public function get_sortable_columns() {
		return array('name' => array('name', false), 'trigger' => array('trigger', false), 'modified' => array('modified', true));
	}

	/**
	 * Get the table data
	 *
	 * @return Array
	 */
	private function table_data() {
		global $wpdb;
		$table = $wpdb->prefix . "webinarignition_webhooks";
		return $wpdb->get_results($wpdb->prepare("SELECT * FROM {$table} WHERE 1=1"), ARRAY_A) ?: [];
	}

	/**
	 * Define what data to show on each column of the table
	 *
	 * @param  Array $item        Data
	 * @param  String $column_name - Current column name
	 *
	 * @return Mixed
	 */
	public function column_default( $item, $column_name ) {
		switch( $column_name ) {
			case 'name':
			case 'trigger':
			    $triggers = WebinarIgnitionWebhooks::webinarignition_webhook_get_triggers();
			    return isset($triggers[$item[ $column_name ]]) ? esc_html($triggers[$item[ $column_name ]]) : '';
			case 'is_active':
			    return isset($item[ $column_name ]) && absint($item[ $column_name ]) === 1 ? esc_html__('Active', 'webinar-ignition') : esc_html__('Inactive', 'webinar-ignition');
			case 'request_method':
				return isset($item[ $column_name ]) && absint($item[ $column_name ]) === 1 ? esc_html__('POST', 'webinar-ignition') : esc_html__('GET', 'webinar-ignition');
			case 'request_format':
				return isset($item[ $column_name ]) && absint($item[ $column_name ]) === 1 ? esc_html__('FORM', 'webinar-ignition') : esc_html__('JSON', 'webinar-ignition');
            case 'modified':
	            $date_format = get_option( 'date_format' );
	            $time_format = get_option( 'time_format' );
				return esc_html(wp_date("{$date_format} {$time_format}", strtotime($item[ $column_name ])));
			default:
				return esc_html(print_r( $item, true ));
		}
	}

	public function column_name($item) {
		$nonce = wp_create_nonce('delete_webhook_' . $item['id']);
		$actions = array(
			'edit'   => sprintf('<a href="%s">%s</a>', esc_url(add_query_arg(['page' => $_REQUEST['page'], 'tab' => $_REQUEST['tab'], 'action' => 'edit', 'id' => $item['id']], admin_url('admin.php'))), esc_html__('Edit', 'webinar-ignition')),
			'delete' => sprintf('<a href="%s">%s</a>', esc_url(add_query_arg(['page' => $_REQUEST['page'], 'tab' => $_REQUEST['tab'], 'action' => 'delete', 'id' => $item['id'], 'wp_nonce' => $nonce], admin_url('admin.php'))), esc_html__('Delete', 'webinar-ignition'))
		);

		return sprintf('%1$s %2$s', esc_html($item['name']), $this->row_actions($actions) );
	}

	/**
	 * Allows you to sort the data by the variables set in the $_GET
	 *
	 * @return Mixed
	 */
	private function sort_data( $a, $b ) {
		$orderby = ( ! empty( $_GET['orderby'] ) ) ? sanitize_key( $_GET['orderby'] ) : 'modified';
		$order = ( ! empty($_GET['order'] ) ) ? sanitize_key( $_GET['order'] ) : 'asc';
		$result = strcmp( $a[$orderby], $b[$orderby] );
		return ( $order === 'asc' ) ? $result : -$result;
	}

	public function extra_tablenav( $which ) {
		if ( 'top' === $which ) {
			?>
			<a class="add-new-h2" href="<?php echo esc_url(add_query_arg(['page' => 'webinarignition_settings', 'tab' => 'webhooks', 'action' => 'edit', 'id' => 0], admin_url('admin.php'))); ?>"><?php esc_html_e('Add webhook', 'webinar-ignition');?></a>
			<?php
		}
	}
}
