<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
?>
<div class="webinar_welcome_user_video_parent_container">
	<div class="webinar_welcome_left_container">
		<div class="webinar_welcome_user_container webinar_dashboard_box_design">
			<?php
			$current_user_data = wp_get_current_user();
			$current_username  = $current_user_data->display_name;
			?>
			<p class="webinar_welcom_user_text"><?php echo esc_html__( 'Hello', 'webinar-ignition' ) . ' ' . esc_html( $current_username ); ?>,</p>
			<h2><?php esc_html_e( 'Welcome to WebinarIgnition', 'webinar-ignition' ); ?></h2>
			<p><?php esc_html_e( 'WebinarIgnition offers a variety of pre-built webinar room designs that are modern, professional, and engaging. These designs are easy to customize with your own branding, and they can be used for a variety of different types of webinars, including lead generation webinars, customer education webinars, and product demos.', 'webinar-ignition' ); ?></p>
			<a class="blue-btn-2 btn newWebinarBTN" href="<?php echo esc_url( admin_url( 'admin.php?page=webinarignition-dashboard&create' ) ); ?>">
				<i class="icon-plus-sign" style="margin-right: 5px;"></i>
				<?php esc_html_e( 'Create a New Webinar', 'webinar-ignition' ); ?>
			</a>
			<a class="blue-btn-2 btn newWebinarBTN" href="<?php echo esc_url( admin_url( 'admin.php?page=webinarignition-dashboard&webinars' ) ); ?>" style="margin-right: 10px;">
				
				<?php esc_html_e( 'Show Webinars', 'webinar-ignition' ); ?>
			</a>
		</div>
		<div class="webinar_dashboard_box_design wi_kb_links_container">
			<h3 class="benefits_heading"><?php esc_html_e( 'Benefits, KB link', 'webinar-ignition' ); ?></h3>
			<div class="webinar_sent_leads_main_container">
				<div class="webinar_sent_leads">
					<div class="webinar_leads_description">
						<h4><?php esc_html_e( 'Auto Webinar - Setting Up An Evergreen Webinar', 'webinar-ignition' ); ?></h4>

						<p><?php esc_html_e( 'This video will show you how to setup an auto webinar and how it differs from a live webinar...', 'webinar-ignition' ); ?></p>

						<a href="https://webinarignition.tawk.help/article/auto-webinar-setting-up-an-evergreen-webinar" target="_blank"><?php esc_html_e( 'Knowledge base', 'webinar-ignition' ); ?></a>
					</div>
					<div class="webinar_leads_description_video">
						<iframe width="100%" height="100%" src="https://www.youtube.com/embed/8aPPvmEa2Gs?si=b-S_zz4OH1PNBUV5" title="<?php esc_attr_e( 'YouTube video player', 'webinar-ignition' ); ?>" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
					</div>
				</div>
				<div class="webinar_sent_leads">
					<div class="webinar_leads_description">
						<h4><?php esc_html_e( 'Dashboard - Getting Familiar With The Core Dashboard', 'webinar-ignition' ); ?></h4>

						<p><?php esc_html_e( 'This video will cover all the information of what the dashboard tells you about the webinar campaign...', 'webinar-ignition' ); ?></p>

						<a href="https://webinarignition.tawk.help/article/dashboard-getting-familiar-with-the-core-dashboard" target="_blank"><?php esc_html_e( 'Knowledge base', 'webinar-ignition' ); ?></a>
					</div>
					<div class="webinar_leads_description_video">
						<iframe width="100%" height="100%" src="https://www.youtube.com/embed/eMIpO1P_jJI?si=M2tsx4KEGseWXGg7" title="<?php esc_attr_e( 'YouTube video player', 'webinar-ignition' ); ?>" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
					</div>
				</div>
				<div class="webinar_sent_leads">
					<div class="webinar_leads_description">
						<h4><?php esc_html_e( 'Auto Webinar Video Format & reduce file size', 'webinar-ignition' ); ?></h4>

						<p><?php esc_html_e( 'If you would like to convert your youtube video to a file, try clipconverter.', 'webinar-ignition' ); ?></p>

						<a href="https://webinarignition.tawk.help/article/auto-webinar-video-format-html5-video-formats" target="_blank"><?php esc_html_e( 'Knowledge base', 'webinar-ignition' ); ?></a>
					</div>
					<div class="webinar_leads_description_video">
						<iframe width="100%" height="100%" src="https://www.youtube.com/embed/RW0QNpAXEUE?si=PZ50IXVFS627AWe4" title="<?php esc_attr_e( 'YouTube video player', 'webinar-ignition' ); ?>" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
					</div>
				</div>
				<div class="webinar_sent_leads">
					<div class="webinar_leads_description">
						<h4><?php esc_html_e( 'Click Registration (Live webinars)', 'webinar-ignition' ); ?></h4>

						<p><?php esc_html_e( 'This video will show you how to allow users to register for your webinar with one-click of your link. No need to enter name and email details from the registration page.', 'webinar-ignition' ); ?></p>

						<a href="https://webinarignition.tawk.help/article/1-click-registration"><?php esc_html_e( 'Knowledge base', 'webinar-ignition' ); ?></a>
					</div>
					<div class="webinar_leads_description_video">
						<iframe width="100%" height="100%" src="https://www.youtube.com/embed/zp7YH26g3Fo?si=GJPYg2HLVHTSJBcB" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
					</div>
				</div>
			</div>

		</div>
		<div class="webinar_comparison_chart webinar_dashboard_box_design wi-comparison-chart">
			<div class="wi-content">
				<h2>WebinarIgnition <?php esc_html_e( 'Comparison Chart', 'webinar-ignition' ); ?></h2>
				<div class="wi-table-container">
					<table>
						<thead>
							<tr>
								<th><?php esc_html_e( 'Price Plans', 'webinar-ignition' ); ?></th>
								<th>Free <span>$0</span></th>
								<th>Essential <span>$9.99</span></th>
								<th>Unlimited <span>$48.99</span></th>
								<th>Unlimited Plus <span>$96.99</span></th>
							</tr>
						</thead>
						<tbody>
						
						<tr>
								<td class="webinar_table_col_one"><?php esc_html_e( 'support', 'webinar-ignition' ); ?></td>


								<td><?php esc_html_e( 'ticket / email', 'webinar-ignition' ); ?></td>
								<td><?php esc_html_e( 'ticket / email', 'webinar-ignition' ); ?></td>

								<td><?php esc_html_e( 'Consultation', 'webinar-ignition' ); ?></td>
								<td><?php esc_html_e( '1:1 support', 'webinar-ignition' ); ?></td>
								

							</tr>

							<tr>
								<td class="webinar_table_col_one"><?php esc_html_e( 'Participants', 'webinar-ignition' ); ?></td>

								<td></td>
								<td></td>

								<td></td>
								<td></td>

							</tr>

							<tr>
								<td class="webinar_table_col_one"><?php esc_html_e( 'HTML code', 'webinar-ignition' ); ?></td>

								<td>✗</td>
								<td>✔</td>

								<td>✔</td>
								<td>✔</td>

							</tr>

							<tr>
								<td class="webinar_table_col_one"><?php esc_html_e( 'Webhooks', 'webinar-ignition' ); ?></td>
							
								<td>✗</td>

								<td>✗</td>
								<td>✔</td>

								<td>✔</td>

							
							</tr>

							<tr>
								<td class="webinar_table_col_one"><?php esc_html_e( 'Export', 'webinar-ignition' ); ?></td>
							
								<td>✗</td>

								<td>✗</td>
								<td>✔</td>

								<td>✔</td>

							
							</tr>

							<tr>
								<td class="webinar_table_col_one"><?php esc_html_e( 'Import (live)', 'webinar-ignition' ); ?></td>
							
								<td>✗</td>

								<td>✗</td>
								<td>✔</td>

								<td>✔</td>

							
							</tr>

							<tr>
								<td class="webinar_table_col_one"><?php esc_html_e( 'Paid Webinar', 'webinar-ignition' ); ?></td>
								<td>✗</td>
								<td>✗</td>

								<td>✔</td>
								<td>✔</td>
							</tr>
							<tr>
								<td class="webinar_table_col_one"><?php esc_html_e( 'CTA alignment', 'webinar-ignition' ); ?></td>
								<td>✗</td>
								<td>✔</td>
								<td>✔</td>
								<td>✔</td>
							</tr>
							<tr>
								<td class="webinar_table_col_one"><?php esc_html_e( 'Licenses', 'webinar-ignition' ); ?></td>
								<td>1</td>
								<td>1</td>
								<td>1</td>
								<td>3</td>
							</tr>
							<tr>
								<td></td>
								<td></td>
								<td>
									<a class="webinarignition-buy-now-button" href="<?php echo esc_url( admin_url( 'admin.php?page=webinarignition-dashboard-pricing' ) ); ?>"><?php esc_html_e( 'Buy Now', 'webinar-ignition' ); ?></a>
								</td>
								<td>
									<a class="webinarignition-buy-now-button" href="<?php echo esc_url( admin_url( 'admin.php?page=webinarignition-dashboard-pricing' ) ); ?>"><?php esc_html_e( 'Buy Now', 'webinar-ignition' ); ?></a>
								</td>
								<td>
									<a class="webinarignition-buy-now-button" href="<?php echo esc_url( admin_url( 'admin.php?page=webinarignition-dashboard-pricing' ) ); ?>"><?php esc_html_e( 'Buy Now', 'webinar-ignition' ); ?></a>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<div class="webinar_welcome_right_container">
		<div class="webinar_dashboard_box_design wi_video">
			<iframe width="100%" height="300px" src="https://www.youtube.com/embed/7IDiVQXnwZI?si=76TDj9zrlYR8u_OJ" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
		</div>
		<div class="webinar_dashboard_box_design wi_video">
			<iframe width="100%" height="300px" src="https://www.youtube.com/embed/L12pNLZUfSI?si=v7J554IDZFamuGz1" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
		</div>

		<?php 
			$is_pending_activation =  webinarignition_fs()->is_pending_activation();
			$is_registered = webinarignition_fs()->is_registered() && webinarignition_fs()->is_tracking_allowed();
			$site = webinarignition_fs()->get_site();
			$is_playground = true;
			$is_localhost = true;
			if ( class_exists('FS_Site') && $site instanceof FS_Site) {
				$is_playground = $site::is_playground_wp_environment_by_host($_SERVER['HTTP_HOST']);
				$is_localhost = $site->is_localhost();
			}

			if(!$is_localhost && !$is_playground){
				if (  wp_validate_boolean($is_pending_activation ) || ! $is_registered  ){
					 require WEBINARIGNITION_PATH . 'UI/opt-in/optin-popup-section-link.php'; 
				}
			}
			 
		if ( webinarignition_fs()->is__premium_only()  ){ 
			if ( Webinar_Ignition_Notices_Manager::webinarignition_is_essential( ) ) { 
				//show the essential section in the essential plan on dashboard
				?>
			<div class="webinar_dashboard_box_design wi_licence_box">
				<?php
				require_once WEBINARIGNITION_PATH . 'UI/opt-in/dash.php';
				require_once WEBINARIGNITION_PATH . 'admin/messages/free-license.php';
				?>
			</div>
		<?php }	
		} ?>
	</div>
</div>