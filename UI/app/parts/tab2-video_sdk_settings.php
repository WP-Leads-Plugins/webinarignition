<?php
/**
 * @var $input_get
 * @var $webinar_data
 */

$video_sdk_api_key_webinar = !empty($webinar_data->video_sdk_api_key) ? esc_html($webinar_data->video_sdk_api_key) : '';
$video_sdk_secret_key_webinar = !empty($webinar_data->video_sdk_secret_key) ? esc_html($webinar_data->video_sdk_secret_key) : '';
$video_sdk_token = !empty($webinar_data->video_sdk_token) ? esc_html($webinar_data->video_sdk_token) : '';
$video_sdk_api_key_global = get_option('wi_video_sdk_api_key', '');
$video_sdk_secret_key_global = get_option('wi_video_sdk_secret_key', '');

$video_sdk_api_key = !empty($video_sdk_api_key_webinar) ? $video_sdk_api_key_webinar : $video_sdk_api_key_global;
$video_sdk_secret_key = !empty($video_sdk_secret_key_webinar) ? $video_sdk_secret_key_webinar : $video_sdk_secret_key_global;
$use_as_global = false;
$video_sdk_type = !empty($webinar_data->video_sdk_type) ? esc_html($webinar_data->video_sdk_type) : 'video_sdk_hls'; // video_sdk_hls || video_sdk_rtc

if (
        empty($video_sdk_api_key_global) ||
        empty($video_sdk_api_key_webinar) ||
        $video_sdk_api_key_global === $video_sdk_api_key_webinar
) {
    $use_as_global = true;
}

$current_user = wp_get_current_user();
$users = get_users(array(
    'role__in'=> array(), // Get all roles, or specify 'subscriber', 'editor', etc.
    'orderby' => 'user_nicename',
    'order'   => 'ASC'
));

$hosts = !empty($webinar_data->video_sdk_hosts) ? esc_html($webinar_data->video_sdk_hosts) : '';
if (empty($hosts)) $hosts = $current_user->ID;

$hosts_ids_array = explode('|', $hosts);
$hosts_array = array();

foreach ($hosts_ids_array as $i => $host_id) {
    $index = array_search($host_id, array_column($users, "ID"));
    $host_user = ($index !== false) ? $users[$index] : false;

    if ($host_user) {
        $hosts_array[] = $host_user;
    } else {
        unset($hosts_ids_array[$i]);
    }
}

if (empty($hosts_array)) {
    $hosts = $current_user->ID;
    $hosts_ids_array = explode('|', $hosts);
    $hosts_array = array($current_user);
} else {
    $hosts = implode('|', $hosts_ids_array);
}

webinarignition_display_field(
    $input_get['id'],
    $video_sdk_api_key,
    esc_html__( 'VideoSDK Api key', 'webinar-ignition' ),
    'video_sdk_api_key',
    esc_html__( 'Input your VideoSDK API key here', 'webinar-ignition' ),
    'Input your VideoSDK API key here'
);

webinarignition_display_field(
    $input_get['id'],
    $video_sdk_secret_key,
    esc_html__( 'VideoSDK secret key', 'webinar-ignition' ),
    'video_sdk_secret_key',
    esc_html__( 'Input your VideoSDK secret key here', 'webinar-ignition' ),
    'Input your VideoSDK secret key here'
);

webinarignition_display_field(
    $input_get['id'],
    $video_sdk_token,
    esc_html__( 'VideoSDK token', 'webinar-ignition' ),
    'video_sdk_token',
    '',
    'Input your VideoSDK secret key here'
);

webinarignition_display_option(
    $input_get['id'],
    $use_as_global ? 'yes' : 'no',
    esc_html__( 'API key usage', 'webinar-ignition' ),
    'video_sdk_api_global_key_usage',
    esc_html__( 'Select if you want to use current Video SDK Api key only for current webinar, or use it globally for all webinars except ones that use their current webinar API key.', 'webinar-ignition' ),
    esc_html__( 'Global', 'webinar-ignition' ) . ' [yes], ' . esc_html__( 'Current Webinar', 'webinar-ignition' ) . ' [no]'
);

webinarignition_display_option(
    $input_get['id'],
    $video_sdk_type,
    esc_html__( 'VideoSDK webinar type', 'webinar-ignition' ),
    'video_sdk_type',
    '',
    esc_html__( 'Meeting', 'webinar-ignition' ) . ' [video_sdk_rtc], ' . esc_html__( 'Streaming', 'webinar-ignition' ) . ' [video_sdk_hls]'
);
?>

<div class="editSection">
    <div class="inputTitle">
        <div class="inputTitleCopy" ><?php echo __('Hosts', 'webinar-ignition'); ?></div>
    </div>
    <div class="inputSection">
        <ul id="videoSdkHosts" class="wi_editable_list">
            <?php
            foreach ($hosts_array as $host_user) {
                ?>
                <li>
                    <?php echo $host_user->display_name; ?>,  <?php echo $host_user->user_email; ?> (<?php echo __('id:', 'webinar-ignition'); ?> <?php echo $host_user->ID; ?>)
                    <span
                        class="wi_editable_item_remove"
                        data-id="<?php echo $host_user->ID; ?>"
                    >×</span>
                </li>
                <?php
            }
            ?>
        </ul>
        <input type="hidden" name="video_sdk_hosts" value="<?php echo $hosts ?>">
        <select id="videoSdkUsersList" class="inputField_select">
            <option>-- <?php echo __('select host', 'webinar-ignition'); ?> --</option>
            <?php
            foreach ($users as $user) {
                $user_id = $user->ID;
                ?>
                <option value="<?php echo $user_id ?>"
                    <?php echo in_array($user_id, $hosts_ids_array) ? 'disabled="disable"' : '' ?>
                >
                    <?php echo $user->display_name; ?>,  <?php echo $user->user_email; ?> (<?php echo __('id:', 'webinar-ignition'); ?> <?php echo $user->ID; ?>)
                </option>
                <?php
            }
            ?>
        </select>
    </div>
</div>

<div class="editSection infoSection">
    <h4>
        <i class="icon-question-sign"></i>
        <?php echo __( "What is VideoSDK", 'webinar-ignition' ) ; ?>
    </h4>
    <p>
        <?php echo __( "VideoSDK is a powerful service that provides real-time video communication directly on your website.", 'webinar-ignition' ) ; ?>
    </p>
    <p>
        <?php echo __( "VideoSDK is designed to scale with your needs. Its robust infrastructure ensures smooth and reliable video streaming, even as your audience grows.", 'webinar-ignition' ) ; ?>
    </p>
    <p>
        <?php
        echo sprintf(
            /* translators: 1: open anchor tag, 2: close anchor tag */
            __(
                'Login to %1$sYour Account%2$s or %3$sCreate Account%4$s to get API key.',
                'webinar-ignition'
            ),
            '<a href="https://app.videosdk.live/login" target="_blank">',
            '</a>',
            '<a href="https://app.videosdk.live/signup" target="_blank">',
            '</a>',
        );
        ?>
    </p>
</div>
