<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

// func :: webinarIgnition_dbug
// --------------------------------------------------------------------------------------
function webinarIgnition_dbug() {
	$cvs = phpversion();                                  // current version string
	$rvs = '5.4.9';                                       // required version string

	if ( version_compare( $cvs, $rvs, '<' ) ) {
		echo '<br>
               <div class="error" style="display:inline-block; padding:12px; margin-left:2px; margin-top:20px"><b>' . esc_html__( 'WARNING !!', 'webinar-ignition' ) . '</b><br>' . esc_html__( 'This plugin requires at least PHP version', 'webinar-ignition' ) . ' ' . esc_html( $rvs ) . ' ' . esc_html__( "but this server's installed version is older:", 'webinar-ignition' ) . ' ' . esc_html( $cvs ) . '<br><br>'
			. esc_html__( 'It is <strong>strongly</strong> recommended that you contact your hosting provider to upgrade your PHP installation to the required version or better.<br> If you ignore this, your software will throw errors or cause unwanted problems.', 'webinar-ignition' )
			. '</div>';
	}
}

function wi_date_difference( $date_1, $date_2, $difference_in = 'days' ) {

	switch ( $difference_in ) {

		case 'seconds':
			$difference_format = '%R%s';
			break;
		case 'minutes':
			$difference_format = '%R%i';
			break;
		case 'hours':
			$difference_format = '%R%h';
			break;
		case 'days':
			$difference_format = '%R%d';
			break;
		default:
			$difference_format = '%R%a';
			break;
	}

	$datetime1 = date_create( $date_1 );
	$datetime2 = date_create( $date_2 );

	$interval = date_diff( $datetime1, $datetime2 );

	$seconds = intval( $interval->format( '%R%s' ) );
	$minutes = intval( $interval->format( '%R%i' ) );
	$hours   = intval( $interval->format( '%R%h' ) );
	$days    = intval( $interval->days );

	if ( 'days' == $difference_in ) {
		return $days;
	} elseif ( 'hours' == $difference_in ) {
		return ( $days * 24 ) + $hours;
	} elseif ( 'minutes' == $difference_in ) {
		return ( $days * 24 * 60 ) + ( $hours * 60 ) + $minutes;
	} elseif ( 'seconds' == $difference_in ) {
		return ( $days * 24 * 60 ) + ( $hours * 60 ) + ( $minutes * 60 ) + $seconds;
	}

	return $interval->format( $difference_format );
}
// --------------------------------------------------------------------------------------


function webinarignition_dashboard() {
	$id = isset( $_GET['id'] ) ? absint( $_GET['id'] ) : null;
	$create = isset( $_GET['create'] ) ? sanitize_text_field( wp_unslash( $_GET['create'] ) ) : null; // Sanitize as plain text
	$webinars = isset( $_GET['webinars'] ) ? sanitize_textarea_field( wp_unslash( $_GET['webinars'] ) ) : null; // For larger text

	// fix :: notice on outdated PHP version
	// --------------------------------------------------------------------------------------
	webinarIgnition_dbug();
	// --------------------------------------------------------------------------------------

	// Universal Variables

	$sitePath = WEBINARIGNITION_URL;

	// UI FRAMEWORK
	include 'ui-core.php';
	include 'ui-com2.php';
	// The whole code of js-core.php is transferred to webinarignition-admin-dashboard.php File
	//include 'js-core.php';
	update_option( 'webinarignition_activated', 0 );

	// Acitvation Look Up ::
	global $wpdb;
	if(webinarignition_fs()->is__premium_only()){
		$statusCheck = WebinarignitionLicense::webinarignition_get_license_level(); // Added premium check
		$site_url = get_site_url(); 
		$statusCheck_title = ! empty( $statusCheck->title ) ? $statusCheck->title : __( 'Free', 'webinar-ignition' );
		$statusCheck = new stdClass();
		$statusCheck->switch = 'free';
		$statusCheck->slug = 'free';
		$statusCheck->licensor = '';
		$statusCheck->is_free = 1;
		$statusCheck->is_dev = '';
		$statusCheck->title = $statusCheck_title;
		$statusCheck->is_registered = '';
		$statusCheck->member_area = '';
		$statusCheck->is_pending_activation = 1;
		$statusCheck->upgrade_url = $site_url. '/wp-admin/admin.php?billing_cycle=annual&page=webinarignition-dashboard-pricing';
		$statusCheck->trial_url = $site_url.'/wp-admin/admin.php?billing_cycle=annual&trial=true&page=webinarignition-dashboard-pricing';
		$statusCheck->reconnect_url = $site_url.'/wp-admin/admin.php?nonce=fc5eb326b0&fs_action=webinar-ignition_reconnect&page=webinarignition-dashboard';
		$statusCheck->account_url = $site_url.'/wp-admin/admin.php?page=webinarignition-dashboard-account';
		$statusCheck->name = '';
	}
	else{
		// Create a new stdClass object
		$site_url = get_site_url(); 
		$statusCheck = new stdClass();

		$statusCheck->switch = 'free';
		$statusCheck->slug = 'free';
		$statusCheck->licensor = '';
		$statusCheck->is_free = 1;
		$statusCheck->is_dev = '';
		$statusCheck->is_registered = '';
		$statusCheck->title = 'Free';
		$statusCheck->member_area = '';
		$statusCheck->is_pending_activation = 1;
		$statusCheck->upgrade_url = $site_url. '/wp-admin/admin.php?billing_cycle=annual&page=webinarignition-dashboard-pricing';
		$statusCheck->trial_url = $site_url.'/wp-admin/admin.php?billing_cycle=annual&trial=true&page=webinarignition-dashboard-pricing';
		$statusCheck->reconnect_url = $site_url.'/wp-admin/admin.php?nonce=fc5eb326b0&fs_action=webinar-ignition_reconnect&page=webinarignition-dashboard';
		$statusCheck->account_url = $site_url.'/wp-admin/admin.php?page=webinarignition-dashboard-account';
		$statusCheck->name = '';
	}

	

	
	$webinarignition_dashboard_link = admin_url( '?page=webinarignition-dashboard' );

	if( webinarignition_fs()->is__premium_only() ){
		$license_title = ! empty( $statusCheck->title ) ? $statusCheck->title : __( 'Free', 'webinar-ignition' );
		$license_name = ! empty( $statusCheck->name ) ? $statusCheck->name : __( 'Free', 'webinar-ignition' );
		$promo_expired = ( time() > 1672549199 );
		$is_basic_pro = ( in_array( $statusCheck->switch, array( 'pro', 'basic' ) ) && ! in_array( $statusCheck->name, array( 'ultimate_powerup_tier2a', 'ultimate_powerup_tier3a' ) ) );
		$optin_button_text = __( 'Opt-in for More Free Registrations, License Options', 'webinar-ignition' );
		$limit_settings_decoded = WebinarignitionLicense::webinarignition_get_limitation_settings();
		$limit_users            = isset( $limit_settings_decoded['limit_users'] ) ? $limit_settings_decoded['limit_users'] : '5';
		$user_count             = WebinarignitionLicense::webinarignition_get_limit_counter();

		$user_left              = absint( $limit_users ) - absint( $user_count );
		$user_left              = absint( $user_left );

		$progress = ( 0 !== absint( $limit_users ) ) ? ( absint( $user_count ) / absint( $limit_users ) ) * 100 : 0;

		$bg_color = 0;

		$total_number_of_days = 30;

		$act_now_text = __( 'Act Now!', 'webinar-ignition' );
		if ( $progress >= 93 ) {
			$act_now_text = __( 'Act Now to open!', 'webinar-ignition' );
			$bg_color = 'rgb(255 71 45)';
		} elseif ( $progress >= 80 ) {
			$bg_color = 'rgb(194 163 43)';
		}

		$limit_count_timeout = (int) get_option( 'webinarignition_limit_timeout' );

		if ( empty( $limit_count_timeout ) ) {
			$limit_count_timeout = strtotime( '+30 days' );
		}

		$starting_time = strtotime( '-29 days', $limit_count_timeout );
		$ending_time   = $limit_count_timeout;
		$current_time  = time();

		$starting_date = gmdate( 'y-m-d', $starting_time );
		$current_date  = gmdate( 'y-m-d', $current_time );

		$current_number_of_days = absint( wi_date_difference( $starting_date, $current_date, 'days' ) );

		$total_count_ratio   = ( $limit_users > 0 && $total_number_of_days > 0 ) ? $limit_users / $total_number_of_days : 0;
		$current_count_ratio = ( $user_count > 0 && $current_number_of_days > 0 ) ? $user_count / $current_number_of_days : 0;

		$reset_date = gmdate( get_option( 'date_format' ), $limit_count_timeout );

		$wi_db_url = add_query_arg( 'page', 'webinarignition-dashboard', admin_url( 'admin.php' ) );
		if( webinarignition_fs()->is__premium_only() ){
			if ( $statusCheck && $statusCheck->name == 'ultimate_powerup_tier1a' ) {
				$wi_db_url = $statusCheck->trial_url;
			}
		}

		$act_now_link = sprintf( '<a class="wi-dashboard-link" href="%s" target="_blank"><strong>%s</strong></a>', $wi_db_url, $act_now_text );

		/* translators: %s: Date when the reset will occur */
		$reset_date_message = sprintf( esc_html__( 'Reset on %s', 'webinar-ignition' ), $reset_date );
	}
	?>
	<div id="mWrapper">
		<div id="mHeader" style="background-color: #353337;">
			<div id="mLogo">
				<div class="webinar_logo_license_cont">
					<div class="mLogoIMG">
					<?php if( webinarignition_fs()->is__premium_only() ){
							if ( $statusCheck && ! $is_basic_pro && $statusCheck->title !== 'Free' ) : ?>
								<a href="<?php echo esc_url($webinarignition_dashboard_link); ?>"><img class="welogo" style="padding-top: 17px;" src="<?php echo esc_url(WEBINARIGNITION_URL); ?>images/webinarignition.png" width="284" alt="" border="0"></a>
								<?php else : ?>
								<img class="welogo" style="padding-top: 10px;" src="<?php echo esc_url(WEBINARIGNITION_URL); ?>images/webinarignition-white-grey.png" width="284" alt="" border="0">
							<?php endif; 
												
							}else{ ?>
								<img class="welogo" style="padding-top: 10px;" src="<?php echo esc_url(WEBINARIGNITION_URL); ?>images/webinarignition-white-grey.png" width="284" alt="" border="0">		
							<?php } 
							?>
					</div>

					<?php
			if ( webinarignition_fs()->is__premium_only() ){

					if(webinarignition_fs()->is__premium_only()){
						?>
							<h3 id="licenseTitle"><?php echo esc_html( $license_title ); ?></h3>
						<?php
					}else{
						if (function_exists('webinarignition_fs')) {
							$is_license_activated = webinarignition_fs()->can_use_premium_code();
						} else {
							$is_license_activated = false; // Freemius is not available
						}
						if( $is_license_activated ){
							if ( $statusCheck && ! empty( $statusCheck->account_url ) ) {
								?>
								<a href="<?php echo esc_url( $statusCheck->account_url ); ?>" class="btn btn-primary mSupport" title="<?php esc_html_e( 'License bought after 01/2021', 'webinar-ignition' ); ?>">
									<i class="icon-star" style="margin-right: 5px;"></i>
									<?php esc_html_e( 'Download Paid Version', 'webinar-ignition' ); ?>
								</a>
								<?php
							}
						}
						else{
							?>
							<h3 id="licenseTitle"><?php echo esc_html( $license_title ); ?></h3>
							<?php
						}
					}
				}
					?>
					
				</div>
				<?php
				if (
					$statusCheck && (
					empty( $statusCheck ) ||
					$statusCheck->switch == '' ||
					! empty( $statusCheck->is_fs ) ||
					empty( $statusCheck->keyused ) )
				) {

				} else {

					$is_freemius_not_registered = ( ! empty( $statusCheck->reconnect_url ) && ! $statusCheck->is_registered );

					if ( $is_freemius_not_registered ) {
						?>
				<style>
					.WIheaderRight {
						width: 80%;
						float: right;
						position: relative;
						display: table;
						padding: 0 14px;
						line-height: 47px;
					}
					.mSupport {
						margin-top: 0px;
						margin-right: 0px;
					}
					.mSupport:last-child {
						margin-right: 0px;
					}
				</style>
				<?php } ?>
					<div class="WIheaderRight">
						<button
								data-toggle="collapse"
								data-target="#unlockFormsContainer"
								aria-expanded="false"
								aria-controls="unlockFormsContainer"
								class="btn btn-primary mSupport"
								title="<?php esc_html_e( 'License bought before 01/2021', 'webinar-ignition' ); ?>"
						>
							<i class="icon-key" style="margin-right: 5px;"></i>
							<?php esc_html_e( 'Manage license', 'webinar-ignition' ); ?>
						</button>
						<?php
				}//end if


?>
				<div class="wi-head-btns-wrap">

				<?php
				if ( webinarignition_fs()->is__premium_only()  ){ 
					if ( $statusCheck && ! empty( $statusCheck->account_url ) ) {
						?>
						<a href="<?php echo esc_url( $statusCheck->account_url ); ?>" class="btn btn-primary mSupport" title="<?php esc_html_e( 'License bought after 01/2021', 'webinar-ignition' ); ?>">
							<i class="icon-user" style="margin-right: 5px;"></i>
							Freemius <?php esc_html_e( 'Account', 'webinar-ignition' ); ?>
						</a>
						<?php
					}
				}
				?>

				<a href="<?php echo esc_url( get_admin_url() . 'admin.php?page=webinarignition_support' ); ?>" class="btn btn-primary mSupport"><i class="icon-question-sign" style="margin-right: 5px;"></i> <?php esc_html_e( 'Solution Center', 'webinar-ignition' ); ?></a>
				</div>
			</div>
		</div>

		<div id="container">
			<?php 
			// Edit App
			if ( isset( $id ) ) {
				include 'editapp.php';
			} elseif ( isset( $create ) ) {
				include 'create.php';
			} elseif ( isset( $webinars ) ) {
				include 'webinars-list.php';
			} else {
				include 'dash.php';
			}
			?>

		</div>
	</div>
	<?php
	// END
}
