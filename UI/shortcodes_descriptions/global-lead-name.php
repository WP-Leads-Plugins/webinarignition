<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
/**
 * @var $webinar_data
 * @var $is_list
 */

$webinarId = $webinar_data->id;

if ( ! empty( $is_list ) ) {
	?><?php
} else {
	?><?php
}
?>

<h4>
	<?php echo esc_html__( 'Webinar info', 'webinar-ignition' ); ?>
</h4>

<p>
	<?php echo esc_html__( 'Webinar Title', 'webinar-ignition' ); ?>: <strong><?php echo ! empty( $webinar_data->webinar_desc ) ? esc_attr( $webinar_data->webinar_desc ) : ''; ?></strong>
</p>
<p class="code-example">
	<span class="code-example-value">[wi_webinar_block id="<?php echo esc_attr( $webinarId ); ?>" block="global_webinar_title"]</span><!--
	--><span class="code-example-copy"><?php echo esc_html__( 'Copy', 'webinar-ignition' ); ?></span><!--
	--><span class="code-example-copied"><?php echo esc_html__( 'Copied. Input into your content!', 'webinar-ignition' ); ?></span>
</p>

<hr>

<p>
	<?php echo esc_html__( 'Webinar Host Name', 'webinar-ignition' ); ?>: <strong><?php echo ! empty( $webinar_data->webinar_host ) ? esc_html( $webinar_data->webinar_host ) : ''; ?></strong>
</p>
<p class="code-example">
	<span class="code-example-value">[wi_webinar_block id="<?php echo esc_attr( $webinarId ); ?>" block="global_host_name"]</span><!--
	--><span class="code-example-copy"><?php echo esc_html__( 'Copy', 'webinar-ignition' ); ?></span><!--
	--><span class="code-example-copied"><?php echo esc_html__( 'Copied. Input into your content!', 'webinar-ignition' ); ?></span>
</p>

<hr>

<p>
	<?php echo esc_html__( 'Webinar Giveaway section content', 'webinar-ignition' ); ?>:
</p>
<div style="border: 1px solid #ddd; background-color: #eee;padding: 5px 10px;margin-bottom: 10px;">
	<?php webinarignition_display( $webinar_data->webinar_giveaway, '<h4>' . __( 'Your Awesome Free Gift</h4><p>You can download this awesome report made you...', 'webinar-ignition' ) . '</p><p>[ ' . __( 'DOWNLOAD HERE', 'webinar-ignition' ) . ' ]</p>' ); ?>
</div>
<p class="code-example">
	<span class="code-example-value">[wi_webinar_block id="<?php echo esc_attr( $webinarId ); ?>" block="global_webinar_giveaway"]</span><!--
	--><span class="code-example-copy"><?php echo esc_html__( 'Copy', 'webinar-ignition' ); ?></span><!--
	--><span class="code-example-copied"><?php echo esc_html__( 'Copied. Input into your content!', 'webinar-ignition' ); ?></span>
</p>

<hr> 

<h4>
	<?php echo esc_html__( 'Lead info', 'webinar-ignition' ); ?>
</h4>

<p>
	<?php echo esc_html__( 'Lead info could be only get after registration. So you should not use shortcodes below it on registration pages.', 'webinar-ignition' ); ?>
</p>

<p>
	<?php echo esc_html__( 'Lead Name', 'webinar-ignition' ); ?>: <strong><?php echo esc_html__( 'John Doe', 'webinar-ignition' ); ?></strong>
</p>
<p class="code-example">
	<span class="code-example-value">[wi_webinar_block id="<?php echo esc_html( $webinarId ); ?>" block="global_lead_name"]</span><!--
	--><span class="code-example-copy"><?php echo esc_html__( 'Copy', 'webinar-ignition' ); ?></span><!--
	--><span class="code-example-copied"><?php echo esc_html__( 'Copied. Input into your content!', 'webinar-ignition' ); ?></span>
</p>

<hr>

<p>
	<?php echo esc_html__( 'Lead Email', 'webinar-ignition' ); ?>: <strong><?php echo esc_html__( 'john.doe@maildomain.com', 'webinar-ignition' ); ?></strong>
</p>

<p class="code-example">
	<span class="code-example-value">[wi_webinar_block id="<?php echo esc_html( $webinarId ); ?>" block="global_lead_email"]</span><!--
	--><span class="code-example-copy"><?php echo esc_html__( 'Copy', 'webinar-ignition' ); ?></span><!--
	--><span class="code-example-copied"><?php echo esc_html__( 'Copied. Input into your content!', 'webinar-ignition' ); ?></span>
</p>
<p>
	<?php echo esc_html__( 'Footer Shortcode', 'webinar-ignition' ); ?>
</p>

<p class="code-example">
	<span class="code-example-value">[webinarignition_footer]</span><!--
	--><span class="code-example-copy"><?php echo esc_html__( 'Copy', 'webinar-ignition' ); ?></span><!--
	--><span class="code-example-copied"><?php echo esc_html__( 'Copied. Input into your content!', 'webinar-ignition' ); ?></span>
</p>