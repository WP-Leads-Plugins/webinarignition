<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
/**
 * @var $webinar_data
 * @var $is_list
 */

$webinarId = $webinar_data->id;

if ( ! empty( $is_list ) ) {
	?><p>
	<?php esc_html_e( 'If you need to show video and sidebar sections together, you can use two shortcodes below. Only working with first, left, new webinar room design!', 'webinar-ignition' ); ?>
	</p><?php
} else {
	?><?php
}
?>

<p class="code-example">
	<span class="code-example-value">[wi_webinar_block id="<?php echo esc_html($webinarId); ?>" block="webinar_video_cta_sidebar"]</span><!--
	--><span class="code-example-copy"><?php esc_html_e( 'Copy', 'webinar-ignition' ); ?></span><!--
	--><span class="code-example-copied"><?php esc_html_e( 'Copied. Input into your content!', 'webinar-ignition' ); ?></span>
</p>
