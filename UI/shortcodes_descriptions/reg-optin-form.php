<?php if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
/**
 * @var $webinar_data
 * @var $is_list
 */

$webinarId = $webinar_data->id;
if(class_exists('NextendSocialLogin')) {
	$webinarignition_registration_shortcode		  	= get_option( 'webinarignition_registration_shortcode', '[nextend_social_login]' );
}else{
	$webinarignition_registration_shortcode		  	= get_option( 'webinarignition_registration_shortcode', '' );
}
if ( ! empty( $is_list ) ) {
	?><?php
} else {
	?><?php
}
?>

<hr>
<h4>
	<?php echo esc_html__( 'Registration form', 'webinar-ignition' ); ?>
</h4>
<p>
	<?php echo esc_html__( 'This shortcode will show optin section with webinar dates selection and registration form together in one column', 'webinar-ignition' ); ?>
</p>

<p class="code-example">
	<span class="code-example-value">[wi_webinar_block id="<?php echo esc_html( $webinarId ); ?>" block="reg_optin_section" readonly="false"]</span><!--
	--><span class="code-example-copy"><?php echo esc_html__( 'Copy', 'webinar-ignition' ); ?></span><!--
	--><span class="code-example-copied"><?php echo esc_html__( 'Copied. Input into your content!', 'webinar-ignition' ); ?></span>
</p>

<p>
	<?php echo esc_html__( 'This shortcode will be used to show the auto login button on the registration page ', 'webinar-ignition' ); ?>
</p>
<?php  if(isset($webinarignition_registration_shortcode) || $webinarignition_registration_shortcode != '') { ?>
<p class="code-example">
	<span class="code-example-value"><?php echo $webinarignition_registration_shortcode; ?></span><!--
	--><span class="code-example-copy"><?php echo esc_html__( 'Copy', 'webinar-ignition' ); ?></span><!--
	--><span class="code-example-copied"><?php echo esc_html__( 'Copied. Input into your content!', 'webinar-ignition' ); ?></span>
</p>
<?php } ?>
<p>
	<?php echo esc_html__( 'If you need to show dates selection and registration form in different page blocks, you need to use both shortcodes below', 'webinar-ignition' ); ?>
</p>

<p><?php echo esc_html__( 'Webinar dates selection', 'webinar-ignition' ); ?></p>
<p class="code-example">
	<span class="code-example-value">[wi_webinar_block id="<?php echo esc_html( $webinarId ); ?>" block="reg_optin_dates"]</span><!--
	--><span class="code-example-copy"><?php echo esc_html__( 'Copy', 'webinar-ignition' ); ?></span><!--
	--><span class="code-example-copied"><?php echo esc_html__( 'Copied. Input into your content!', 'webinar-ignition' ); ?></span>
</p>

<p><?php echo esc_html__( 'Optin form fields', 'webinar-ignition' ); ?></p>
<p class="code-example">
	<span class="code-example-value">[wi_webinar_block id="<?php echo esc_html( $webinarId ); ?>" block="reg_optin_form" readonly="false"]</span><!--
	--><span class="code-example-copy"><?php echo esc_html__( 'Copy', 'webinar-ignition' ); ?></span><!--
	--><span class="code-example-copied"><?php echo esc_html__( 'Copied. Input into your content!', 'webinar-ignition' ); ?></span>
</p>

<p>
	<?php echo esc_html__( 'If you want to show dates selection and optin fields without heading, you can use compact views below', 'webinar-ignition' ); ?>
</p>

<p class="code-example">
	<span class="code-example-value">[wi_webinar_block id="<?php echo esc_html( $webinarId ); ?>" block="reg_optin_dates_compact"]</span><!--
	--><span class="code-example-copy"><?php echo esc_html__( 'Copy', 'webinar-ignition' ); ?></span><!--
	--><span class="code-example-copied"><?php echo esc_html__( 'Copied. Input into your content!', 'webinar-ignition' ); ?></span>
</p>

<p class="code-example">
	<span class="code-example-value">[wi_webinar_block id="<?php echo esc_html( $webinarId ); ?>" block="reg_optin_form_compact" readonly="false"]</span><!--
	--><span class="code-example-copy"><?php echo esc_html__( 'Copy', 'webinar-ignition' ); ?></span><!--
	--><span class="code-example-copied"><?php echo esc_html__( 'Copied. Input into your content!', 'webinar-ignition' ); ?></span>
</p>


<?php
if ( $webinar_data->webinar_date !== 'AUTO' ) {
	?>
	<p class="code-example">
	<span class="code-example-value">[wi_webinar_block id="<?php echo esc_html( $webinarId ); ?>" block="reg_date_inline"]</span><!--
	--><span class="code-example-copy"><?php echo esc_html__( 'Copy', 'webinar-ignition' ); ?></span><!--
	--><span class="code-example-copied"><?php echo esc_html__( 'Copied. Input into your content!', 'webinar-ignition' ); ?></span>
	</p>

	<p class="code-example">
		<span class="code-example-value">[wi_webinar_block id="<?php echo esc_html( $webinarId ); ?>" block="reg_time_inline"]</span><!--
	--><span class="code-example-copy"><?php echo esc_html__( 'Copy', 'webinar-ignition' ); ?></span><!--
	--><span class="code-example-copied"><?php echo esc_html__( 'Copied. Input into your content!', 'webinar-ignition' ); ?></span>
	</p>

	<p class="code-example">
		<span class="code-example-value">[wi_webinar_block id="<?php echo esc_html( $webinarId ); ?>" block="reg_timezone_inline"]</span><!--
	--><span class="code-example-copy"><?php echo esc_html__( 'Copy', 'webinar-ignition' ); ?></span><!--
	--><span class="code-example-copied"><?php echo esc_html__( 'Copied. Input into your content!', 'webinar-ignition' ); ?></span>
	</p>
	<?php
}//end if
?>
