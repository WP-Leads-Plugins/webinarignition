<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
/**
 * @var $webinar_data
 * @var $is_list
 */

$webinarId = $webinar_data->id;

if ( ! empty( $is_list ) ) {
	?><p>
	<?php esc_html_e( 'Webinar video and Call To Actions section together', 'webinar-ignition' ); ?>
	</p><?php
} else {
	?><?php
}
?>

<p class="code-example">
	<span class="code-example-value">[wi_webinar_block id="<?php echo esc_html($webinarId); ?>" block="webinar_video_cta"]</span><!--
	--><span class="code-example-copy"><?php esc_html_e( 'Copy', 'webinar-ignition' ); ?></span><!--
	--><span class="code-example-copied"><?php esc_html_e( 'Copied. Input into your content!', 'webinar-ignition' ); ?></span>
</p>
