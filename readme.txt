== Webinar Solution: Create live/evergreen/automated/instant webinars, stream & Zoom Meetings | WebinarIgnition ==
Contributors: tobias_conrad, freemius
Plugin URI:	https://webinarignition.com/
Author URI:	https://profiles.wordpress.org/tobias_conrad/#content-plugins
Tags: webinar, stream, live, zoom, seminar
Requires at least: 5.0
Tested up to: 6.7.1 
Requires PHP: 7.2.5
Stable tag: 4.00.1
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-3.0.html

Convert your visitors into customers. Run webinars on your website with your design. With a fully extendable attendee experience inside the webinar.

== External Services ==
This plugin interacts with multiple external services to provide its functionality. Below is a detailed explanation of each service, the data exchanged, and their terms and privacy policies:

1. **Google APIs**
   - Purpose: Used for authentication and data handling related to webinars and analytics.
   - Data Sent: User email addresses, webinar registration details, and metadata.
   - Domains: `https://www.googleapis.com`
   - Privacy Policy: [Google Privacy Policy](https://policies.google.com/privacy)
   - Terms of Service: [Google API Terms](https://cloud.google.com/terms/)

2. **cURL**
   - Purpose: Used for making HTTP requests to external services.
   - Data Sent: Variable based on the configured service request.
   - Documentation: [cURL Documentation](https://www.php.net/manual/en/book.curl.php)

3. **Freemius**
   - Purpose: For licensing, subscription management, and plugin insights.
   - Data Sent: Site URL, user email, plugin version, and activation data.
   - Domains: `https://api.freemius.com`
   - Privacy Policy: [Freemius Privacy Policy](https://freemius.com/privacy/)

4. **LinkedIn Platform**
   - Purpose: For social sharing and integration.
   - Data Sent: No user data is sent directly; the service is loaded via external scripts.
   - Domains: `https://platform.linkedin.com`
   - Privacy Policy: [LinkedIn Privacy Policy](https://www.linkedin.com/legal/privacy-policy)

5. **Twitter Platform**
   - Purpose: For embedding Twitter widgets in webinar pages.
   - Data Sent: No user data is sent directly; the service is loaded via external scripts.
   - Domains: `https://platform.twitter.com`
   - Privacy Policy: [Twitter Privacy Policy](https://twitter.com/en/privacy)

6. **Moment.js**
   - Purpose: For date and time manipulation in webinar scheduling.
   - Data Sent: No data is sent externally.
   - Documentation: [Moment.js Documentation](https://momentjs.com/)

7. **Picker.js**
   - Purpose: For date and time picking functionality.
   - Data Sent: No data is sent externally.
   - Documentation: [Picker.js Documentation](https://github.com/flatpickr/flatpickr)

8. **Bootstrap**
   - Purpose: For responsive design and UI components.
   - Data Sent: No data is sent externally.
   - Domains: `https://cdn.jsdelivr.net/npm/bootstrap@5.0.2`
   - Documentation: [Bootstrap Documentation](https://getbootstrap.com/)

9. **Polyfills**
    - Purpose: Ensure compatibility of modern JavaScript features in older browsers.
    - Data Sent: No data is sent externally.
    - Documentation: [Core-js Documentation](https://github.com/zloirock/core-js), [MDN Polyfills](https://developer.mozilla.org/en-US/docs/Glossary/Polyfill)

10. **Video.js**
    - Purpose: For video player functionalities.
    - Data Sent: No data is sent externally.
    - Documentation: [Video.js Documentation](https://videojs.com/)

11. **jQuery**
    - Purpose: For DOM manipulation and event handling.
    - Data Sent: No data is sent externally.
    - Documentation: [jQuery Documentation](https://jquery.com/)

12. **Bootbox.js**
    - Purpose: For managing modal dialogs with Bootstrap styles.
    - Data Sent: No data is sent externally.
    - Documentation: [Bootbox.js Documentation](https://bootboxjs.com/)

13. **Luxon**
    - Purpose: For modern date and time manipulation, including time zone handling and date formatting.
    - Data Sent: No data is sent externally.
    - Documentation: [Luxon Documentation](https://moment.github.io/luxon/#/)

14. **PeriodicalUpdater**
    - Purpose: For making periodic, decaying AJAX requests to a server.
    - Data Sent: Custom data defined in the plugin configuration and AJAX response handling.
    - Documentation: [PeriodicalUpdater Documentation](http://www.360innovate.co.uk/blog/2009/03/periodicalupdater-for-jquery/)
    - Dependencies:
      - jQuery
      - Optionally, the `jquery-cookie` plugin for cookie-based interval persistence.
      
== Domain Usage Disclosure ==

The following external scripts are loaded in the plugin and their respective domains are used:

- `https://platform.linkedin.com/in.js`: Used for LinkedIn sharing widgets.
- `https://js.stripe.com/v2/`: Used for Stripe payment processing.
- `https://platform.twitter.com/widgets.js`: Used for embedding Twitter widgets.
- `https://cdn.jsdelivr.net/npm/bootstrap@5.0.2`: Used for Bootstrap framework.
- `https://vjs.zencdn.net/`: Used for Video.js functionality.

Each of these domains has been disclosed, and their respective privacy policies are linked in the "External Services" section above.




== Description ==

= Welcome & Overview video =

https://www.youtube.com/watch?v=vr1mJeigX-Y

= YEAH i did it: Recorded me in a Live webinar = 

https://www.youtube.com/watch?v=TVrwl6Fhq38

feedback please…

= Quick start Evergreen in under 15 minutes =

https://www.youtube.com/watch?v=6WlBejkFyEE

= See Dave´s webinar story with an happy end =

https://www.youtube.com/watch?v=BR8F1FC0zPw

= Custom Support Reviews, the other way of introducing Webinar Plugin for WordPress - WebinarIgnition =

https://www.youtube.com/watch?v=A5x1djj9-_w

[Link to text reviews (please right click and open in new tab)](https://wordpress.org/plugins/webinar-ignition/#reviews)
[Link to video reviews (please right click and open in new tab)](https://webinarignition.com/#customer-voices_probably-best-rated-webinar-software-for-wp)

[To get the same results and maybe write a review about -> Book your free appointment(s) now!](https://webinarignition.com/?fluentcrm=1&route=smart_url&slug=bg6lha9)

= For high conversions, the unlimited CTAs (Call-to-Actions) are inside the webinar = 

Inside webinar converts much better and more often, than the button leading to an external page. Compare this with an external sales agency and an in-house sales team. [To see CTA details: Right click link and open in new tab](https://webinarignition.tawk.help/article/do-you-also-have-only-one-action-in-webinar)

https://www.youtube.com/watch?v=mzQsHzRkNkQ

[See the in webinar CTAs here (please right click and open in new tab)](https://webinarignition.com/demo)
-

https://www.youtube.com/watch?v=QuGt_-VLI0Q

[CTA loading issues? Load all plugins/shortcodes in CTAs (please right click and open in new tab)](https://webinarignition.tawk.help/article/cta-loading-issues)


= To keep the design the people trust, build all your webinar pages in your page builder = 

and add the webinar functionality via shortcodes (placeholder). Select the custom page and WebinarIgnition replace the default pages. [To see design details: Right click link and open in new tab](https://webinarignition.tawk.help/article/create-your-own-designed-webinar-landing-pages_webinar-registration-pages)

https://www.youtube.com/watch?v=IKpHHeynMOE

= Scale up your income with a fully marketing automation integration =

Use webhooks (on register, attended, purchased) to send user data to any CRM, service to include WebinarIgnition into your complete marketing. [To see webhooks details: Right click link and open in new tab](https://webinarignition.tawk.help/article/outgoing-webhooks-sending-attendees-data-anywhere)

= To ensure known user registered when clicked a link (in a mail ect.), use less click registration = 

to skip registration page and redirect user directly to the thank you page, webinar page, ... [To see integration details: Right click link and open in new tab](https://webinarignition.tawk.help/article/seemlessly-integration-in-your-funnel-marketing-crm)

= You like to receive before giving? = 

We start giving by offering free setup service and also we want to know you better in the free appointments. [See available dates: Right click link and open in new tab](https://webinarignition.com/?fluentcrm=1&route=smart_url&slug=bg6lha9)

= Get know quickly & free if the webinar solution is the right for you and use all features for free =

Opt-in to the free version to get more registration, up to 500 registrations for free are possible per month.

= For more webinar success book your free 1:1 setup appointment now. Bring in all your questions and requirements = 
[Book here](https://webinarignition.com/?fluentcrm=1&route=smart_url&slug=bg6lha9)

= See what you get, in the hands on overview (01-2022) =

https://www.youtube.com/watch?v=-pt6nTJ_8cM

= Quick start evergreen webinar the WordPress way (08-2022) =

https://www.youtube.com/watch?v=4xDuq-pJBkY 

[Click to see the videos](https://wordpress.org/plugins/webinar-ignition/)

[Click to see the big knowledge base](https://webinarignition.tawk.help/)

[To get ideas, examples for your own webinars visit updated evergreen demo pages please](https://webinarignition.com/demo)

== Content of the page ==

* Quick start video
* Unique features
* Ultimate features
* Unique Call-to-Actions
* Evergreen DEMO
* Tutorials
* Best Webinar platform to start with all ultimate features

= What makes WebinarIgnition services so unique =

* Add any action INSIDE the webinar room above video or in the sidebar
* Build custom webinar pages with shortcodes
* Implement any functionality by plugin shortcode
* Restricted white-labeled host & supporter areas
* Any language possible, German, English included
* Nice styled email reminders
* Comprehensive SMTP settings and spamyness test

= WebinarIgnition Ultimate Features =

* Multiple Call-to-Actions to boost sales in Evergreen/Automated and Live Webinars
* Add webinar functionality to your design and layout with webinar shortcodes. Use your own page builder like Thrive themes, Elementor, Oxygen, OptimizePress, Beaver page builder, Avada, Enfold, flatsome, Pootlepress build-in editors.
* Modern webinar room with flexible sidebar
* 2-way Q&A chat system: multi-staff, hosts (Live webinars only at the moment)
* White Label the Webinar Console for staff and hosts
* Lock-out later comers and redirect them
* Unlimited & flexible evergreen dates & times (From instant to once a week
* Social sharing buttons & icons
* and much much more...

= See innovative, unique Call-to-Action (CTA) =
which keep visitor in action, INSIDE the webinar and boost your conversions

https://www.youtube.com/watch?v=sXl3H_R4fHw

[See CTA KnowledgeBase article](https://webinarignition.tawk.help/article/do-you-also-have-only-one-action-in-webinar)

= Evergreen & Automated Webinar/Seminar DEMO =
See own styled webinar registration page, multiple Call-to-Actions in modern webinar room with tabs and more ...
[Go to the Evergreen & Auto Webinar Demo](https://webinarignition.com/demo/)

= Webinar's User features (features that webinar's attendees see) =

* Multiple Email & Text Message (SMS) Reminders

* Q&A section integrated and third-party Q&A / 2-way chat support

* Add to Calendar allows users to add the event to their Google Calendars once they have registered for the webinar

* "On-Air Message" allows you to push any Call to Action onto your live webinar page when you are ready. Perfect for when you are pitching your product. Push a CTA button or push any HTML or Plugin Short that you would like. That lets users do shopping, polls, appointments etc. inside webinar, live stream or online video call

* Fully translatable (language), fully localised (date&time format)

Machine translated (DeepL):
LTR

* es_ES, 	Spanish (Spain)
* es_MX, 	Spanish (Mexico)
* fr_FR, 	French (France)
* hi_IN,		Hindi (India) NEW
* HR,		Croatian (Croatia) NEW
* it_IT, 		Italian (Italy)
* JA, 		Japanese (Japan)
* nl_NL, 	Dutch (Netherland)
* pt_BR, 	Portuguese (Brazil)
* ru_RU, 	Russian (Russia)
* UK   , 	Ukrainian (Ukraine)
* zh_CN, 	Chinese (China)
* hu_HU	    Hungarian,
* tr_TR,	Turkish

RTL

* UR   , 	Urdu (Pakistan and India) (!) site needs to be english
* More machine translations for free on request via chat

Hand translated:

* us_US, English (default)
* de_DE, German informal (NOT SIE) (used for all german speaking countries)

= Admin features =

* Customizable Registration, Confirmation, & Live Pages (Free)
* Add the webinar features via shortcodes to your designed pages (Ultimate)
* Embed any video feed such as Youtube Live, Facebook LiveStream and Ustream into your Live Webinar Page.
* Embed any call- and meeting feed such as Zoom meetings, Jitsi Meet, into your Live Webinar Page.

* Sales and Conversion Tracking with provided tracking script (Live console).

* Integrate with any email auto-responder including top providers like GetResponse, Aweber, iContact, SendReach, Mautic and MailChimp.
For German speakers we offer a full Webinar integration with KlickTipp via WP2LEADS, see more in the FAQ below.
[see more in the FAQ below.)](https://wordpress.org/plugins/webinar-ignition/#has%20webinarignition%20a%20full%20integration%20with%20klicktipp%20email%20marketing%20service%3F)
[see more in FAQ below. only #)](#has%20webinarignition%20a%20full%20integration%20with%20klicktipp%20email%20marketing%20service%3F)

* "Live Console" to easily manage your webinar or stream as it is happening.
Monitor the number of attendees in your webinar or video conference calls in real-time (only live), total users,
total sales/revenue, see and answer questions, export, import (only live) leads and more!

* Paid Online or Recorded Webinars/Seminars/Streams: Host and charge your registrants a fee to attend one of your webinars, streams, live Q&As, video conferences and more. Integrates with virtually any payment gateway.

* 1-Click Registration: Register 100% of your subscribers for a live webinar with the click of a link. As soon as someone clicks a link in your email, it will automatically register them for your webinar event.
Usage example: Register your paid clients directly from your email provider after receiving payments notification inside your email provider.
Evergreen 1-Click Registration on request.

= Overview video =
Video is some years old, a lot redone and ultimate features added since the beginning of 2021 are not included ;-)

https://www.youtube.com/watch?v=HyB2KXP_cgU

= Tutorials - Clear & Easy Instructions =

* Tutorial for [Jitsi Meet Integration](https://webinarignition.tawk.help/article/jitsi-integration)
* Tutorial for [Zoom Meeting, Call and Webinars Integration](https://webinarignition.tawk.help/article/zoom-integration)

Use Jitsi Meet and Zoom Meetings, Zoom Calls and Live Webinars inside WebinarIgnition and you can:

* get email address for webinar content,
* send email reminders, notifications
* track and engage with your audience & potential leads
* convert more with inside webinar custom Call-To-Actions (More sales, more appointments, more referrers, ...).

[Lots of useful tutorials, solution videos & more (right click and open in new window please)](https://support.webinarignition.com/)

[See all changes here](https://plugins.trac.wordpress.org/browser/webinar-ignition/trunk/changlog.txt)

[Join the community of successful webinar marketers by buy your license on website](https://webinarignition.com/#pricing) or 
for easy license activation, buy directly inside the plugin please.

== Installation ==

1. Upload the WebinarIgnition folder to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress

== Screenshots ==

1. Webinar Registration page designed with own page builder (elementor) and the WebinarIgnition registration shortcode (Registration in PopUp or inside page)
2. Webinar Registration Page (mobile responsive, 3 default layouts, style settings available)
3. Webinar confirmation page (1 of 3 default styles), shortcodes to design your own available
4. Modern webinar room template & sidebar with unlimited tabs and flexible tab content
5. Modern webinar room template with default CTA Button which opens an external page!
6. Modern webinar room with unlimited Call-to-Actions in a video overlay, inside the webinar!
7. Webinar default Page
8. 2 CTAs with a poll - add plugin output here
9. 2 CTAs with a poll - mobile view - add any shortcode here
10. Add external payment gateways - mobile view - like Freemius, WooCommerce
11. Sent recorded and live webinar reminders beautifully
12. Compare Level and Typ to see what fits best for you

== Frequently Asked Questions ==
= Why choose webinar WordPress plugins compared to an external webinar service? =

Plattform webinar features comparison:

* Unlimited webinars* : Limited webinars
* Unlimited attendees* : Limited attendees
* Unlimited duration* : Limited duration
* Pay one price : Pay for what you may use
* Any language : Only the language offered
(We help you with free DeepL translation)
* All in one place : Webinar hosted by a proprietary platform
(Make a backup and take your whole online business copy with you)
* Do your own design : Only the design offered
(Add webinar functionality inside your design)
* Do it your way : Only the functionality offered
(Extend webinar pages with any of the [59.812 free and open-source plugins](https://wordpress.org/plugins/) Please hold STRG/CMD and click to open in a new tab)

With webinar WordPress plugins you are part of the unlimited and beautiful WordPress ecosystem which powers more than [41% of all websites](https://www.tooltester.com/en/blog/wordpress-market-share/) (Please hold STRG/CMD and click to open in a new tab).
**What to choose:** A proprietary webinar platform or an easy and flexible platform? I would feel adventurous, open and choose one of the webinar WordPress plugins.
I am happy to choose WordPress in 2014 and built two growing businesses on it. A herbal shop and a webinar WordPress plugin business.

*Only the server which getting cheaper and cheaper is the limit. We working on a solution to take also this load from you.

= How is webinar marketing automation working with WebinarIgnition? =
The to show you how webinar [marketing automation](https://en.wikipedia.org/wiki/Marketing_automation) is working by telling the possible customer story: User brought by ads sign up through webinar registration, will be reminded before and on the webinar date. During registration, the user is also added to your e-mail marketing provider, with all the details from the registration form. In webinars, you can show the webinar attendee unlimited actions, like polls, sales offers or appointments they can book. You can nearly use any of the free [https://wordpress.org/plugins/](https://wordpress.org/plugins/). If you have more than this in mind for your webinar marketing automation then [tell us your needs here in the chat](https://support.webinarignition.com)

= I do not see the Account page, what should i do? =

On the WebinarIgnition dashboard page, click Opt-in to see the account button in top right corner, on next page click Allow & Continue button. After this, your account page will be available under WebinarIgnition menu.

= I bought freemius license, but license still not upgraded. How to get my plugin upgraded? =

* Please check your mail box. After you bought a license, you need to activate the new version via email link.
* In some cases if the previous step doesn't activate the license you've purchased, visit Account page under Webinarignition menu. You will find there all your license info. Click Sync link, to update info and fix the license issue.
* Also you can do it by clicking Activate license link under the WebinarIgnition title on Plugins page and add your license sent via email.
* Change the email address to match with the buyer email address and see the bought license
Go to account and in the line of the email please click "edit" and follow the global message for details see this video:

https://www.youtube.com/watch?v=AERE0fUbnTU

= How do users join to my webinar or video conference? =
Via custom link to your video conference or webinar. You can also share your link via email and social media platforms.

= Do I have to create a separate page for the video meeting or can I implement it on any page? =
The choice is yours, you can add it with shortcodes to an already existing page or post, or you can create a completely new page/post.

= Is the plugin compatible with any WordPress theme? =
Yes, the plugin is compatible with any WordPress theme. Install it, activate it and enjoy the perfect performance.

= Am I able to manage my webinar or video conference users? =
Yes, you have a Live Console dashboard inside your WordPress site. Complete control and management of your users and attendees.

= How quickly can I get to start hosting the video conference? =
Just activate the plugin on your website and get started within minutes.

= Does the plugin support Gutenberg and Elemento, Divi builders? =
Yes, you can create meetings, webinars, seminars, streams and video conference calls inside any editor or theme builder.

= Does this plugin uses shortcodes? =
Yes, with the shortcodes you can easily add webinar or video calls on any page, post or sidebar. [Read more in our KB](https://webinarignition.tawk.help/article/create-your-own-designed-webinar-landing-pages_webinar-registration-pages)

= I want a refund, what is the procedure? =
You are fully protected by our 100% Satisfaction Guarantee. If over the next 30 days you are unhappy with our plugin or have an issue that we are unable to resolve, we'll happily consider offering a 100% refund of your money.
[Read more in our KB](https://webinarignition.tawk.help/article/cancel-plan-subscription-or-trial)

= Who do I contact for any kind of support? =
For instant support, please send us a message inside the website chat, or you can also contact us through Support. You can also visit our Knowledge Database to find answers to your question. [Read more in our KB](https://webinarignition.tawk.help/)

= Can I add my logo to the video conference or stream? =
Yes, you can use your own branding elements inside your webinar, stream or video conference, also known as white labeling.

= Where will my video conference recording be stored? =
The recording option is only available inside third-party apps like Zoom, Jitsi, Google Meet and others.

= Can I do live streaming? =
Yes, you can do live streaming with our plugin, even with YouTube, Zoom, Google Meet, Jitsi and others. [Read more in our KB](https://webinarignition.tawk.help/article/what-kind-of-video-sources-can-i-use-for-live-streaming)

= Do I need a Zoom account?
Yes, you should be registered in Zoom. Also, depending on the zoom account plan you are using. [Read more in our KB](https://webinarignition.tawk.help/article/zoom-integration)

= Does it work with all streaming services? =
Yes, all that provides an embedded or an iframe code (like Youtube Live, Jitsi Meet Integration, Facebook Live) or a shortcode like the ZOOM integration will work. [See the full list and tutorial on how to set up the webinar streaming service.](https://webinarignition.tawk.help/category/streaming-service)

= How to sell live webinars (do paid webinars) in WordPress... =

* Paid live & evergreen webinars: with Stripe, PayPal and since 2.9.1.0+ also with WooCommerce
[See article and video for details ](https://webinarignition.tawk.help/article/creating-paid-webinars)

* With KlickTipp and WebinarIgnition
Connect the payment processor like PayPal or Digistore24 with KlickTipp.
On the payment tag, sent a mail with the one-click registration link:
Use the link in emails. Replace NAME/EMAIL with wildcards e.g. %Subscriber:CustomFieldFirstName% %Subscriber:EmailAddress%
https://your-domain.com/live-webinar/?register-now&n=NAME&e=EMAIL

* With MailChimp ect. and WebinarIgnition
Connect the payment processor like PayPal MailChimp.
On payment confirmation, sent a mail with the one click registration link:
Use the link in emails. Replace NAME/EMAIL with wildcards e.g. MailChimp with *|EMAIL|* *|FNAME|* and the link looks like this with the parameter added.
https://your-domain.com/live-webinar/?register-now&n=*|FNAME|*&e=*|EMAIL|*

**User story, how a user gets automatically registered to a webinar:**
Before the email is sent the parameter in the link are replaced by NAME/EMAIL of the user.
When user click the link in his postbox he will be auto-registered and redirected to the webinar confirmation page.

**One-click registration only working at the moment with live webinars.** If you want to sell also evergreen/automated/recorded Webinar then hide your webinar registration page and give access only to the customers. So you share the link to the registration page instead of using the one click registration link.

**By the way:** One click registration for evergreen is on our very long nice-to-have feature list. If you need it ASAP then we give you a quote and do it quicker.
We can develop a solution to start the webinar 5 minutes after the user clicked the link.
You can do a 6 Minutes countdown in your email and a 5 minutes countdown on the registration confirmation page.

= Has WebinarIgnition a full integration with KlickTipp email marketing service? =
Yes, it has. That means you know exactly when attended, how long stayed, when exited when rejoined and has seen the CTA1, CTA2, ... (recorded webinars only).
With this data and the KlickTipp campaign which comes with the connection, you can automate and fine-tune your webinar user experience in every detail.

Some cases:

* You can get them back when they drop out for any reason.
* If too long outside the webinar and CTA not seen offer a new webinar date.
* Sell if they have not bought but seen the whole webinar.
* Upsell if they have seen the whole webinar and bought.

**WP2LEADS is used to connect KlickTipp and WebinarIgnition**
Complex KlickTipp Webinar Campagne included start with the connection with this video:

https://www.youtube.com/watch?v=lsN8Zv2j2xs

[read more here.](https://webinarignition.tawk.help/de/article/klick-tipp-webinar-webinarignition-anbindung-an-klick-tipp)

**KlickTipp about:** It is a german started email marketing provider, who really use the full advantages of tag (digital post-its on an email address), to laser-sharp automate email marketing.

**Did you searched for the [KlickTipp webinar 2022?](https://www.klick-tipp.com/webinar/15194)** <- Please follow my Affiliate link to support WP2LEADS development.

= I know you can use this plug-in to charge for webinars. Is it possible to grant members the ability to view the webinar without paying? =
For example, we have a group of members that are in a paid coaching program. We would like to give free webinars access to those coaching member, but charge others that are not in the program.
Yes you can, [read more, about what is possible when creating paid webinars, here.](https://webinarignition.tawk.help/article/creating-paid-webinars#:~:text=the%20confirmation%20page.-,What%20is%20possible%3F,-Question%3A%20I%20know)

= What is a webinar? =
A video in a secure room with call-to-actions (CTAs). Since users have to register for a webinar, you can use it as a lead magnet.
There are live webinars and evergreen (also called automated) webinars. Live webinars can be a ZOOM meeting or Youtube live within a secured room.
Evergreen is often a recorded live webinar with the same offers and CTAs.
Both types of webinars are held at a specific time and on a specific date, which makes them more limited and more valuable.
Once started, a webinar cannot be paused, skipped or jumped forward in time, so you can build a nice strategy to sell and argue your topic.
Compared to a YouTube video, which can be watched by anyone without the content creator getting an email address. It can also be skipped, jumped forward in time and break the tension.

My personal opinion of Webinar ZOOM: = =
As I cannot test it for free, I cannot find a starting point with ZOOM Webinar. So I stay with ZOOM Meeting and integrate it with WebinarIgnition and get my flexible Webinar ZOOM solution. I also find the pricing structure not clear enough. So I stay with the unlimited WebinarIgnition, which has only one price for all features, participants, webinar time and participants at the same time, ...

= How can I report security bugs? = 

You can report security bugs through the Patchstack Vulnerability Disclosure Program. The Patchstack team help validate, triage and handle any security vulnerabilities. [Report a security vulnerability.](https://patchstack.com/database/vdp/webinar-ignition)

== Changelog ==

= 4.00.0-rc.1 2024.09.24 =

== Changelog ==

= 4.00.0-rc.1 2024.09.24 =

Welcome back, thanks for the patience.
"I am very happy to made this new version and it is just the beginning." Tobias CEO

Notice: If premium license is activated in WordPress backend under WebinarIgnition menu, 
go to your account and click download premium plugin. No other changes for you.
You can always [download the premium version here:](https://webinarignition.com/members/#!/login)

If you are not on premium version, you use the WordPress.org version and upgrade to new version you will lose enabled webhooks, admin notifications on new users, paid webinars, CTA alignment in overlay to see speaker, send users via HTML subscription code, view and export leads.
The 2013-2024 licensing system has been removed, and nice offers have been made to switch.
Background: To follow wp.org rules, free and pro code must be separated. 
On WordPress.org only free code allowed and Free must always be free without restrictions (like counting free registrations) and contain only free code (We had both in Free).

= What you get with 4.00.0 =

* 3.5 months of reworked plugin by 2 developers according to WordPress coding standards
* You can opt-in separately and the plugin stays the same.
* More compatible with other plugins and theme (Own CSS classes)
* More secure, as all possible data is secured (escaped)
* Modern, mobile responsive backend (Thanks Mahafuz)
* Huge amount of fixes during intensive testing (Thanks Arman)
* Unlimited support (Thanks Ehtisham)
Free "New to WebinarIgnition 1:1 Meeting" [here:](https://webinarignition.com/meet-us/)
Free chat support [here:](https://webinarignition.com/#contact-us)
* Unlimited updates
* Unlimited webinars
* Unlimited registrations
* Unlimited attendees
* Unlimited webinar time
* Unlimited custom webinar funnels
* Unlimited feature requests sent to us that will be reviewed and considered developed

= 3.08.2 2024.06.16 =

= If premium licence is activated, in account user see download premium plugin, please do so.

= 3.08.1 2024.05.14 =

= Yes, usability improved =

* Added: Links to Webinar Room and Console on Webinar page (top right) and Console (left)
* Fixed: Delete webinars now working again 
* Improved: Evergreen webinar MP4 webinar room preview (admin only), 
Jump in time now possible when showing an iframe or full width CTA.
I was getting trapped during preview CTAs, so I improved it.
* Improved: Live CTA on mobile/tablet where not shown
* Improved: Live on OFF broadcast message does not hide the CTA
* Improved: Live only hitting "Save" will update the CTA and send to attendees
I was preparing a CTA and it was already live before I hit save.
* Improved: Evergreen if no sidebar / CTA shown, should only show video.
* Improved: Live/Evergreen CTA width like 40% also on mobile and tablet
Previously was 90% and speaker could not be seen
* Improved: Hide admin bar, on evergreen delayed autofill thank you page as set in global settings 

= In progress =

* Homepage completely in Gutenberg, fast, beautiful and with our unique USPs.
* More Quickstart videos, linked in mails, thx Tobias (me) and Fahad
* New webinar about webinars in English and German thx Ronny
* V5 CTAs, which are now global, getting ready
Already done Gutenberg (GB) templates in different languages, also working on non GB sites.
Plan to release first version mid-June 2024.
This version will work with your current v3 license.

= 3.08.0 2024.05.03 =

= Autofill logged in user - read only =

* Added: When you use the registration shortcode inside a members area name and email get filled and email is read-only
Why? Only the member that paid should be able to register for webinar.
How to make read-only? Replace the current shortcode with the new and change readonly="false" to "true"
Thanks Victor N. for the idea

= Improvements =

Welcome Fahad - who do QA 

* Evergreen: Not loaded more then 7 days in future
* Not loading time on some browsers and improve (issue with merge/minified Javascript)
* Verification working after entered wrong code, when entered working code.
* Branding free 100 registration got deactivated on save global settings
* Updated Freemius, for better license and free usage
* If not confirmed e-mail address after confirm to opt-in, it is now showing as overlay over the free amount table
To be sure you get all the free 100 registrations per months.
* Autofill on live and custom SC pages not login
* Dismiss "old" webinars message
* If two default pages selected autofill ect. not working
* Added: Padding in overlay CTA like on sidebar
* Evergreen Replay need ALL actions like webinar room
* Verfification code modal button style
* Essential plan 100 -> 250 registrations

Known issue, soon solved:

* both: When no sidebar is show and in mobile/tablet view, show white space between the video and the footer.
* only live: 40% width only on desktop, mobile/tablet view bigger
* Can not delete webinar

= Best we are working on V5 =

* With 2 double developer speed
* To be released in weeks

= 3.07.0 2024.04.17 =

= All can now register for webinar = 

* Improved: All can now register for webinar.
Non subscriber user roles can now register for webinar after confirm their email address.
Logged in users not forced to confirm. You can enable confirmation per webinar or global.

= Sending emails nicely by default = 

* Added: Email sender name and sender email in "Settings" / "Email Template"
So your webinar reminders are delivered nicely to the inbox. To be sure double the with the built in email Spammyness tester.
Which is overwritten when you have an SMTP Plugin [see all details in screencast here](https://webinarignition.tawk.help/article/smtp-setup)
If you not use a SMTP plugin you see the admin name and email as sender instead of wordpress@domain.

= The easiest way to build a custom webinar room = 

* Added: One shortcode that do video & cta and sidebar in one shortcode [wi_webinar_block id=" " block="webinar_video_cta_sidebar"]
Also it hides the sidebar without CTA content, for full video view.

* Improved: No fatal error on install a second webinarignition plugin
Only the last activated one will stay active.
* Improved: Select custom shortcode registration pages and disable the default page
Before it was a struggle and multiple settings saving needed to disable default webinar registration 
and only use your custom designed registration pages.
* Improved a lot of code and usability

= Seamless integration with autofill on evergreen = 

* Autofill user details from any CRM, autoresponder 
example ULR: https://domain.com/test-autofill/?confirmed&first=Tobias&email=Tobias@domain.com&readonly=true
Replace Tobias with Name placeholder and Tobias@domain.com with email placeholder
You can decide readonly=true so email can not be changed or readonly=false

= Live Webinar CTA = 

* Load anything by enable "load in iframe" option
* Align, set width to show CTA and your live content like your face
* Improved usability: Any changes lead to update the CTA in attendees browser

YEAH i did it: Recorded me in a Live webinar: 

https://www.youtube.com/watch?v=TVrwl6Fhq38

feedback please…

= Evergreen CTAs =

* Updated the KB article by adding a nice iframe code and same site cookie issue solution [see here please](https://webinarignition.tawk.help/article/do-you-also-have-only-one-action-in-webinar)
* Updated the DEMO, it [is worth a try](https://webinarignition.com/demo)

= Essential webinar default pages stay healthy = 

* Sometimes on cleanup anyone clean also default webinar page and nothing works after.
Vlad made it possible to disable delete, un publish ect. this pages, so your webinars stay on and generate leads and $$$
Only when webinar is deleted they will be deleteable.

= Former webinars with issues can work again = 

* Updated the hint for the solution:
If you have webinars created before 25th March 2022, you may encounter a redirect issue when the registration URL shows ?p=123.
To fix this, follow these steps: open the webinar dashboard, click on the top left edit button near the title, which will open the default webinar page for this webinar. Then, publish the page. This will solve all related issues.
After checking and saving all webinars again, please dismiss the message by clicking on 'dismiss_old_webinars_notification'.

= Next =

* Improve verification code in popup
Code gets invalid when popup with registration shortcode 
e.g. from elementor gets closed below the confirmation modal.
* Essential webinar custom pages can not be altered

= Next big steps =

* We focussed a lot of time on Version 3 and the RRatingg plugin (https://wordpress.org/plugins/5-stars-rating-funnel/), now we focus on:
* "Easy Streaming" a plugin that stream directly inside your live webinar and is easy to setup with flexible usage plans.
* Phoenix the Version 5 of WebinarIgnition, which we started but paused to focus on V3 and RR.

= 3.06.1 2024.04.01 =

* Added: New plans that we added months ago require a new price table inside the plugin.

= 3.06.0 2024.03.16 =

= This is the first release with the new Team Arman Developers =

* Fixed: Browser asking for reload unlimited times in live and evergreen
* Added: Glowing mute icon and increased its size (Registration and Thank you page video)
* Added: Verification code 
* Improved: Security thanks to patch stack
* Added: Footer shortcode for the custom pages
* Added: On Plugins page link to dashboard
* Added: Sidebar shortcode to build your complete webinar room in your page builder
Next: Improve loading of shortcodes via iframe
Next: one shortcode that do video & cta and sidebar in one shortcode
* Improved: Live webinar CTA on/off/on not showing CTA again
* Added: Support of Shortcodes in Live webinars, inclusive loading in iframe when advanced iframe plugin is activated.
* Removed:  Error in the console
* Fixed: Video and CTA shortcode separated issue with CTAs
Was not loading the second CTA
* Replaced: Console editor of the CTA content with default WP editor
Fixed evergreen video progress bar on ctas
now above Iframes that fill the full width and now admin can always jump in time

Next: Autofill user data and register user for evergreen webinar

= 3.05.8 2024.02.18 =

* 18 translations now available

= 3.05.7 2024.02.18 =

* Fixed "confirmation code" made optional by default and overlay better with code field on thrive and other page builder, confirmation more reliable

Still working to improve and also translate it.

= 3.05.6 2024.02.13 =

* fixed: "wrong confirmation code" by run the migration script on update plugin
Sorry for not finding it through my testing, added more testing tasks to my release routine.
Thank you Tomasz for finding and coming up again and again with this topic.
Thanks Vlad, my developer since years, for investigating and finding a solution for Tomasz and all user.

= 3.05.5 2024.01.26 =

* Changed: maybe_unserialize -> json_decode
details see: https://bitbucket.org/WP-Leads-Plugins/webinarignition/commits/054eca72f295

= 3.05.4 2024.01.23 =

* Improved: Security, thanks to patch stack and WordPress.org team
* Added: Email verification on register, to secure only verified user gets registered on the WP site.
* Changed: "unserialize" functions to "maybe_unserialize"
* Added: Show hint "already registered" and block registration to non subscriber users role.

See: This release contains fixes after commit 8a434e8 [The code changes here.](https://bitbucket.org/WP-Leads-Plugins/webinarignition/commits/)
See screencast with the email confirmation and details on the fix: [Read more here.](https://www.awesomescreenshot.com/video/24303712?key=9547fef5e9d36a6f0da4f708bee92670)

= 3.05.1 2024.01.05 =

* Security improved thanks to patch stack and WordPress.org team
* Freemius SDK update. Now auto cleaning DB entries 

--
Soon, delayed because security fixes first: 

* Fixed: Live auto login 
Working now with base64 encrypted name and email address and with plain.

= Evergreen auto fill URL in dashboard =

Will be improved, together with the live auto fill and login
We think of adding a security token so misuse of the link is not possible.
Case: Intruder see WebinarIgnition registration and add "?register-now&n=NAME&e=EMAIL&readonly=true&login=true" to the link and fill name and email of other persons which then gets registered.
* Current development: Make working with custom pages created by adding shortcodes.

= mail confirmation on registration =

* Current beta and ready for testing: 
At the moment only working with the autofill URL feature
Only the the token registered user will not need to confirm.
Why no need to confirm? Because the click on a link inside an email to their account.
Why adding email confirmation? Often seen and helping to get valid and proofed owned email addresses

FAQ: 
Question: Why we will not have an auto login on evergreen?
Answer: Because attendee should be able to choose a date & time to attend the webinar. Live Webinars have a fixed date & time.

* Current parallel development: Total new plugin

--

[View full changelog](https://plugins.trac.wordpress.org/browser/webinar-ignition/trunk/changelog.txt)


== Upgrade Notice ==

* Manual upgrade (delete plugin, download new and overwrite with new plugin file) will work smooth when update from 1.x or 1.9.x or 2.x to 2.2.
* All data will be save.
* 1.x Free Version user please upgrade and optin to freemius get new stable code and some free evergreen webinar registrations with the new multiple Call-to-Actions to boost sales and the Shortcodes to design and layout your own webinars.
* 1.9.x or 2.x paid users upgrade to get new more stable code. Your license will continue working.