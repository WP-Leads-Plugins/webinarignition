<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>
<div class="ultimate-container">
	<div class="ultimate-below-sec">
		<div class="ultimate-graphic">
			<img src="<?php echo esc_url( WEBINARIGNITION_URL . 'images/logo-avatar.png' ); ?>" alt="Unlock potential">
			<div class="ultimate-below-container">
				<div class="ultimate-lock-text-cont"><img src="<?php echo esc_url( WEBINARIGNITION_URL . 'images/padlock.png' ); ?>" /></div>
				<div class="ultimate-text-container">Ultimate</div>
			</div>
		</div>
		<div class="ultimate-content">
			<h2>Upgrade to Ultimate Version and Unleash Your Potential</h2>
			<p><a href="https://webinarignition.com/even-after-11-years-you-can-still-get/?ppt=13485e8b3c7f81c1b8e06fd7a163a1e8" target="_blank">To continue receiving updates and support, you must upgrade to Ultimate Essential for free.</a></p>
			<a class="download-legacy-version" href="https://www.dropbox.com/scl/fi/yrz21n27upliq093v494k/webinar-ignition.zip?rlkey=qp51ir9deke441hak99ydd4bv&dl=1">Download your latest version with the old license check and no features cut out here!</a>
			<a href="<?php echo esc_url( admin_url( 'admin.php?page=webinarignition-dashboard-pricing' ) ); ?>" class="ultimate-button">Go Ultimate</a>
		</div>
	</div>
</div>