var webinar = window.WEBINARIGNITION.webinar;
var $lead_id      = window.WEBINARIGNITION.lead_id;
var lead_info = window.WEBINARIGNITION.lead_info;
var ajaxurl = window.WEBINARIGNITION.ajax_url;
var webinar_page = window.WEBINARIGNITION.webinar_page;
var nonce = window.WEBINARIGNITION.nonce;
var $webinar_type = 'AUTO' === webinar.webinar_date ? 'evergreen' : 'live';
var ip = window.WEBINARIGNITION.ip;
var lid = window.WEBINARIGNITION.lid;
var globalOffset = 0;

	jQuery.expr.pseudos.parents = function (a, i, m) {
		return jQuery(a).parents(m[3]).length < 1;
	};

	(function ($) {
		if ( $.trim(lead_info) !== '' && webinar.limit_lead_visit !== '' && webinar.limit_lead_visit === 'enabled' ) {
			
			$(window).on('beforeunload', function(e) {
				e.preventDefault();

				webinarignition_mark_lead_status('complete');

				return null;
			});
		}

		if ( webinar_page === 'webinar' || 'webinar' === webinar_page ) {
			// TRACK +1 VIEW
			if (typeof $.cookie === "function") {
				$getTrackingCookie = $.cookie('we-trk-live-' + webinar.id );

				// if not tracked yet
				if ($getTrackingCookie != "tracked") {
					// No Cookie Set - Track View
					$.cookie('we-trk-live-' + webinar.id, "tracked", {expires: 30});
					var data = {
						action: 'webinarignition_track_view',
						security:nonce, 
						id: webinar.id,
						page: "live"
					};
					$.post(ajaxurl, data, function (results) {
					});
				}
			}
			// Track +1 Total
			var data = {
				action: 'webinarignition_track_view_total',
				security:nonce, 
				id: webinar.id,
				page: "live"
			};
			$.post(ajaxurl, data, function (results) {
			});
		}

		// VIDEO FIXES:
		$(".ctaArea").find("embed, object").height(518).width(920);

		var $lead_id               = window.WEBINARIGNITION.lead_id; 
		var $is_auto_login_enabled = window.WEBINARIGNITION.is_auto_login_enabled;
		var $is_user_registered = window.WEBINARIGNITION.is_user_registered;
		var $is_webinar_page = window.WEBINARIGNITION.is_webinar_page;
		var $is_alt_webinar_page = window.WEBINARIGNITION.is_alt_webinar_page;
		if ( ( $is_user_registered || ! $is_auto_login_enabled ) && ( $is_webinar_page || $is_alt_webinar_page ) )  {
			if (typeof $.cookie === "function") {
				// Track Event Attendance
				$checkCookie = $.cookie('we-trk-' + webinar.id );
				// Post & Track
				$.post(ajaxurl, {
					action: 'webinarignition_update_view_status',
					security: nonce, 
					id: webinar.id, 
					lead_id: $lead_id 
				});
			}

			if ( lead_info !== '' && 'enabled' === webinar.limit_lead_visit ) { 
				webinarignition_mark_lead_status('attending');
			}

			if ( 'hide' !== webinar.webinar_qa ) {
				// Get Name / Email
				if ( 'AUTO' === webinar.webinar_date ) {
					var data = {
						action: 'webinarignition_get_qa_name_email2',
						security:nonce, 
						cookie: $lead_id, 
						ip: lid 
					};
				} else {
					var data = {
						action: 'webinarignition_get_qa_name_email',
						security:nonce, 
						cookie: lid, 
						ip: ip 
					};
				}

				let optNameVal = '';
				if( $('#optName').length > 0 ) {
					optNameVal = $('#optName').val();
				}

				let optEmailVal = '';
				if( $('#optEmail').length > 0 ) {
					optEmailVal = $('#optEmail').val();
				}

				let leadIDVal = '';
				if( $('#leadID').length > 0 ) {
					leadIDVal = $('#leadID').val();
				}

				if ('' === optNameVal || '' === optEmailVal || '' === leadIDVal) {
					$.post(ajaxurl, data, function (results) {
						if (results) {
							$qaInfo = results.split("//");

							if ('' === optNameVal.trim()) {
								$("#optName").val($qaInfo[0] !== 'undefined' ? $qaInfo[0] : "");
							}

							if ('' === optEmailVal.trim()) {
								$("#optEmail").val($qaInfo[1]);
							}

							if ('' === leadIDVal.trim()) {
								$("#leadID").val($qaInfo[2]);
							}
							
							// $("#optName").attr("disabled","disabled");
							// $("#optEmail").attr("disabled","disabled");
						}
					});
				}
			}
		}
		// Polling For Live Broadcast Message

		function setBackgroundTransparency(element, transparency) {
			// Ensure transparency is between 0 and 100
			transparency = Math.max(0, Math.min(100, transparency));
			
			// Convert transparency to a scale from 0 (fully opaque) to 1 (fully transparent)
			let alpha = 1 - (transparency / 100);
			
			// Get the current background color of the element
			let currentBgColor = $(element).css("background-color");
		
			// If the current background color is not rgba, convert it
			if (!currentBgColor.includes("rgba")) {
				let rgb = currentBgColor.match(/\d+/g); // Extract RGB values
				if (rgb && rgb.length >= 3) {
					currentBgColor = `rgba(${rgb[0]}, ${rgb[1]}, ${rgb[2]}, ${alpha})`;
				} else {
					// Fallback to black if background color is undefined or invalid
					currentBgColor = `rgba(0, 0, 0, ${alpha})`;
				}
			} else {
				// Update the alpha value if it's already rgba
				currentBgColor = currentBgColor.replace(/[\d\.]+\)$/g, alpha + ")");
			}
		
			// Apply the new background color with transparency
			$(element).css("background-color", currentBgColor);
		}
		
		if ( webinar.hasOwnProperty( 'webinar_date') && webinar.hasOwnProperty( 'live_stats') ) { 
			if ( 'AUTO' !== webinar.webinar_date && 'disabled' !== webinar.live_stats ) {
			sessionStorage.removeItem('hash');
			// $.ajax(ajaxurl, {
			// 	method: 'get',
			// 	data: {
			// 		action: 'webinarignition_broadcast_msg_poll_callback',
			// 		security: nonce,
			// 		id: webinar.id,
			// 		lead_id: $lead_id, 
			// 		ip: ip
			// 	},
			// 	interval: 5000,
			// 	type: 'text'
			// }, function (jsonString) {

			// 	var jsonobj     = JSON.parse(jsonString);
			// 	var orderBTN    = $("#orderBTN");
			// 	var hash        =  sessionStorage.getItem('hash');

			// 	if (jsonobj.air_toggle !== "OFF" && ( !hash || hash != jsonobj.hash  )) {

			// 		$('.timedUnderArea.test-7').css('width', jsonobj.box_width);

			// 		console.log("element to style is :");

			// 		console.log($('.timedUnderArea.test-7'));

			// 		var boxAlignment = jsonobj.box_alignment;
			// 		var $timedUnderArea = $('.timedUnderArea.test-7');

			// 		if (boxAlignment !== null && boxAlignment.trim() === "flex-end") {
			// 			$timedUnderArea.css({
			// 				'right': '10px',
			// 				'left': 'auto'
			// 			});
			// 		} else if (boxAlignment !== null && boxAlignment.trim() === "flex-start") {
			// 			$timedUnderArea.css({
			// 				'left': '10px',
			// 				'margin': '0'
			// 			});
			// 		} else if((boxAlignment !== null && boxAlignment.trim() === "center")) {
			// 			$timedUnderArea.css({
			// 				'margin': 'auto',
			// 				// 'right': '0'
			// 			});
			// 		}
			// 		else{
			// 			$timedUnderArea.css({
			// 				'margin': 'auto'
			// 			});
			// 		}
		
			// 		$('.webinarVideoCTA').addClass('webinarVideoCTAActive');
			// 		orderBTN.show();

			// 		$("#orderBTNCopy").html(jsonobj.response); 
					
			// 		if( jsonobj.hash ){
			// 			sessionStorage.setItem('hash', jsonobj.hash );
			// 		}

			// 	}
			// 	else{
			// 		sessionStorage.setItem('hash', jsonobj.hash );
			// 	}

			// 	if (jsonobj.air_toggle == "OFF") {
			// 		$('.webinarVideoCTA').removeClass('webinarVideoCTAActive');
			// 		orderBTN.hide();
			// 	}

			// });
				function pollWebinarStatus(options) {
					// Merge custom options with defaults
					var settings = $.extend({
						method: 'GET',
						dataType: 'json',
						interval: 1000, // Default interval of 1 second
						data: {},
						url: ajaxurl
					}, options);
				
					function poll() {
						$.ajax({
							url: settings.url,
							method: settings.method,
							dataType: settings.dataType,
							data: settings.data,
							success: function(jsonString) {
								// Parse the response if necessary (assuming JSON is not already parsed)
								var jsonobj = typeof jsonString === "string" ? JSON.parse(jsonString) : jsonString;
								var orderBTN = $("#orderBTN");
								var hash = sessionStorage.getItem('hash');
				
								if (jsonobj.air_toggle !== "OFF" && (!hash || hash != jsonobj.hash)) {
									console.log("air broadcast mesage");


									var liveCtaPosition = jsonobj.cta_position;
									console.log("on response: ", liveCtaPosition)
									var btnColorA = getTextColorFromBgColor(jsonobj.button_color);
									if(liveCtaPosition === 'overlay'){
										var boxAlignment = jsonobj.box_alignment;
										var $timedUnderArea = $('.timedUnderArea.test-7');
										$timedUnderArea.css("flex-direction", "column");
					
										if (boxAlignment && boxAlignment.trim() === "flex-end") {
											$timedUnderArea.css({
												'right': '10px',
												'left': 'auto'
											});
										} else if (boxAlignment && boxAlignment.trim() === "flex-start") {
											$timedUnderArea.css({
												'left': '10px',
												'margin': '0'
											});
										} else if (boxAlignment && boxAlignment.trim() === "center") {
											$timedUnderArea.css({
												'margin': 'auto',
												'left': '0', 
												'right': '0'
											});
										} else {
											$timedUnderArea.css({
												'margin': 'auto',
												'left': '0', 
												'right': '0'
											});
										}

										if(jsonobj.box_width === '100%'){
											$('.timedUnderArea.test-7').css('width', '97%');
										}else{
											$('.timedUnderArea.test-7').css('width', jsonobj.box_width);
										}
										setBackgroundTransparency(".timedUnderArea.test-7", jsonobj.bg_transparency);
										$('.webinarVideoCTA').addClass('webinarVideoCTAActive');
										orderBTN.show();
					
										$("#orderBTNCopy").html(jsonobj.response);
									
										var buttonHTML = '<a href="' + jsonobj.button_url + '" target="_blank" class="large radius button success addedArrow replayOrder wiButton wiButton-lg wiButton-block wi-live-btn" style="background-color:' + jsonobj.button_color + ';color:'+btnColorA+';border: 1px solid rgba(0,0,0,0.20);">' + jsonobj.button_text + '</a>';

										// Append the dynamically created anchor tag to the div with id orderBTNArea
										$('#orderBTNArea').remove();
										$('#orderBTN').append('<div id="orderBTNArea"></div>'); // Recreate the container
										if(jsonobj.button_url && jsonobj.button_text){
											$('#orderBTNArea').html(buttonHTML);
										}
										
										$(".webinarVideoCTA ").css("display", "block");
										console.log("Iframes : ",$("#orderBTNCopy iframe html"));
										$("#orderBTNCopy iframe").on("load", function () {
											var iframeDocument = this.contentDocument || this.contentWindow.document;
										
											if (iframeDocument) {
												var htmlElement = iframeDocument.documentElement;
										
												// Add a `style` attribute with `!important`
												htmlElement.style.setProperty("margin-top", "0px", "important");
											}
										});
										if ($("#webinarTabs li").length > 0) {
											// Your code goes here
											console.log("There are more than 0 <li> elements");
											$("#tab-new-tab-content").remove();
											// Remove the tab from the tabs list
											$("#tab-cta-sidebar-tab").closest("li").remove();

											// Remove the content div
											$("#new-live-content").remove();
											console.log("liveCtaPosition is overlay'. Cleaning up appended elements.");
											// Optionally reset other tabs or content to active
											$("#webinarTabs .wi-nav-link")
												.removeClass("active") 
												.attr("aria-selected", "false"); 
											$("#webinarTabs .wi-nav-link:first").addClass("active").attr("aria-selected", "true");
											$(".wi-tab-content .wi-tab-pane")
    											.removeClass("active");
											$(".wi-tab-content .wi-tab-pane:first").addClass("active");
										}else{
											console.log("There are 0 <li> elements");
											$("#webinarSidebar").remove();
											$("#webinarVideo")[0].classList.remove('wi-col-lg-9');
										}
									}else if(liveCtaPosition === 'outer'){
										var tabText = jsonobj.tab_text;
										$('#tab-cta-sidebar-tab').css('display','block');
										$('#tab-cta-sidebar-tab').text(tabText);
										sessionStorage.setItem('liveCtaPosition', liveCtaPosition);
										sessionStorage.setItem('tabText', tabText);
										sessionStorage.setItem('jsonResponse', jsonobj.response);
										sessionStorage.setItem('buttonText', jsonobj.button_text);
										sessionStorage.setItem('buttonUrl', jsonobj.button_url);
										sessionStorage.setItem('buttonColor', jsonobj.button_color);
										sessionStorage.setItem('btnColorA', btnColorA);
										sessionStorage.setItem('airToggle', jsonobj.air_toggle);

										$(".webinarVideoCTA ").css("display", "none");
										if ($("#webinarSidebar").length) {
											if ($("#webinarTabs").length) {
												var webinarVideoElement = document.getElementById("webinarVideo");
												webinarVideoElement.classList.add("wi-col-lg-9");
												var sidebar = document.getElementById("webinarSidebar");
												sidebar.classList.add("wi-col-lg-3");
												// Check if the tab already exists
												if (!$("#tab-cta-sidebar-tab").length) {
													// Create the new <li> element
													var newTab = `
														<li class="wi-nav-item nav-item">
															<a class="wi-nav-link nav-link active" id="tab-cta-sidebar-tab" data-toggle="tab" href="#tab-new-tab-content" role="tab" aria-controls="tab-new-tab-content" aria-selected="false">
																${tabText}
															</a>
														</li>
													`;

													// Prepend the new <li> to the <ul>
													$("#webinarTabs").prepend(newTab);

													// Remove 'active' class from other tabs
													$("#webinarTabs .wi-nav-link")
														.not("#tab-cta-sidebar-tab")
														.removeClass("active")
														.attr("aria-selected", "false");

													$("#tab-cta-sidebar-tab").attr("aria-selected", "true");

													// Remove 'active' class from other content divs
													$(".wi-tab-content .wi-tab-pane").removeClass("active");

													// Append the new content with the 'active' class

													var newDiv = `
															<div class="wi-tab-pane active" id="tab-new-tab-content" aria-labelledby="tab-cta-sidebar-tab" role="tabpanel">
																
																	<div class="timedUnderArea test-7" id="orderBTNSidebar">
																		<div class="orderBTNCopy">
																			${jsonobj.response}
																		</div>
																	</div>
															
															</div>
														`;

													$(".wi-tab-content").append(newDiv);
													// Append the dynamically created anchor tag to the div with id orderBTNArea
													$('#orderBTNAreaPanel').remove();
													$('#orderBTNSidebar').append('<div id="orderBTNAreaPanel"></div>'); // Recreate the container
													var buttonHTML = '<a href="' + jsonobj.button_url + '" target="_blank" class="large radius button success addedArrow replayOrder wiButton wiButton-lg wiButton-block wi-live-btn" style="background-color:' + jsonobj.button_color + ';color:'+btnColorA+';border: 1px solid rgba(0,0,0,0.20);">' + jsonobj.button_text + '</a>';
													console.log("button Url in sidebar:", jsonobj.button_url);
													if(jsonobj.button_url && jsonobj.button_text){
														$('#orderBTNAreaPanel').html(buttonHTML);
													}

													const $tabContent = $('#tab-new-tab-content');

													// Check if the element has the "active" class
													if ($tabContent.hasClass('active')) {
													// Do nothing if the "active" class is already present
													console.log('The active class is already present.');
													} else {
													// Add the "active" class if it's not present
													$tabContent.addClass('active');
													console.log('The active class has been added.');
													}
													
												} else {
													// Tab already exists, update content if it's different
													const existingResponse = $("#tab-new-tab-content .orderBTNCopy").html();
													if (existingResponse !== jsonobj.response) {
														console.log("Updating content with a new response.");
														$("#tab-new-tab-content .orderBTNCopy").html(jsonobj.response);
														$('#orderBTNAreaPanel').remove();
														$('#orderBTNSidebar').append('<div id="orderBTNAreaPanel"></div>'); // Recreate the container
														var buttonHTML = '<a href="' + jsonobj.button_url + '" target="_blank" class="large radius button success addedArrow replayOrder wiButton wiButton-lg wiButton-block wi-live-btn" style="background-color:' + jsonobj.button_color + ';color:'+btnColorA+';border: 1px solid rgba(0,0,0,0.20);">' + jsonobj.button_text + '</a>';
														console.log("button Url in sidebar:", jsonobj.button_url);
														if(jsonobj.button_url && jsonobj.button_text){
															$('#orderBTNAreaPanel').html(buttonHTML);
														}
														$("#webinarSidebar").addClass("wi-col-lg-3");
													} else {
														console.log("Response content is identical, skipping update.");
													}
												}
											}else{
												console.log("have sidebar but no tabs ");
												var tabsHtml = `
													<ul class="wi-nav wi-nav-tabs wi-bg-light wi-px-1 wi-pt-1" id="webinarTabs" role="tablist" kt-hidden-height="48" style="">
														<li class="wi-nav-item nav-item">
															<a class="wi-nav-link nav-link active" id="tab-cta-sidebar-tab" data-toggle="tab" href="#tab-new-tab-content" role="tab" aria-controls="tab-new-tab-content" aria-selected="true">
																${tabText}
															</a>
														</li>
													</ul>
												`;
												
												var tabsContentHtml = `
													<div class="wi-tab-pane active" id="tab-new-tab-content" aria-labelledby="tab-cta-sidebar-tab" role="tabpanel">
														<div class="timedUnderArea test-7" id="orderBTN" style="margin: auto; width: 100%;">
															<div class="orderBTNCopy" >
																${jsonobj.response}
															</div>`;
															if(jsonobj.button_url && jsonobj.button_text){
																tabsContentHtml +=`<div id="orderBTNArea">
																			<a href="${jsonobj.button_url}" target="_blank" class="large radius button success addedArrow replayOrder wiButton wiButton-lg wiButton-block wi-live-btn" style="background-color:${jsonobj.button_color};color:${btnColorA};border: 1px solid rgba(0,0,0,0.20);">${jsonobj.button_text}</a>
																		</div>`;
															}
															
															tabsContentHtml +=`</div>
													</div>
												`;

												$('#webinarSidebar').addClass("wi-col-lg-3");
												var webinarVideoElement = $("#webinarVideo");
												webinarVideoElement.addClass("wi-col-lg-9");
												$('#webinarSidebar .wi-col-12').prepend(tabsHtml);
												$('#webinarTabsContent .webinarTabsContent-inner .wi-tab-content').append(tabsContentHtml);
											}
										}else{		
											console.log("No Sidebar FOuind - adding new sidebar")	
											console.log(tabText);						
											var webinarSidebar = `
												<aside id="webinarSidebar" class="wi-col-12 wi-col-lg-3">
													<div class="wi-row wi-g-0">
														<div class="wi-col-12">
															<ul class="wi-nav wi-nav-tabs wi-bg-light wi-px-1 wi-pt-1" id="webinarTabs" role="tablist" kt-hidden-height="48" style="">
																<li class="wi-nav-item nav-item">
																	<a class="wi-nav-link nav-link active" id="tab-cta-sidebar-tab" data-toggle="tab" href="#tab-new-tab-content" role="tab" aria-controls="tab-new-tab-content" aria-selected="true">
																		${tabText}
																	</a>
																</li>
															</ul>
															
															<div id="webinarTabsContent" class="wi-p-3" style="height: 80vh; overflow:auto;">
																<div class="webinarTabsContent-inner default-template-sidebar webinarTabsContent-inner-absolute"  style="height: inherit;">
																	<div class="wi-tab-content"  style="height: inherit;" >
																		<div class="wi-tab-pane active" id="tab-new-tab-content" aria-labelledby="tab-cta-sidebar-tab" role="tabpanel">
																			<div class="timedUnderArea test-7" id="orderBTN" style="margin: auto; width: 100%;">
																				<div class="orderBTNCopy" >
																					${jsonobj.response}
																				</div>`;
																				if(jsonobj.button_url && jsonobj.button_text){
																					webinarSidebar +=`<div id="orderBTNArea">
																								<a href="${jsonobj.button_url}" target="_blank" class="large radius button success addedArrow replayOrder wiButton wiButton-lg wiButton-block wi-live-btn" style="background-color:${jsonobj.button_color};color:${btnColorA};border: 1px solid rgba(0,0,0,0.20);">${jsonobj.button_text}</a>
																							</div>`;
																				}
																				
																				webinarSidebar +=`</div>
																		</div>
																	</div>
																</div>
															</div>
															
														</div>
													</div>
												</aside>
											`;
											var webinarVideoElement = document.getElementById("webinarVideo");
											webinarVideoElement.classList.add("wi-col-lg-9");
											webinarVideoElement.insertAdjacentHTML('afterend', webinarSidebar);
										}
									}
									if (jsonobj.hash) {
										sessionStorage.setItem('hash', jsonobj.hash);
									}

									const iframes = document.querySelectorAll(".orderBTNCopy iframe");
									iframes.forEach((iframe) => {
										iframe.onload = function () {
											try {
												// Check if iframe content can be accessed (same-origin)
												const contentWindow = iframe.contentWindow;
												const document = contentWindow.document;

												// Remove margin-top from the html tag inside the iframe with !important
												const style = document.createElement('style');
												style.innerHTML = 'html { margin-top: 0 !important; }';
												document.head.appendChild(style);

												// Adjust iframe height based on content
												const contentHeight = document.body.scrollHeight;
												iframe.style.height = contentHeight+50 + "px";

												// Enable scrolling on the parent container if content overflows
												// const parentContainer = iframe.closest(".orderBTNCopy");
												// if (parentContainer) {
													// parentContainer.style.overflowY = "auto";
													// parentContainer.style.maxHeight = contentHeight - 100 + "px"; // Limit container height to viewport height
													// parentContainer.style.minHeight = "400px";
												// }
											} catch (error) {
												console.warn("Cannot access iframe content due to cross-origin restrictions.");
											}
										};
									});
				
								} else if(jsonobj.air_toggle === "OFF") {
									sessionStorage.setItem('hash', jsonobj.hash);
									$('.webinarVideoCTA').removeClass('webinarVideoCTAActive');
									$('#tab-cta-sidebar-tab').closest('.wi-nav-item').remove();
									$('#tab-new-tab-content').removeClass('active');
									$('.webinarVideoCTA').css('display', 'none');
									const navItems = $('#webinarTabs .wi-nav-item .wi-nav-link');
									const hasActive = navItems.hasClass('active');
									// If none of them has the 'active' class, add it to the first item
									if (!hasActive) {
										navItems.first().click();
									}

									if( $('#webinarTabs li').length){
										console.log("it contains tabs already ")
									}else{
										
										var webinarVideoElement = document.getElementById("webinarVideo");
										webinarVideoElement.classList.remove("wi-col-lg-9");
										
										// Check if any of the nav-links have the 'active' class
										
									}
									
								}
								// Continue polling after the specified interval
								setTimeout(poll, settings.interval);
							},
							error: function(xhr, status, error) {
								console.error("Error in AJAX request: ", status, error);
				
								// Optionally, handle error and continue polling
								setTimeout(poll, settings.interval);
							}
						});
					}
				
					// Start the polling process
					poll();
				}
			
			
				pollWebinarStatus({
					data: {
						action: 'webinarignition_broadcast_msg_poll_callback',
						security: nonce,
						id: webinar.id,
						lead_id: $lead_id, 
						ip: ip
					}
				});

				// window.addEventListener('load', () => {
				// 	sessionStorage.removeItem('tabText');
				// 	sessionStorage.removeItem('jsonResponse');
				// 	sessionStorage.removeItem('buttonText');
				// 	sessionStorage.removeItem('buttonUrl');
				// 	sessionStorage.removeItem('buttonColor');
				// 	sessionStorage.removeItem('btnColorA');
				// 	sessionStorage.removeItem('liveCtaPosition');
				// 	sessionStorage.removeItem('airToggle');
				// 	console.log('Specific keys cleared from sessionStorage on page load');
				// });


				// document.addEventListener('DOMContentLoaded', () => {
				// 	const savedCtaPosition = sessionStorage.getItem('liveCtaPosition');
				// 	const airToggle = sessionStorage.getItem('airToggle');
				// 	console.log("HONDA;");
				// 	console.log("air toggle: ", airToggle);
				// 	console.log(" savedCtaPosition: ", savedCtaPosition);
				// 	if(airToggle === 'ON'){
				// 		const savedTabText = sessionStorage.getItem('tabText');
				// 		const savedResponse = sessionStorage.getItem('jsonResponse');
				// 		const savedButtonText = sessionStorage.getItem('buttonText');
				// 		const savedButtonUrl = sessionStorage.getItem('buttonUrl');
				// 		const savedButtonColor = sessionStorage.getItem('buttonColor');
				// 		const savedBtnColorA = sessionStorage.getItem('btnColorA');
				// 		const giveAwayTab = document.getElementById('giveaway-tab-webinar-modern');
				// 		const qaTab = document.getElementById('qa-tab-webinar-modern');
				// 		if (savedCtaPosition === 'outer') {
				// 			console.log("reloaded page: its outer:");
				// 			if ($("#webinarSidebar").length) {
				// 				if ($("#webinarTabs").length) {
				// 					// Check if the tab already exists
				// 					if (!$("#tab-cta-sidebar-tab").length) {
				// 						// Create the new <li> element
				// 						var newTab = `
				// 							<li class="wi-nav-item nav-item">
				// 								<a class="wi-nav-link nav-link active" id="tab-cta-sidebar-tab" data-toggle="tab" href="#tab-new-tab-content" role="tab" aria-controls="tab-new-tab-content" aria-selected="false">
				// 									${savedTabText}
				// 								</a>
				// 							</li>
				// 						`;

				// 						// Prepend the new <li> to the <ul>
				// 						$("#webinarTabs").prepend(newTab);

				// 						// Remove 'active' class from other tabs
				// 						$("#webinarTabs .wi-nav-link")
				// 							.not("#tab-cta-sidebar-tab")
				// 							.removeClass("active")
				// 							.attr("aria-selected", "false");

				// 						$("#tab-cta-sidebar-tab").attr("aria-selected", "true");

				// 						// Remove 'active' class from other content divs
				// 						$(".wi-tab-content .wi-tab-pane").removeClass("active");

				// 						// Append the new content with the 'active' class
				// 						if (!$("#tab-new-tab-content").length){
				// 							var newDiv = `
				// 								<div class="wi-tab-pane active" id="tab-new-tab-content" aria-labelledby="tab-cta-sidebar-tab" role="tabpanel">
													
				// 										<div class="timedUnderArea test-7" id="orderBTN">
				// 											<div class="orderBTNCopy">
				// 												${savedResponse}
				// 											</div>`;
				// 											if(savedButtonUrl && savedButtonText){
				// 												newDiv += `<div id="orderBTNArea">
				// 																<a href="${savedButtonUrl}" target="_blank" class="large radius button success addedArrow replayOrder wiButton wiButton-lg wiButton-block wi-live-btn" style="background-color:${savedButtonColor};color:${savedBtnColorA};border: 1px solid rgba(0,0,0,0.20);">${savedButtonText }</a>
				// 															</div>`;
				// 											}
				// 										newDiv += `</div>
												
				// 								</div>
				// 							`;
				// 							$(".wi-tab-content").append(newDiv);
				// 						}
										
				// 						const $tabContent = $('#tab-new-tab-content');

				// 						// Check if the element has the "active" class
				// 						if ($tabContent.hasClass('active')) {
				// 							// Do nothing if the "active" class is already present
				// 							console.log('The active class is already present.');
				// 						} else {
				// 							// Add the "active" class if it's not present
				// 							$tabContent.addClass('active');
				// 							console.log('The active class has been added.');
				// 						}
										
				// 					} else {
				// 						// Tab already exists, update content if it's different
				// 						const existingResponse = $("#tab-new-tab-content .orderBTNCopy").html();
				// 						if (existingResponse !== jsonobj.response) {
				// 							console.log("Updating content with a new response.");
				// 							$("#tab-new-tab-content .orderBTNCopy").html(jsonobj.response);
				// 						} else {
				// 							console.log("Response content is identical, skipping update.");
				// 						}
				// 					}
				// 				}
				// 			}else{
				// 				var webinarSidebar = `
				// 						<aside id="webinarSidebar" class="wi-col-12 wi-col-lg-3">
				// 							<div class="wi-row wi-g-0">
				// 								<div class="wi-col-12">
				// 									<ul class="wi-nav wi-nav-tabs wi-bg-light wi-px-1 wi-pt-1" id="webinarTabs" role="tablist" kt-hidden-height="48" style="">
				// 										<li class="wi-nav-item nav-item">
				// 											<a class="wi-nav-link nav-link active" id="tab-cta-sidebar-tab" data-toggle="tab" href="#tab-new-tab-content" role="tab" aria-controls="tab-new-tab-content" aria-selected="true">
				// 												${savedTabText}
				// 											</a>
				// 										</li>
				// 									</ul>
													
				// 									<div id="webinarTabsContent" class="wi-p-3" style="height: 80vh; overflow:auto;">
				// 										<div class="webinarTabsContent-inner default-template-sidebar webinarTabsContent-inner-absolute"  style="height: inherit;">
				// 											<div class="wi-tab-content"  style="height: inherit;">
				// 												<div class="wi-tab-pane active" id="tab-new-tab-content" aria-labelledby="tab-cta-sidebar-tab" role="tabpanel">
				// 													<div class="timedUnderArea test-7" id="orderBTN" style="margin: auto; width: 100%;">
				// 														<div class="orderBTNCopy">
				// 															${savedResponse}
				// 														</div>`;
				// 														if(savedButtonUrl && savedButtonText){
				// 															webinarSidebar +=`<div id="orderBTNArea">
				// 																				<a href="${savedButtonUrl}" target="_blank" class="large radius button success addedArrow replayOrder wiButton wiButton-lg wiButton-block wi-live-btn" style="background-color:${savedButtonColor};color:${savedBtnColorA};border: 1px solid rgba(0,0,0,0.20);">${savedButtonText}</a>
				// 																			</div>`;
				// 														}
																		
				// 														webinarSidebar +=`</div>
				// 												</div>
				// 											</div>
				// 										</div>
				// 									</div>
													
				// 								</div>
				// 							</div>
				// 						</aside>
				// 					`;
				// 			}
					
				// 			var webinarVideoElement = document.getElementById("webinarVideo");
				// 			webinarVideoElement.classList.add("wi-col-lg-9");
	
				// 			// Check if the sidebar already exists
				// 			const existingSidebar = document.getElementById('webinarSidebar');
				// 			if (existingSidebar && (giveAwayTab === 'hide' || qaTab === 'hide')  ) {
				// 				// Remove the existing sidebar
				// 				existingSidebar.remove();
				// 				console.log("ON LOAD: Existing Webinar sidebar removed.");
				// 			}
				// 			// Add the new sidebar
				// 			webinarVideoElement.insertAdjacentHTML('afterend', webinarSidebar);
				// 			console.log("ON LOAD: New Webinar sidebar added.");
				// 		}else{
				// 			console.log("ON lOAD: liveCtaPosition is not 'outer'. Cleaning up appended elements.");
	
				// 			// Remove the tab from the tabs list
				// 			$("#tab-cta-sidebar-tab").closest("li").remove();
	
				// 			// Remove the content div
				// 			$("#new-live-content").remove();
	
				// 			// Optionally reset other tabs or content to active
				// 			$("#webinarTabs .wi-nav-link:first").addClass("active").attr("aria-selected", "true");
				// 			$(".wi-tab-content .wi-tab-pane:first").addClass("active");
				// 			$(".webinarVideoCTA ").css("display", "block");
				// 			if ($("#webinarTabs li").length > 1) {
				// 				// Your code goes here
				// 				console.log("There are more than 1 <li> elements");
				// 			}else{
				// 				$("#webinarSidebar").remove();
				// 				$("#webinarVideo")[0].classList.remove('wi-col-lg-9');
				// 			}
				// 		}
				// 	} else if(airToggle === "OFF") {
				// 		$('.webinarVideoCTA').removeClass('webinarVideoCTAActive');
				// 		$('#tab-cta-sidebar-tab').closest('.wi-nav-item').remove();
				// 		$('#tab-new-tab-content').removeClass('active');
				// 		const navItems = $('#webinarTabs .wi-nav-item .wi-nav-link');
				// 		const hasActive = navItems.hasClass('active');
				// 		// If none of them has the 'active' class, add it to the first item
				// 		if (!hasActive) {
				// 			navItems.first().click();
				// 		}

				// 		if( $('#webinarTabs li').length){
				// 			console.log("it contains tabs already ")
				// 		}else{
							
				// 			var webinarVideoElement = document.getElementById("webinarVideo");
				// 			webinarVideoElement.classList.remove("wi-col-lg-9");
				// 			$('.webinarVideoCTA').css('display', 'none');
				// 			// Check if any of the nav-links have the 'active' class
							
				// 		}
						
				// 	}
				// });
			
			}//end if

		}//end if
		function webinarignition_mark_lead_status(status, always_callback) {
			return $.ajax({
				url: ajaxurl,
				method: 'POST',
				dataType: 'JSON',
				async: (/Firefox[\/\s](\d+)/.test(navigator.userAgent) && new Number(RegExp.$1) >= 4) === false,
				data: {
					action: 'webinarignition_lead_mark_' + status,
					nonce: nonce,
					webinar_id: webinar.id, 
					lead_id: $lead_id 
				},
				always: function(xhr, xhr_status, xhr_error) {
					if( always_callback && typeof always_callback === 'function' ) {
						always_callback(xhr, xhr_status, xhr_error);
					}
				}
			});
		}
		
		function getTextColorFromBgColor(hexColor) {
			// Remove the hash symbol if it exists
			let hexCode = '';
			if(hexColor){
				hexCode = hexColor.replace(/^#/, '');	
			}

		
			// Expand shorthand hex color (e.g., #abc to #aabbcc)
			if (hexCode.length === 3) {
				hexCode = hexCode.split('').map(char => char + char).join('');
			}
		
			// Convert hex to RGB
			const r = parseInt(hexCode.substring(0, 2), 16);
			const g = parseInt(hexCode.substring(2, 4), 16);
			const b = parseInt(hexCode.substring(4, 6), 16);
		
			// Calculate the YIQ value
			const yiq = (r * 299 + g * 587 + b * 114) / 1000;
		
			// Return black or white based on the YIQ value
			return (yiq >= 198) ? 'black' : 'white';
		}
	})(jQuery);

