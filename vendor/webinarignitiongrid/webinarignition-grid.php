<?php

define( 'WI_GRID_VERSION', '1.1.1' );
define( 'WI_GRID_MINIMUM_WP_VERSION', '5.0' );
define( 'WI_GRID_PLUGIN_FILE', __FILE__ );
define( 'WI_GRID_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
define( 'WI_GRID_PLUGIN_URL', plugin_dir_url( __FILE__ ) );

require_once( WI_GRID_PLUGIN_DIR . 'class.wig.metabox.php' );
require_once( WI_GRID_PLUGIN_DIR . 'class.wig.php' );
require_once( WI_GRID_PLUGIN_DIR . 'class-w-i-g-short-code.php' );
add_action( 'plugins_loaded', array( 'WI_GRID', 'plugins_loaded' ) );
register_activation_hook( __FILE__, array( 'WI_GRID', 'plugin_activation' ) );


add_action( 'init', array( 'WI_GRID', 'init' ) );

add_shortcode( 'wi_grid', array('WI_GRID', 'wi_grid_shortcode') );