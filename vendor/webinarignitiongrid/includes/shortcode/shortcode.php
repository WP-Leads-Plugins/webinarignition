<?php

defined('ABSPATH') || exit;

$shortcode = WI_GRID::get_created_shortcode();

$terms = get_terms([
    'taxonomy' => 'wi_grid_category',
    'hide_empty' => false,
]);

$selected_webinars   = isset( $_POST['wig_webinars'] ) ? $_POST['wig_webinars'] : '';
$selected_types      = isset( $_POST['wig_types'] ) ? $_POST['wig_types'] : '';
$selected_categories = isset( $_POST['wig_categories'] ) ? $_POST['wig_categories'] : '';
$post_labels         = isset( $_POST['wig_css_classes'] ) ? $_POST['wig_css_classes'] : '';

?>
<div class="wis-shortcode-wrapper">
	<h2><?php esc_html_e('WebinarIgnition Grid Shortcode', 'webinar-ignition'); ?></h2>
	<div class="shortcode-form-wrap" style="">
		<form method="POST">
			<label><?php esc_html_e('Select Webinars', 'webinar-ignition'); ?></label>
			<select class="wig_webinars" name="wig_webinars[]" multiple>
			<?php foreach( WI_GRID::wi_grid_get_all_webinars() as $webinar ) :
				$hide_webinar = get_post_meta( $webinar->postID, 'wi_grid_hide', true ); // checkbox

				if( 'on' !== $hide_webinar ) {
			    	$webinar_data = WebinarignitionManager::webinarignition_get_webinar_data($webinar->ID);

			    	if( !empty($webinar_data) ) {
			    		?>
			    		<option value="<?php echo $webinar->ID; ?>" <?php echo in_array($webinar->ID, (array) $selected_webinars) ? 'selected':''; ?>>
				    		<?php echo isset($webinar_data->webinar_desc) ? $webinar_data->webinar_desc : ''; ?>
				    	</option>
			    		<?php
			    	}
			    }
			endforeach;
			?>
			</select>
			<hr>
			<label><?php esc_html_e('Webinar Types', 'webinar-ignition'); ?></label>
			<input type="checkbox" value="new" name="wig_types[]" <?php echo in_array('new', (array) $selected_types) ? 'checked': ''; ?>><?php esc_html_e('Live', 'webinar-ignition'); ?><br>
			<input type="checkbox" value="auto" name="wig_types[]" <?php echo in_array('auto', (array) $selected_types) ? 'checked': ''; ?>><?php esc_html_e('Evergreen', 'webinar-ignition'); ?><br>
			<!--<input type="checkbox" value="import" name="wig_types[]" <?php //echo in_array('import', (array) $selected_types) ? 'checked': ''; ?>><?php //esc_html_e('Import', 'webinar-ignition'); ?>-->
			<hr>
			<label><?php esc_html_e('Categories', 'webinar-ignition'); ?></label>
			<?php if( !empty( $terms ) ) : ?>
				<?php foreach( $terms as $term ) : ?>
					<input type="checkbox" name="wig_categories[]" <?php echo in_array($term->slug, (array) $selected_categories) ? 'checked': ''; ?> value="<?php echo $term->slug; ?>"><?php echo $term->name; ?><br>
				<?php endforeach; ?>
			<?php endif; ?>
			<hr>
			<label><?php esc_html_e('Label Classes', 'webinar-ignition'); ?></label>
			<input type="text" placeholder="Enter your classes. seperate them with a space." name="wig_css_classes" value="<?php echo $post_labels; ?>">
			<hr>
			<button type="submit" name="wig-create-shortcode"><?php esc_html_e('Create Shortcode', 'webinar-ignition'); ?></button>
		</form>
	</div>
	<div class="shortcode-container">
		<code><?php echo esc_html( $shortcode ); ?></code>
		<button id="copy-shortcode-button" data-message="<?php esc_html_e('Shortcode Copied!', 'webinar-ignition'); ?>" type="button"><?php esc_html_e('copy', 'webinar-ignition'); ?></button>
	</div>
</div>
<script type="text/javascript">
	jQuery(document).ready(function($){
		$('.wig_webinars').select2();
		$('#copy-shortcode-button').click( function(){
			
			$(this).css('opacity', '0.3');
			var $temp = $("<input>");
            $("body").append($temp);
            $temp.val($('.shortcode-container code').text()).select();
			document.execCommand("copy");
            $temp.remove();
			$(this).css('opacity', '1');
			$(this).toggleClass('show-message');
			setTimeout( function(){
				$('.shortcode-container button').toggleClass('show-message');
			}, 1000);
		});
	});
</script>