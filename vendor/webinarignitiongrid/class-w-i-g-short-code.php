<?php

defined("ABSPATH") || exit;

class W_I_G_Short_Code{

	public function __construct(){
		add_action('admin_menu', array($this, 'add_submenu'), 100 );

		add_action('init', array( $this, 'register_category'));

		add_action('admin_enqueue_scripts', array( $this, 'enqueue_scripts') );
	}

	public function enqueue_scripts(){
		wp_enqueue_style('wig-admin-shortcode', WI_GRID_PLUGIN_URL . 'assets/css/admin.css');

		wp_register_style('wi-select2', 'https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css');
		wp_register_script('wi-select2', 'https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js');

		if( isset( $_GET['page'] ) && 'wig-shortcode' == $_GET['page'] ) {
			wp_enqueue_style('wi-select2');
		}
	}

	public function register_category(){

		$labels = array(
			'name'              => _x( 'Webinar Categories', 'taxonomy general name', 'webinar-ignition'),
			'singular_name'     => _x( 'Webinar Category', 'taxonomy singular name', 'webinar-ignition'),
			'search_items'      => __( 'Search Webinar Category', 'webinar-ignition'),
			'all_items'         => __( 'All Webinar Categories', 'webinar-ignition'),
			'parent_item'       => __( 'Parent Webinar Category', 'webinar-ignition'),
			'parent_item_colon' => __( 'Parent Webinar Category:', 'webinar-ignition'),
			'edit_item'         => __( 'Edit Webinar Category', 'webinar-ignition'),
			'update_item'       => __( 'Update Webinar Category', 'webinar-ignition'),
			'add_new_item'      => __( 'Add New Webinar Category', 'webinar-ignition'),
			'new_item_name'     => __( 'New Webinar Category Name', 'webinar-ignition'),
			'menu_name'         => __( 'Webinar Category', 'webinar-ignition'),
		);
		$args   = array(
			'hierarchical'      => true, // make it hierarchical (like categories)
			'labels'            => $labels,
			'show_ui'           => true,
			'show_admin_column' => true,
			'query_var'         => true,
			'rewrite'           => [ 'slug' => 'wi_grid_category' ],
		);
		register_taxonomy( 'wi_grid_category', [ 'page' ], $args );
	}

	public function add_submenu(){
		add_submenu_page(
			'webinarignition-dashboard',
			__('Grid Settings', 'webinar-ignition'),
			__('Grid Settings', 'webinar-ignition'),
			'manage_options',
			'wig-shortcode',
			array($this, 'add_shortcode_page'),
		);
	}

	public function add_shortcode_page(){
		wp_enqueue_script('wi-select2');
		include_once WI_GRID_PLUGIN_DIR . 'includes/shortcode/shortcode.php';
	}
}

new W_I_G_Short_Code();