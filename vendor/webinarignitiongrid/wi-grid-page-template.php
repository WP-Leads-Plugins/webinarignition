<?php 
/**
Template Name: Webinars
**/

get_header();

$webinars = WI_GRID::wi_grid_get_all_webinars();
?>
<h3 style="text-align:center;margin-bottom: 10px;"><?php echo the_title(); ?></h3>
<div id="wi_grid_content_details" style="text-align:center;">
    <?php echo the_content();?>
</div>
<?php if( !empty($webinars) ): ?>
    <section id="wi_grid" class="wi_grid_wrapper">
    <?php
        foreach( $webinars as $wi ):
		    WI_GRID::grid_item($wi);
	    endforeach;
    ?>
    </section>
    <?php
endif;

get_footer();