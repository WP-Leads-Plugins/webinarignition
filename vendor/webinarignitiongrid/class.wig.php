<?php

class WI_GRID {
	private static $initiated = false;
	public static function init() {
		if ( ! self::$initiated ) {
			self::init_hooks();
		}
	}

	private static function init_hooks() {
		self::$initiated = true;
		// Add initial hooks here.
		self::page_setup(); // this is supposed to be in the activation hook, but what if the page got deleted unintentionally?
        add_filter( 'template_include', array('WI_GRID', 'page_template') , 99 );
		add_action('wp_enqueue_scripts', array('WI_GRID', 'wi_grid_css'));
	}

	public static function plugins_loaded() {
		if ( current_user_can( 'activate_plugins' ) && !class_exists( 'WebinarIgnition' ) ) {

			if ( ! function_exists( 'is_plugin_active_for_network' ) ) {
				include_once( ABSPATH . '/wp-admin/includes/plugin.php' );
			}

			deactivate_plugins ( plugin_basename ( WI_GRID_PLUGIN_FILE ), true );

			add_action( "admin_notices", ['WI_GRID', 'missing_plugin_notice'] );
		}
	}

	public static function missing_plugin_notice() {
		unset($_GET['activate']);  //unset this to hide default Plugin activated. notice

		$plugin_data = get_plugin_data( WI_GRID_PLUGIN_FILE );

		$class = 'notice notice-error is-dismissible';
		$message = sprintf( __( '%s requires <a href="https://wordpress.org/plugins/webinar-ignition/">WebinarIgnition</a> plugin to be activated.', 'webinar-ignition' ), $plugin_data['Name'] );
		printf ( "<div id='message' class='%s'> <p>%s</p></div>", $class, $message );
	}

	public static function page_setup() {
		$grid_page_id = get_option('wi_grid_page_id', 0);

		if( empty($grid_page_id) || get_post_type($grid_page_id) !== 'page' ) {
			$admin_user_id = 1;
			$admin_user = get_users( ['role' => 'administrator', 'number' => 1, 'fields' => 'ID'] );
			if( !is_wp_error($admin_user) ) {
				$admin_user_id = reset($admin_user);
			}

			$page_details = array(
				'post_title'    => 'Webinars',
				'post_content'  => 'Exclusive webinars to help you!',
				'post_status'   => 'publish',
				'post_author'   => $admin_user_id,
				'post_type' => 'page'
			);

			$grid_page_id = wp_insert_post( $page_details );

			update_option('wi_grid_page_id', $grid_page_id);
		}
	}

	public static function page_template( $template ) {
		//locate_template( array( 'wi-grid-template.php' ) );
		$grid_page_id = get_option('wi_grid_page_id', 0);
		if( !empty($grid_page_id) && is_page($grid_page_id) ) {
		    $template_path = dirname( WI_GRID_PLUGIN_FILE ) . '/wi-grid-page-template.php';
            if ( file_exists( $template_path ) ) {
                $template = $template_path;
            }
		}

		return $template;
	}

	public static function get_created_shortcode() {
		$shortcode = '[wi_grid]';
		if( isset($_POST['wig-create-shortcode'])){

		    $wi_selected_types = [];
		    if( isset($_POST['wig_types']) && is_array($_POST['wig_types']) ) {
			    $wi_selected_types = $_POST['wig_types'];

			    if( ($key = array_search('new', $wi_selected_types)) !== FALSE ) {
			        $wi_selected_types[$key] = 'live';
			    }
		    }

			$shortcode = '[wi_grid';

			$wig_webinars    = isset( $_POST['wig_webinars'] ) ? ' ids="' . implode( ',', $_POST['wig_webinars'] ) . '"' : '';
			$wig_types       = !empty( $wi_selected_types ) ? ' type="' . implode( ',', $wi_selected_types ) . '"' : '';
			$wig_categories  = isset( $_POST['wig_categories'] ) ? ' categories="' . implode( ',', $_POST['wig_categories'] ) . '"' : '';
			$wig_css_classes = isset( $_POST['wig_css_classes'] ) ? ' label_classes="' . $_POST['wig_css_classes'] . '"' : '';

			$shortcode .= $wig_webinars . $wig_types . $wig_categories . $wig_css_classes . ']';
		}
		return $shortcode;
	}

	public static function wi_grid_css() {
	    global $post;
		if( is_singular() ) {
			$grid_page_id = get_option('wi_grid_page_id', 0);
		    if( $post->ID == $grid_page_id || has_shortcode( $post->post_content, 'wi_grid') ) {
			    wp_enqueue_style( 'wi-grid', plugin_dir_url( WI_GRID_PLUGIN_FILE ) . 'wi-grid.css', array(), '1.0.0', 'all' );
            }
        }
	}

	public static function wi_grid_get_all_webinars($type = null, $categories = array(), $label_classes = array()) {
		global $wpdb;

		$page_ids = null;

		$categories = array_map('trim', $categories);
		$categories = array_map('sanitize_text_field', $categories);
		$categories = array_filter($categories);

		if(!empty($categories)) {

			$args = array(
				'post_type'      => 'page',
				'posts_per_page' => -1,
				'post_status'    => array( 'publish' ),
				'fields'         => 'ids',
				'tax_query'      => array(
					'relation'	=> 'OR',
					array(
						'taxonomy' => 'wi_grid_category',
						'field'    => 'slug',
						'terms'    => $categories,
						'operator' => 'IN'
					)
				)
			);

			$pages_query = new WP_Query( $args );
			$page_ids    = $pages_query->get_posts();
		}

		$params = [];

		$query = "SELECT * FROM {$wpdb->prefix}webinarignition as WIA WHERE 1=1";

		if( !empty($page_ids) ) {
			$query .= " AND WIA.postID IN (" . implode( ',', $page_ids ) . ")";
		}

		if( !empty($type) ) {

			$types = explode(',', $type);

			foreach( $types as $index => $type ) {

		    if( $type == 'live' ) {
			    $type = 'new';
		    }
			    if( 0 == $index ){
			    	$query .= " AND (WIA.camtype = %s";
			    } else {
			    	$query .= " OR WIA.camtype = %s";
			    }

			$params[] = $type;
		}

			if( count( $types ) >= 1 ) {
		    	$query .= ")";
		    }
		}

		if( !empty($params) ) {
			$query = $wpdb->prepare( $query, $params );
		}

		$webinars = $wpdb->get_results( $query );

		return $webinars;
	}

	private static function get_registered_user($webinar_data) {
		global $wpdb;

		$registered_user = null;

		if( is_user_logged_in() ) {

			$leads_table = "{$wpdb->prefix}webinarignition_leads";
			if( strtolower($webinar_data->webinar_date) == 'auto' ) {
				$leads_table = "{$wpdb->prefix}webinarignition_leads_evergreen";
			}

			$current_user = wp_get_current_user();
			$current_user_email = $current_user->user_email;
			$registered_user = $wpdb->get_row( $wpdb->prepare("SELECT * FROM {$leads_table} as L WHERE L.app_id=%d AND L.email=%s" , absint($webinar_data->id), $current_user_email ));
		}

		return $registered_user;
	}

	public static function grid_item($wi) {
	    global $wpdb;

		$hide_webinar = get_post_meta( $wi->postID, 'wi_grid_hide', true ); // checkbox
		

		if( 'on' !== $hide_webinar ) {

		    $webinar_data = WebinarignitionManager::webinarignition_get_webinar_data($wi->ID);
			if(!empty($webinar_data)) {
				WebinarignitionManager::webinarignition_set_locale($webinar_data);
				if(isset($webinar_data->webinar_status) && $webinar_data->webinar_status == 'published') {
					$registered_user = self::get_registered_user($webinar_data);
                    ?>
                    <div class="wi_grid_webinar">
                    <?php self::grid_item_image($webinar_data); ?>
                    <?php self::grid_item_time($webinar_data, $registered_user); ?>
                    <h2><?php echo $webinar_data->webinar_desc; ?></h2>
                    <p class="wi_grid_cta_social_desc"><?php echo $webinar_data->lp_metashare_desc; ?></p>
                    <?php self::grid_item_button($webinar_data, $registered_user); ?>
                    </div>
				<?php }
				WebinarignitionManager::webinarignition_restore_locale($webinar_data);
			}
		}
	}

	private static function grid_item_button($webinar_data, $registered_user) {
		$default_registration_page_id  = WebinarignitionManager::webinarignition_get_webinar_post_id($webinar_data->id);
		$saved_registration_page = empty($webinar_data->default_registration_page) || get_post_type( $webinar_data->default_registration_page ) != 'page' ? absint( $default_registration_page_id ) :absint($webinar_data->default_registration_page);

        if(!empty($registered_user)) { ?>
            <a href="<?php echo get_the_permalink( $saved_registration_page ) . '?live&lid=' . $registered_user->hash_ID ; ?>" class="wi_grid_cta_btn" style="background-color:#ccc; color: #000;"><?php _e('You\'re Registered!', 'webinar-ignition'); ?></a>
        <?php } else { ?>
            <?php if ( (!empty( $webinar_data->webinar_switch ) && $webinar_data->webinar_switch == 'closed') ) { ?>
                <div style="text-align:center; padding:5px;">
                    <?php echo $webinar_data->lp_optin_closed ? $webinar_data->lp_optin_closed : __( 'Registration is closed for this webinar.', "webinarignition") ; ?>
                </div>
                <?php } else { ?>
                    <a href="<?php echo get_the_permalink( $saved_registration_page ); ?>" class="wi_grid_cta_btn"><?php _e('Register', 'webinar-ignition'); ?></a>
                <?php
            }
        }
	}

	private static function grid_item_time($webinar_data, $registered_user) {
	    if($webinar_data->webinar_date==='AUTO') {
            if(!empty($registered_user)) { ?>
                <p class="wi_grid_cta_time">
	                <?php echo webinarignition_get_localized_date($webinar_data, $registered_user); ?>
	                <?php echo webinarignition_display_time( $webinar_data, $registered_user ); ?>
                </p>
            <?php } else { 
				if ( 'yes' === trim( $webinar_data->auto_today ) ) {
					$instant_text = empty( $webinar_data->auto_translate_instant ) ? __( 'Instant Access', 'webinar-ignition' ) : $webinar_data->auto_translate_instant;
				}else{
					$webinar_dates = self::webinarignition_gird_auto_lp_get_dates_callback($webinar_data->id);
					$webinar_next_time = self::get_next_time($webinar_dates);
					$instant_text = $webinar_next_time['date'].' '.$webinar_next_time['time'] ;

				}
				?>
                <p class="wi_grid_cta_time">
                    <?php webinarignition_display(
                        $webinar_data->auto_translate_headline1,
                        $instant_text
                    ); ?>
                </p>
            <?php }
        } else {
	        ?>
            <p class="wi_grid_cta_time">
			    <?php echo webinarignition_get_localized_date($webinar_data, $registered_user); ?>
			    <?php echo webinarignition_display_time( $webinar_data, $registered_user ); ?>
            </p>
            <?php
	    }
	}
	private static function get_next_time($data) {
		date_default_timezone_set($data['tz']); // Set timezone
		$current_date = date('Y-m-d'); // Get today's date
		$current_time = date('h:i A'); // Get current time
		
		foreach ($data['dates'] as $date => $formatted_date) {
			// Get times for the current date
			$times = $data['times_by_date'][$date];
			
			sort($times); // Sort times in chronological order
	
			// If today is the current date
			if ($date === $current_date) {
				foreach ($times as $time) {
					// If a time is in the future, return it
					if (strtotime($time) > strtotime($current_time)) {
						return ['date' => $formatted_date, 'time' => $time];
					}
				}
			}
			// If today is not the current date or all times are passed
			elseif (strtotime($date) > strtotime($current_date)) {
				return ['date' => $formatted_date, 'time' => $times[0]]; // Return the first time of the next date
			}
		}
	
		// If no times or dates are available
		return null;
	}
	private static function webinarignition_gird_auto_lp_get_dates_callback( $webinar_id ) {
		check_ajax_referer( 'webinarignition_ajax_nonce', 'security', false );
	
		// Prototype For Evergreen Webinar
		// Get Variables
		$ID = $webinar_id;
	
		// Get Results
		$webinar_data = WebinarignitionManager::webinarignition_get_webinar_data( $ID );
	
		if ( ! empty( $webinar_data->webinar_lang ) ) {
			switch_to_locale( $webinar_data->webinar_lang );
			unload_textdomain( 'webinar-ignition' );
			load_textdomain( 'webinar-ignition', WEBINARIGNITION_PATH . 'languages/webinar-ignition-' . $webinar_data->webinar_lang . '.mo' );
		}
	
		$date_format = ! empty( $webinar_data->date_format ) ? $webinar_data->date_format : 'l, F j, Y';
	
		$day_toggle = array(
			'monday'    => $webinar_data->auto_monday,
			'tuesday'   => $webinar_data->auto_tuesday,
			'wednesday' => $webinar_data->auto_wednesday,
			'thursday'  => $webinar_data->auto_thursday,
			'friday'    => $webinar_data->auto_friday,
			'saturday'  => $webinar_data->auto_saturday,
			'sunday'    => $webinar_data->auto_sunday,
		);
	
		$blacklisted_dates  = array_map( 'trim', explode( ',', $webinar_data->auto_blacklisted_dates ) );
		$webinar_timezone   = webinarignition_get_webinar_timezone( $webinar_data, "Asia/Karachi" );
		$weekday_time_slots = webinarignition_get_time_slots_by_weekdays( $webinar_data );
		$one_day_interval   = DateInterval::createfromdatestring( '+1 day' );
		$datetime_now       = new DateTime( 'now', new DateTimeZone( $webinar_timezone ) );
		$weekday_now        = strtolower( $datetime_now->format( 'l' ) );
	
		if ( ! empty( $weekday_time_slots[ $weekday_now ] ) ) {
			$today_time_slots_desc_order = $weekday_time_slots[ $weekday_now ];
			webinar_ignition_sort_datetime_desc( $today_time_slots_desc_order ); // Sort the times in DESC order
	
			// Create a new datetime object with today's date for comparison with max time slot, and assign webinar timezone
			$datetime_compare = new DateTime( $datetime_now->format( 'Y-m-d' ) . ' ' . $today_time_slots_desc_order[0], new DateTimeZone( $webinar_timezone ) );
	
			// Convert current datetime from webinar timezone to UTC for comparison, and to avoid daylight saving differences
			$datetime_now->setTimezone( new DateTimeZone( 'UTC' ) );
	
			// Convert compare datetime from webinar timezone to UTC
			$datetime_compare->setTimezone( new DateTimeZone( 'UTC' ) );
	
			$skip_today = $datetime_now->getTimestamp() > $datetime_compare->getTimestamp();
	
			if ( $skip_today ) {
				$datetime_now->add( $one_day_interval ); // Skip today
			}
	
			// Set current datetime back to webinar timezone for later use
			$datetime_now->setTimezone( new DateTimeZone( $webinar_timezone ) );
		}//end if
	
		$number_of_days       = ! empty( $webinar_data->auto_day_limit ) && is_numeric( $webinar_data->auto_day_limit ) ? $webinar_data->auto_day_limit : 7;
		$number_of_days_delay = ! empty( $webinar_data->auto_day_offset ) && is_numeric( $webinar_data->auto_day_offset ) ? $webinar_data->auto_day_offset : 0;
		if ( ! empty( $number_of_days_delay ) ) {
			$datetime_now->add( DateInterval::createfromdatestring( "+$number_of_days_delay day" ) );
		}
	
		$datetimeObjects = array();
		$dates           = array();
		$times           = array();
	
		while ( $number_of_days > count( $datetimeObjects ) ) {//phpcs:ignore
	
			$_current_date_weekday_full = strtolower( $datetime_now->format( 'l' ) );
			$skip_day                   = in_array( $datetime_now->format( 'Y-m-d' ), $blacklisted_dates, true ) || 'yes' !== trim( $day_toggle[ $_current_date_weekday_full ] );
	
			if ( $skip_day ) {
				$datetime_now->add( $one_day_interval );
				continue;
			}
	
			$datetimeObjects[] = clone $datetime_now;
			$datetime_now->add( $one_day_interval );
		}
	
		// Instant access
		if ( 'yes' === trim( $webinar_data->auto_today ) ) {
			$instant_text = empty( $webinar_data->auto_translate_instant ) ? __( 'Instant Access', 'webinar-ignition' ) : $webinar_data->auto_translate_instant;
			$dates        = array( 'instant_access' => $instant_text );
		}
	
		if ( ! empty( $datetimeObjects ) ) {
			foreach ( $datetimeObjects as $datetimeObject ) {
				$dates[ $datetimeObject->format( 'Y-m-d' ) ] = $datetimeObject->format( $date_format );
			}
		}
	
		foreach ( $dates as $mysql_date_string => $date_format_string ) {
	
			if ( 'instant_access' === $mysql_date_string ) {
				continue;
			}
	
			$_current_date_weekday_full = strtolower( gmdate( 'l', strtotime( $mysql_date_string ) ) );
	
			if ( isset( $weekday_time_slots[ $_current_date_weekday_full ] ) && ! empty( $weekday_time_slots[ $_current_date_weekday_full ] ) ) {
				$times[ $mysql_date_string ] = $weekday_time_slots[ $_current_date_weekday_full ];
			} else {
				unset( $dates[ $mysql_date_string ] );
			}
		}
	
		$times = array_map( 'array_unique', $times );
	
		$response_data = array(
			'dates'         => webinarignition_get_wp_locale_date_strings( $dates, $date_format ),
			'times_by_date' => $times,
			'tz'            => $webinar_timezone,
		);
	
		restore_previous_locale();
	
		return  $response_data;
	}
	private static function grid_item_image($webinar_data) {

	    $webinar_status = webinarignition_get_lead_status($webinar_data);
	    if( !isset($webinar_data->webinar_switch) ) {
		    $webinar_data->webinar_switch = $webinar_status;
	    }

	    switch ($webinar_data->webinar_switch) {
          case 'live':
            $status_label = __('Live Now', 'webinar-ignition');
            $status_label_class = $webinar_data->webinar_switch;
            break;
          case 'countdown':
            $status_label = __('Upcoming', 'webinar-ignition');
            $status_label_class = 'upcoming';
            break;
          case 'replay':
            $status_label = __('Replay', 'webinar-ignition');
            $status_label_class = $webinar_data->webinar_switch;
            break;
          case 'closed':
            $status_label = __('Closed', 'webinar-ignition');
            $status_label_class = $webinar_data->webinar_switch;
            break;
          default:
            $status_label = __('Evergreen', 'webinar-ignition');
            $status_label_class = 'evergreen';
        }

        switch ($webinar_data->paid_status) {
          case 'paid':
            $pay_type_label = __('Paid', 'webinar-ignition');
            break;
          default:
            $pay_type_label = __('Free', 'webinar-ignition');
        }

		$pay_type_label_class = strtolower($webinar_data->webinar_switch ?? '');

        $grid_image_url = WEBINARIGNITION_URL . 'inc/lp/images/noctaimage.png';
        if( isset($webinar_data->lp_grid_image_url) && !empty($webinar_data->lp_grid_image_url) ) {
            $grid_image_url = $webinar_data->lp_grid_image_url;
        }
//        $grid_image_url = 'https://via.placeholder.com/275x200.png?text=Sample%20Image';
        $grid_image_script = 'style="background-image:url(\'' . $grid_image_url . '\'); background-position:center;"';
        ?>
        <div class="wi_grid_cta" <?php echo $grid_image_script; ?>>
            <div class="wi_grid_cta_content">
                <span class="wi_grid_type wi_grid_type_<?php echo $pay_type_label_class; ?>"><?php echo $pay_type_label; ?></span>
                <span class="wi_grid_label wi_grid_label_<?php echo $status_label_class; ?> <?php echo !empty($label_classes) ? $label_classes : ''; ?>"><?php echo $status_label; ?></span>
            </div>
        </div>
        <?php
	}

	public static function wi_grid_shortcode($atts) {
		extract( shortcode_atts( array(
			'ids'  => '',
			'type' => '',
			'categories' => [],
			'label_classes' => []
		), $atts ) );

		if( !empty($categories) ) {

		    $categories = explode(',',$categories);

		    if( !is_array($categories) ) {
		        $categories = [];
		    }
		}

		if( !empty($ids) ) {

		    $ids = explode(',',$ids);

		    if( !is_array($ids) ) {
		        $ids = [];
		    }

		    $ids = array_filter( $ids );
		}

		$webinars = self::wi_grid_get_all_webinars($type, $categories, $label_classes);


		ob_start();
		if(!empty($webinars)):

            ?>
            <section id="wi_grid" class="wi_grid_wrapper">
				<?php
				foreach($webinars as $wi):
					if( !empty( $ids ) && !in_array( $wi->ID, $ids ) ) {
						continue;
					}
					self::grid_item( $wi );
				endforeach;
				?>
            </section>
			<?php
		endif;

		return ob_get_clean();
	}

	public static function plugin_activation() {
	    self::do_elc_wi_db_update();
	}

	/**
	 * Remove elc DB field names, and replace it with wi prefix
	 */
	private static function do_elc_wi_db_update() {
		global $wpdb;

		$wpdb->update(
			$wpdb->options,
			['option_name' => 'wi_grid_page_id'],
			['option_name' => 'elc_wiarchive_page_id']
		);

		$wpdb->update(
			$wpdb->postmeta,
			['meta_key' => 'wi_grid_hide'],
			['meta_key' => 'elc_wiarchive_hide-webinar-from-archive-page']
		);
	}
}